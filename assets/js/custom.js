// Array used as error collection
var errors = [],

// Validation configuration
conf = {
  onElementValidate : function(valid, $el, $form, errorMess) {
     if( !valid ) {
      // gather up the failed validations
      errors.push({el: $el, error: errorMess});
     }
  }
}

// Add custom validation rule
$.formUtils.addValidator({
  name : 'optional_number',
  validatorFunction : function(value, $el, config, language, $form) {
    if (value === '')
      return true;

    if (value.match(/^\d+$/) === null)
      return false;

    return true;
  },
  errorMessage : 'El valor proporcionado no es un número válido',
  errorMessageKey: 'notOptionalNumber'
});

// Add custom validation rule
$.formUtils.addValidator({
  name : 'currency',
  validatorFunction : function(value, $el, config, language, $form) {

    if (value.match(/^\d+(\.\d{1,2})?$/) === null)
      return false;

    return true;
  },
  errorMessage : 'El valor proporcionado no es un número válido',
  errorMessageKey: 'notOptionalNumber'
});

// Manually load the modules used in this form
$.formUtils.loadModules('security, date, html5');

$(window).load(function() {
    $.validate({
    	//form: 'form',
        modules : 'file, security, html5',
        validateOnBlur : true,
        lang: 'es'
    });
});