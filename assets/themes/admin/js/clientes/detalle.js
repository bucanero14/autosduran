$(document).ready(function () {
	$.fn.dataTable.moment('DD/MM/YYYY');
	$.fn.dataTable.moment('DD/MM/YYYY hh:mm:ss A');

	//Tabla de pagos pendientes
	$('#table-pagos-pendientes').dataTable({
		language: language,
		dom: 'tp',
		pageLength: 5
	});

	//Tabla de pagos realizados
	$('#table-pagos-realizados').dataTable({
		language: language,
		dom: 'tp',
		pageLength: 5
	});

	//Observaciones
	$('.table-observaciones').dataTable({
		language: language,
		order: [[0, 'desc']],
		dom: "<'row'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
		buttons: [{
		text: '<i class="fa fa-plus"></i> Agregar Observación',
		action: function(e, dt, node, config) {
			bootbox.prompt('Agregar Observación:', function (result) {
				if (result == null || result == '')
					return true;
				//ajax
				$.ajax({
					url: '/clientes/agregarObservacion',
					type: 'post',
					data: {
						observacion: result,
						id: $('.table-observaciones').data('id')
					}
				}).done(function (data) {
					if (data)
						location.reload();
				});
			});
		},
		className: 'btn btn-flat btn-success'
	}]
	});

	$(document).on('click','.open-modal',function(e) {
		e.preventDefault();
		var href = $(this).attr('href');

		var obj;
		if ($(this).hasClass('print'))
		{
			obj = bootbox.dialog({
			    message: '<div id="modal"></div>',
			    title: 'Recibo',
			    size: 'large',
			    closeButton: true,
			    onEscape: true,
			    buttons: {
			    	buttonName: {
			    		label: 'Imprimir',
			    		className: 'btn-flat btn-primary',
			    		callback: function() {
			    			$('.modal-body').print();
			    		}
			    	}
			    }
			});
		}
		else if ($(this).hasClass('liquidacion'))
		{
			obj = bootbox.dialog({
			    message: '<div id="modal-liquidacion"></div>',
			    title: 'Recibo',
			    size: 'large',
			    closeButton: true,
			    onEscape: true
			});
		}
		else
		{
			obj = bootbox.dialog({
			    message: '<div id="modal"></div>',
			    title: 'Recibo',
			    size: 'large',
			    closeButton: true,
			    onEscape: true
			});
		}
		$('#modal').load(href,function() {
			//asignamos la funcion de pagar cuando en el click del boton
			$('.btn-pagar').on('click',pagar);
			$('.btn-cerrar').on('click',function() {
				obj.modal('hide');
			});
			$('.input-pagos').on('change',calc_cambio);
			$('#table-pagos-pendientes-modal').dataTable({
				language: language,
				dom: 'tp',
				pageLength: 5
			});
		});
		$('#modal-liquidacion').load(href,function() {
			//asignamos la funcion de pagar cuando en el click del boton
			$('.btn-pagar').on('click',pagar);
			$('.input-pagos').on('change',calc_cambio);
			$('#table-pagos-pendientes-modal').dataTable({
				language: language,
				dom: 'tp',
				pageLength: 5
			});
			$('#importe').prop('readonly', true);

			$('#pendiente').change(function(e) {
				var pendiente = $(this).val();
				$.ajax({
					url: '/clientes/update_monto_liquidacion/' + $('#idcredito').val(),
					type: 'post',
					data: {
						pendiente: pendiente
					}
				}).done(function(result) {
					result = $.parseJSON(result);
					$('#importe').val(result.monto);
					$('#total').html('$' + result.monto_format);
				});
			});
		});
	});

	$(document).on('submit', '.form-pago', function(e) {
		$('.btn-pagar').prop('disabled', true);

		if (!$('.form-pago').isValid('', conf, true))
		{
			$('.btn-pagar').prop('disabled', false);
			return false;
		}
	});
});//READY

function pagar() {
	var idclient = $('.btn-pagar').data('id');
	// reset error array
	errors = [];

	if($('.form-pago').isValid('', conf, true) ) {
		//desactivamos el click del boton para que no se envien multiples solicitudes
		$('.btn-pagar').off('click');
		//si paso la validacion desactivamos el boton
		crear_pago(idclient);
	}
}

function calc_cambio() {

	var efectivo = $('#efectivo').val() > 0 ? $('#efectivo').val() : 0;
	var importe = $('#importe').val() > 0 ? $('#importe').val() : 0;
	var cambio = $('#cambio').val() > 0 ? $('#cambio').val() : 0;

	cambio = (efectivo - importe).toFixed(2);

	$('#cambio').val(cambio);
}

function crear_pago(id) {

	var idclient = id;

	$.post('/dashboard/add_pago/'+idclient,$('.form-pago').serialize(),function(data) {
		//tiramos una alerta si se ingreso el pago o no
		if (data.error==false)
		{
			bootbox.alert("Pago Realizado Correctamente.",function () {
				location.reload();
			});
		}
		else
		{
			bootbox.alert("Error, por favor intente de nuevo.",function () {
				location.reload();
			});

		}
	},'json');

}