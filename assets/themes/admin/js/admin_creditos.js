$(document).ready(function() {
  $.fn.dataTable.moment('DD/MM/YYYY');

  $('table').DataTable({
    language: language,
    dom: "<'row'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
	buttons: [{
		text: '<i class="fa fa-plus"></i> Agregar Credito',
		action: function(e, dt, node, config) {
			window.location = 'creditos/crear';
		},
		className: 'btn btn-flat btn-success'
	}]
  });

  $(document).on('click','.open-modal',function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    
    bootbox.dialog({
        message: '<div id="modal"></div>',
        title: 'Información del Credito',
        size: 'large',
        closeButton: true,
        onEscape: true
    });
    $('#modal').load(href);

    //console.log(href);
  });

  $(document).on('click','.liquidacion',function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    
    bootbox.dialog({
        message: '<div id="modal"></div>',
        title: 'Liquidar Crédito',
        size: 'large',
        closeButton: true,
        onEscape: true,
        buttons: {
          buttonName: {
            label: 'Imprimir',
            className: 'btn-flat btn-primary',
            callback: function() {
              $('.modal-body').print();
            }
          }
        }
    });
    $('#modal').load(href);

    //console.log(href);
  });

  $(document).on('click','.pagos-pendientes',function(e) {
    e.preventDefault();

    var href = $(this).attr('href');

    bootbox.dialog({
      message: '<div  id="modal"></div>',
      title: 'Pagos pendientes',
      size: 'large',
      closeButton: true,
      onEscape: true
    });

    $('#modal').load(href, function () {
      $('#pagos').DataTable({
        language: language
      });
    });
  })
  $('.btn-truncate').click(function(e) {
    e.preventDefault();
    $btn = $(this);
    bootbox.confirm({
      message: '¿Estás seguro que quieres truncar a este credito?',
      buttons: {
        confirm: {
          label: 'Aceptar',
          className: 'btn-success'
        },
        cancel: {
          label: 'Cancelar',
          className: 'btn-danger'
        }
      },
      callback: function(result) {
          if (result){
            $.ajax({
              url: $btn.attr('href'),
              type: 'post',
              dataType: 'json'
            });
            location.reload();
          }
        }
    });
  });
})
