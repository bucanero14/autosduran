$(document).ready(function (){
	var datatable = $('table').DataTable({
		language: language,
		columns: [
			{data: 'usuario'},
			{data: 'nombre'},
			{data: 'rol'}
		]
	});

	$('#generar-reporte').click(function () {
		if ($('#sucursal').val() === 0)
			return false;
		if ($('#fecha').val() === '')
			return false;

		$('#cobranza').html('');
		$.ajax({
			url: '/reportes/get_cobranza_sucursal',
			type: 'post',
			data: { 
				idsucursal: $('#sucursal').val(),
				fecha: $('#fecha').val()
			}
		}).done(function (data) {
			data = $.parseJSON(data);
			if (data) {
				var pagos_totales = parseInt(data.pagos_totales);
				var pagos_hechos = parseInt(data.pagos_hechos);
				var pagos_pendientes = pagos_totales - parseInt(data.pagos_hechos);

				var efectividad = Math.round((pagos_hechos * 100) / pagos_totales);
				var ineficiencia = Math.round((pagos_pendientes * 100) / pagos_totales);

				Morris.Donut({
					element: 'cobranza',
					data: [
					    {label: 'Efectividad de cobranza', value: efectividad},
					    {label: 'Porcentaje de pagos pendientes', value: ineficiencia}
					],
					colors: ["#3c8dbc", "#f56954"],
					formatter: function (x, data) { return x + '%'; }
				});

				datatable.clear().draw();
				datatable.rows.add(data.data); // Add new data
				datatable.columns.adjust().draw(); // Redraw the DataTable
			}
		});
	});

	$('#fecha').keydown(function(e) {
		return false;
	})
	.datetimepicker({
		locale: 'es',
		format: 'YYYY-MM'
	});
})