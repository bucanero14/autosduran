$(document).ready(function (){
	var buttonCommon = {
		exportOptions: {
			format: {
				body: function (data, row, column, node) {
					return (column === 4 || column === 5) ? data.replace( /[$,]/g, '' ) : data;
				}
			}
		}
	};

	var datatable = $('table').DataTable({
		language: language,
		dom: "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
		columns: [
			{data: 'fecha'},
			{data: 'recibo'},
			{data: 'nombre'},
			{data: 'documento'},
			{
				data: 'importe_capital',
				render: function(data, type, row) {
					return '$' + data;
				}
			},
			{
				data: 'importe_interes',
				render: function(data, type, row) {
					return '$' + data;
				}
			}
		],
		buttons: [{
			extend: 'copy',
            text: 'Copiar',
		},
		$.extend( true, {}, buttonCommon, {
        	extend: 'excel',
        	text: 'Excel'
        }),
		{
			extend: 'print',
            text: 'Imprimir',
		}]
	});

	$('#generar-reporte').click(function () {
		if ($('#inversionista').val() === 0)
			return false;

		$.ajax({
			url: '/reportes/get_cartera_fecha',
			type: 'post',
			data: { 
				id: $('#inversionista').val(),
				fecha_start: $('#fecha_start').val(),
				fecha_end: $('#fecha_end').val()
			}
		}).done(function (data) {
			data = $.parseJSON(data);
				datatable.clear().draw();
				datatable.rows.add(data); // Add new data
				datatable.columns.adjust().draw(); // Redraw the DataTable
			// }

			var id = $('#inversionista').val();
			var fecha_start = $('#fecha_start').val();
			var fecha_end = $('#fecha_end').val();
			$.post('/reportes/get_total_cartera_fecha',{id:id,fecha_start:fecha_start,fecha_end:fecha_end}).done(function(data) {
				data = $.parseJSON(data);

				$('#totales').html('<h3 style="text-align:right; padding-right:10px;">Suma Capital: $'+data.total_capital+'</h3><h3 style="text-align:right; padding-right:10px;">Suma Interes: $'+data.total_interes+'</h3><h3 style="text-align:right; padding-bottom:10px; padding-right:10px;">Suma Total: $'+data.total+'</h3>');
			});

		});
	});

	var currentTime = new Date();
	// First Date Of the month 
	var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
	// Last Date Of the Month 
	//var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);


	$('#fecha_start').keydown(function(e) {
		return false;
	})
	.datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD',
		defaultDate: startDateFrom
	});	

	$('#fecha_end').keydown(function(e) {
		return false;
	})
	.datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD',
		defaultDate: currentTime

	});
})