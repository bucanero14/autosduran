$(document).ready(function (){
	var buttonCommon = {
		exportOptions: {
			format: {
				body: function (data, row, column, node) {
					return (column === 2 || column === 3) ? data.replace( /[$,]/g, '' ) : data;
				}
			}
		}
	};

	var datatable = $('.data-table').DataTable({
		dom: "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
		language: language,
		columns: [
			{data: 'codigo'},
			{data: 'nombre'},
			{
				data: 'monto',
				render: function(data, type, row) {
					return '$' + data;
				}
			},
			{
				data: 'saldo',
				render: function(data, type, row) {
					return '$' + data;
				}
			}
		],
		buttons: [{
			extend: 'copy',
            text: 'Copiar',
		},
		$.extend( true, {}, buttonCommon, {
        	extend: 'excel',
        	text: 'Excel'
        }),
		{
			extend: 'print',
            text: 'Imprimir',
		}]
	});

	$('#generar-reporte').click(function () {
		if ($('#inversionista').val() === 0)
			return false;

		//$('#cobranza').html('');
		$.ajax({
			url: '/reportes/get_cartera',
			type: 'post',
			data: { 
				id: $('#inversionista').val()
				/*fecha_start: $('#fecha_start').val(),
				fecha_end: $('#fecha_end').val()*/
			}
		}).done(function (data) {
			data = $.parseJSON(data);
				$('#info-box .box-body').html('<table class="table"><thead><tr><th>Nombre</th><th>Telefono</th><th>Movil</th><th>Total</th></tr></thead><tbody><tr><td>'+data[0]['nombre_inv']+'</td><td>'+data[0]['tel_inv']+'</td><td>'+data[0]['movil_inv']+'</td><td>'+data[0]['saldo_total']+'</td></tr></tbody></table>');
				datatable.clear().draw();
				datatable.rows.add(data); // Add new data
				datatable.columns.adjust().draw(); // Redraw the DataTable
			// }
		});
	});

	var currentTime = new Date();
	// First Date Of the month 
	var startDateFrom = new Date(currentTime.getFullYear(),currentTime.getMonth(),1);
	// Last Date Of the Month 
	//var startDateTo = new Date(currentTime.getFullYear(),currentTime.getMonth() +1,0);


	$('#fecha_start').keydown(function(e) {
		return false;
	})
	.datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD',
		defaultDate: startDateFrom
	});	

	$('#fecha_end').keydown(function(e) {
		return false;
	})
	.datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD',
		defaultDate: currentTime

	});
})