$(document).ready(function() {
	$.fn.dataTable.moment('DD/MM/YYYY');
	var buttonCommon = {
		exportOptions: {
			columns: [2, 3, 4, 5, 6, 7, 8],
			format: {
				body: function (data, row, column, node) {
					return (column === 6) ? data.replace( /[$,]/g, '' ) : data;
				}
			}
		}
	};

	var datatable = $('table').DataTable({
		language: language,
		dom: "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
		columns: [
			{data: 'id'},
			{
				className: 'details-control',
				data: null,
				orderable: false,
				defaultContent: '<i style="cursor:pointer; color:green;" class="fa fa-plus"></i>'
			},
			{data: 'nombre'},
			{data: 'telefono'},
			{data: 'movil'},
			{data: 'sucursal'},
			{data: 'fecha_pago'},
			{data: 'pagos_pendientes'},
			{
				data: 'total',
				render: function(data, type, row) {
					return '$' + data;
				}
			}
		],
		columnDefs: [
			{
				targets: [0],
				visible: false,
				searchable: false
			}
		],
		buttons: [{
			extend: 'copy',
            text: 'Copiar',
		},
		$.extend( true, {}, buttonCommon, {
        	extend: 'excel',
        	text: 'Excel'
        }),
		{
			extend: 'print',
            text: 'Imprimir',
		}]
	});

	// Add event listener for opening and closing details
    $('table tbody').on('click', 'td.details-control', function () {
    	var td = $(this);
        var tr = td.closest('tr');
        var row = datatable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            td.html('<i style="cursor:pointer; color:green;" class="fa fa-plus"></i>');
        }
        else {
        	$.ajax({
        		url: '/reportes/get_clientes_morosos_detalle',
				type: 'post',
				data: { 
					idcredito: row.data().id
				}
        	}).done(function (data) {
        		// Open this row
	            row.child(format(data)).show();
	            td.html('<i style="cursor:pointer; color:red;" class="fa fa-minus"></i>');
        	});
        }
    });

	$('#generar-reporte').click(function () {
		if ($('#sucursal').val() === 0)
			return false;

		$.ajax({
			url: '/reportes/get_clientes_morosos',
			type: 'post',
			data: { 
				idsucursal: $('#sucursal').val()
			}
		}).done(function (data) {
			data = $.parseJSON(data);
			if (data) {
				datatable.clear().draw();
				datatable.rows.add(data); // Add new data
				datatable.columns.adjust().draw(); // Redraw the DataTable
			}
		});
	});
})

function format(data) {
	data = $.parseJSON(data);
	var table = '<table class="table table-bordered table-hover table-responsive">';

	table += '<thead>';
	table += '<th>Monto</th>';
	table += '<th>Capital</th>';
	table += '<th>Interes</th>';
	table += '<th>FMD</th>';
	table += '<th>Interes FMD</th>';
	table += '<th>Fecha</th>';
	table += '</thead>';
	table += '<tbody>';
	for (var i = 0; i < data.length; i++) {
		table += '<tr>';
		table += '<td>$' + data[i].monto + '</td>';
		table += '<td>$' + data[i].capital + '</td>';
		table += '<td>$' + data[i].interes + '</td>';
		table += '<td>$' + data[i].fmd + '</td>';
		table += '<td>$' + data[i].interes_fmd + '</td>';
		table += '<td>' + data[i].fecha_limite + '</td>';
		table += '</tr>';
	}
	table += '</tbody>';

	table += '</table>';

	return table;
}