$(document).ready(function() {
	var buttonCommon = {
		exportOptions: {
			columns: [0, 1, 2, 3, 4],
			format: {
				body: function (data, row, column, node) {
					return (column === 2) ? data.replace( /[$,]/g, '' ) : data;
				}
			}
		}
	};

	var datatable = $('table').DataTable({
		language: language,
		dom: "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
		columns: [
			{data: 'recibo'},
			{data: 'nombre'},
			{
				data: 'monto',
				render: function(data, type, row) {
					return '$' + data;
				}
			},
			{data: 'tipo_pago'},
			{data: 'pendiente'},
			{
				data: 'opciones',
				render: function(data, type, row) {
					if ($('#fecha').val() == moment().format('YYYY-MM-DD'))
						return '<a class="btn btn-flat btn-danger"><i class="fa fa-trash"></i></a>';
					else
						return '<a class="btn btn-flat btn-success"><i class="fa fa-list"></i></a>';
				}
			}
		],
		buttons: [{
			extend: 'copy',
            text: 'Copiar',
		},
		$.extend( true, {}, buttonCommon, {
        	extend: 'excel',
        	text: 'Excel'
        }),
		{
			extend: 'print',
            text: 'Imprimir',
		}]
	});

	// Add event listener for opening and closing details
    $('table tbody').on('click', 'td > .btn-success', function () {
        var row = datatable.row($(this).closest('tr'));

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
        }
        else {
        	$.ajax({
        		url: '/reportes/get_detalle_pago',
				type: 'post',
				data: { 
					recibo: row.data().recibo
				}
        	}).done(function (data) {
        		// Open this row
	            row.child(format(data)).show();
        	});
        }
    });

	$('#generar-reporte').click(function () {
		if ($('#sucursal').val() === 0)
			return false;
		if ($('#fecha').val() === '')
			return false;

		$.ajax({
			url: '/reportes/get_pagos_del_dia',
			type: 'post',
			data: { 
				idsucursal: $('#sucursal').val(),
				fecha: $('#fecha').val()
			}
		}).done(function (data) {
			data = $.parseJSON(data);
			if (data) {

				datatable.clear().draw();
				datatable.rows.add(data); // Add new data
				datatable.columns.adjust().draw(); // Redraw the DataTable
			}
		});
	});

	$('#fecha').keydown(function(e) {
		return false;
	})
	.datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD'
	});

	// Add event listener for opening and closing details
    $('table tbody').on('click', 'td > .btn-danger', function () {
    	var row = datatable.row($(this).closest('tr'));

    	bootbox.confirm({
    		message: '¿Estás seguro que deseas eliminar este pago?',
    		buttons: {
    			confirm: {
		            label: 'Sí',
		            className: 'btn-success'
		        },
		        cancel: {
		            label: 'No',
		            className: 'btn-danger'
		        }
    		},
    		callback: function (result) {
		        if (result)
		        	window.location = '/reportes/delete_pago/' + row.data().recibo;
		    }
    	});
    });
})

function format(data) {
	data = $.parseJSON(data);
	var table = '<table class="table table-bordered table-hover table-responsive">';

	table += '<thead>';
	table += '<th>Documento</th>';
	table += '<th>Importe a Capital</th>';
	table += '<th>Importe a Interés</th>';
	table += '</thead>';
	table += '<tbody>';
	for (var i = 0; i < data.length; i++) {
		table += '<tr>';
		table += '<td>' + data[i].documento + '</td>';
		table += '<td>$' + data[i].capital + '</td>';
		table += '<td>$' + data[i].interes + '</td>';
		table += '</tr>';
	}
	table += '</tbody>';

	table += '</table>';

	return table;
}