
$(document).ready(function() {
	var date = $('.datepicker').val() == '' ? moment().add(1, 'M').format('YYYY-MM-DD') : $('.datepicker').val();

	$('.datepicker').datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD'
	}).keydown(function(e) {
		e.preventDefault();
	}).val(date);

	$('#cliente-check').change(function() {
		if ($(this).is(':checked'))
		{
			$('#aval\\[nombre\\]').val($('#cliente\\[nombre\\]').val());
			$('#aval\\[direccion\\]').val($('#cliente\\[direccion\\]').val());
			$('#aval\\[telefono\\]').val($('#cliente\\[telefono\\]').val());
			$('#aval\\[email\\]').val($('#cliente\\[email\\]').val());
			$('#aval\\[movil\\]').val($('#cliente\\[movil\\]').val());
		}
	});

	$('#credito\\[monto\\], #credito\\[enganche\\]')
	.change(function () {
		var enganche = 0;
		if (!isNaN($('#credito\\[enganche\\]').val()) && $('#credito\\[enganche\\]').val() !== '')
			enganche = parseInt($('#credito\\[enganche\\]').val());

		var monto = 0;
		if (!isNaN($('#credito\\[monto\\]').val()) && $('#credito\\[monto\\]').val() !== '')
			monto = parseInt($('#credito\\[monto\\]').val());

		var valor = monto + enganche;
		$('#vehiculo\\[valor\\]').val(valor);
	});

	$('#cliente-check').iCheck({
          checkboxClass: 'icheckbox_square-red',
          radioClass: 'iradio_square-red',
          increaseArea: '20%' // optional
        })
		.on('ifChecked', function (e) {
			$('#aval\\[nombre\\]').val($('#cliente\\[nombre\\]').val()).prop('readonly', 'true');
			$('#aval\\[direccion\\]').val($('#cliente\\[direccion\\]').val()).prop('readonly', 'true');
			$('#aval\\[telefono\\]').val($('#cliente\\[telefono\\]').val()).prop('readonly', 'true');
			$('#aval\\[email\\]').val($('#cliente\\[email\\]').val()).prop('readonly', 'true');
			$('#aval\\[movil\\]').val($('#cliente\\[movil\\]').val()).prop('readonly', 'true');
        })
        .on('ifUnchecked', function (e) {
        	$('#aval\\[nombre\\]').val('').removeAttr('readonly');
			$('#aval\\[direccion\\]').val('').removeAttr('readonly');
			$('#aval\\[telefono\\]').val('').removeAttr('readonly');
			$('#aval\\[email\\]').val('').removeAttr('readonly');
			$('#aval\\[movil\\]').val('').removeAttr('readonly');
        });

    $('form').submit(function() {

    	if ($('form').isValid()){
	    	$.ajax({
	    		url: '/creditos/confirmar_credito',
	    		type: 'post',
	    		data: $('form').serialize(),
	    	}).done(function(data) {
	    		bootbox.confirm({
	    		title: 'Confirmar información',
	    		message: data,
	    		size: 'large',
	    		buttons: {
	    			confirm: {
	    				label: 'Confirmar',
	    				className: 'btn-flat btn-success'
	    			},
	    			cancel: {
	    				label: 'Cancelar',
	    				className: 'btn-flat btn-danger pull-left'
	    			}
	    		},
	    		callback: function (result) {
	    			if (result){
	    				$('form').off('submit');
	    				$('form').submit();
	    			}
	    		}
	    	});
	    	});
	    }

    	return false;
    });

    $('#eliminar-credito').click(function(e) {
    e.preventDefault();
    $btn = $(this);
    bootbox.confirm({
      message: '¿Estás seguro que quieres eliminar este credito?',
      buttons: {
        confirm: {
          label: 'Aceptar',
          className: 'btn-success'
        },
        cancel: {
          label: 'Cancelar',
          className: 'btn-danger'
        }
      },
      callback: function(result) {
          if (result){
            
            window.location.href = $btn.attr('href');
          }
        }
    });
  });
})