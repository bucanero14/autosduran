$(document).ready(function() {
	$.fn.dataTable.moment('DD/MM/YYYY');

	$(document).on('click','.open-modal',function(e) {
		e.preventDefault();
		var href = $(this).attr('href');

		obj = bootbox.dialog({
		    message: '<div id="modal"></div>',
		    title: 'Recibo',
		    size: 'large',
		    closeButton: true,
		    onEscape: true,
		    buttons: {
		    	buttonName: {
		    		label: 'Imprimir',
		    		className: 'btn-flat btn-primary',
		    		callback: function() {
		    			$('.modal-body').print();
		    		}
		    	}
		    }
		});
		$('#modal').load(href,function() {
			//asignamos la funcion de pagar cuando en el click del boton
			$('.btn-pagar').on('click',pagar);
			$('.btn-cerrar').on('click',function() {
				obj.modal('hide');
			});
			$('.input-pagos').on('change',calc_cambio);
		});
		
	});

	$('#realizar-pago').click(function(e) {
		e.preventDefault();
		var href = $(this).attr('href');

		obj = bootbox.dialog({
		    message: '<div id="modal"></div>',
		    size: 'large',
		    closeButton: true,
		    onEscape: true
		});
		$('#modal').load(href,function() {
			//asignamos la funcion de pagar cuando en el click del boton
			$('.btn-pagar').on('click',pagar);
			$('.btn-cerrar').on('click',function() {
				obj.modal('hide');
			});
			$('.input-pagos').on('change',calc_cambio);
		});
	});

	load_list();
});//ready

function pagar() {
	//var idclient = $('.btn-pagar').data('id');
	// reset error array
	errors = [];

	if($('.form-pago').isValid('', conf, true) ) {
		//desactivamos el click del boton para que no se envien multiples solicitudes
		$('.btn-pagar').off('click');
		//si paso la validacion desactivamos el boton
		crear_pago();
	}
}

function calc_cambio() {

	var efectivo = $('#efectivo').val() > 0 ? $('#efectivo').val() : 0;
	var importe = $('#importe').val() > 0 ? $('#importe').val() : 0;
	var cambio = $('#cambio').val() > 0 ? $('#cambio').val() : 0;

	cambio = (efectivo - importe).toFixed(2);

	$('#cambio').val(cambio);
}

function crear_pago() {

	$.post('pagos_extra/add_pago',$('.form-pago').serialize(),function(data) {
		//tiramos una alerta si se ingreso el pago o no
		if (data.error==false)
		{
			$('#response .alert').addClass('alert-success');
			$('#response .alert').append('<span>Pago Realizado Correctamente.</span>');
			$('#response').show();
		}
		else
		{
			$('#response .alert').addClass('alert-danger');
			$('#response .alert').append('<span>El pago no se ha realizado correctamente.</span>');
			$('#response').show();

		}
	},'json');

}

function load_list() {
	$('#tabla-pagos-extra').load('pagos_extra/load_list',function() {
		$('table').DataTable({
		language: language,
		order: [[0, 'desc']],
		});
	});
}