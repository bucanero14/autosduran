$(document).ready(function() {
  $('table').DataTable({
    language: language,
    dom: "<'row'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
	buttons: [{
		text: '<i class="fa fa-plus"></i> Agregar Inversionista',
		action: function(e, dt, node, config) {
			window.location = 'inversionistas/crear';
		},
		className: 'btn btn-flat btn-success'
	}]
  });
})
