$(document).ready(function() {
	$.fn.dataTable.moment('DD/MM/YYYY');

	$('table').DataTable({
		language: language
	});

	$(document).on('click','.open-modal',function(e) {
		e.preventDefault();
		var href = $(this).attr('href');

		bootbox.dialog({
		    message: '<div id="modal"></div>',
		    title: 'Pago',
		    size: 'large',
		    closeButton: true,
		    onEscape: true
		});
		$('#modal').load(href,function() {
			//asignamos la funcion de pagar cuando en el click del boton
			$('.btn-pagar').on('click',pagar);
			$('#nota-credito').iCheck({
	          checkboxClass: 'icheckbox_square-red',
	          radioClass: 'iradio_square-red',
	          increaseArea: '20%' // optional
	        });
		});
		
	});

	$(document).on('change','.input-pagos',calc_cambio);

	$(document).on('click','.liquidacion',function(e) {
	    e.preventDefault();
	    var href = $(this).attr('href');
	    
	    bootbox.dialog({
	        message: '<div id="modal"></div>',
	        title: 'Liquidar Crédito',
	        size: 'large',
	        closeButton: true,
	        onEscape: true,
	        buttons: {
	          buttonName: {
	            label: 'Imprimir',
	            className: 'btn-flat btn-primary',
	            callback: function() {
	              $('.modal-body').print();
	            }
	          }
	        }
	    });
    	$('#modal').load(href);
	});
	

});//ready

function pagar() {
	var idclient = $('.btn-pagar').data('id');
	// reset error array
	errors = [];

	if($('.form-pago').isValid('', conf, true) ) {
		//desactivamos el click del boton para que no se envien multiples solicitudes
		$('.btn-pagar').off('click');
		//si paso la validacion desactivamos el boton
		crear_pago(idclient);
	}
}

function calc_cambio() {

	var efectivo = $('#efectivo').val() > 0 ? $('#efectivo').val() : 0;
	var importe = $('#importe').val() > 0 ? $('#importe').val() : 0;
	var cambio = $('#cambio').val() > 0 ? $('#cambio').val() : 0;

	cambio = (efectivo - importe).toFixed(2);

	$('#cambio').val(cambio);
}

function crear_pago(id) {

	var idclient = id;

	$.post('dashboard/add_pago/'+idclient,$('.form-pago').serialize(),function(data) {
		//tiramos una alerta si se ingreso el pago o no
		if (data.error==false)
		{
			bootbox.alert("Pago Realizado Correctamente.",function () {
				location.reload();
			});
		}
		else
		{
			bootbox.alert("Error, por favor intente de nuevo.",function () {
				location.reload();
			});

		}
	},'json');

}