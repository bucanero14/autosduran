$(document).ready(function() {
	$('.datepicker').datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD',
		minDate: $('.datepicker').val()
	});
})