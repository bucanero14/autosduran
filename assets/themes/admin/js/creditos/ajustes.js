$(document).ready(function() {
	$.fn.dataTable.moment('DD/MM/YYYY hh:mm:ss A');

	//Tabla de pagos pendientes
	$('#table-pagos-pendientes').dataTable({
		language: language,
		dom: 'tp',
		pageLength: 5
	});

	$(document).on('click','.btn-ajuste',function(e) {
		e.preventDefault();
		var href = $(this).attr('href');

		obj = bootbox.dialog({
		    message: '<div id="modal"></div>',
		    title: 'Ajustar Letra',
		    size: 'medium',
		    closeButton: true,
		    onEscape: true
		});

		$('#modal').load(href,function() {
			//asignamos la funcion de pagar cuando en el click del boton
			$('.btn-ajustar').on('click',ajuste);

			$('.btn-cerrar').on('click',function() {
				obj.modal('hide');
			});
		});
	});

	$(document).on('click','.btn-ajuste-interes',function(e) {
		e.preventDefault();
		var href = $(this).attr('href');

		obj = bootbox.dialog({
		    message: '<div id="modal"></div>',
		    title: 'Ajustar Letra',
		    size: 'medium',
		    closeButton: true,
		    onEscape: true
		});

		$('#modal').load(href,function() {
			//asignamos la funcion de pagar cuando en el click del boton
			$('.btn-ajustar').on('click',ajuste_interes);

			$('.btn-cerrar').on('click',function() {
				obj.modal('hide');
			});
		});
	});
})

function ajuste() {
	var id = $('.btn-ajustar').data('id');
	// reset error array
	errors = [];

	if($('#form-ajuste').isValid('', conf, true) ) {
		//desactivamos el click del boton para que no se envien multiples solicitudes
		$('.btn-ajustar').off('click');

		$.post('/creditos/set_adjustment/'+id,$('#form-ajuste').serialize(),function(data) {
			if (data.error==false)
			{
				bootbox.alert("Ajuste Realizado Correctamente.",function () {
					location.reload();
				});
			}
			else
			{
				bootbox.alert("Error, por favor intente de nuevo.",function () {
					location.reload();
				});
			}
		}, 'json');
	}
}

function ajuste_interes() {
	var id = $('.btn-ajustar').data('id');
	// reset error array
	errors = [];

	if($('#form-ajuste').isValid('', conf, true) ) {
		//desactivamos el click del boton para que no se envien multiples solicitudes
		$('.btn-ajustar').off('click');

		$.post('/creditos/set_adjustment_interest/'+id,$('#form-ajuste').serialize(),function(data) {
			if (data.error==false)
			{
				bootbox.alert("Ajuste Realizado Correctamente.",function () {
					location.reload();
				});
			}
			else
			{
				bootbox.alert("Error, por favor intente de nuevo.",function () {
					location.reload();
				});
			}
		}, 'json');
	}
}