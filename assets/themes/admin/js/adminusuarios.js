$(document).ready(function() {
  $('table').DataTable({
    language: language,
    dom: "<'row'<'col-sm-6'B><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
	buttons: [{
		text: '<i class="fa fa-plus"></i> Agregar Usuario',
		action: function(e, dt, node, config) {
			window.location = 'usuarios/crear';
		},
		className: 'btn btn-flat btn-success'
	}]
  });

  $('button[name="reset"]').click(function(e) {
    $btn = $(this);
    bootbox.confirm({
      message: '¿Estás seguro que deseas restablecer la contraseña de este usuario?',
      buttons: {
        confirm: {
          label: 'Aceptar',
          className: 'btn-success'
        },
        cancel: {
          label: 'Cancelar',
          className: 'btn-danger'
        }
      },
      callback: function(result) {
          if (result){
            $.ajax({
              url: '/usuarios/reset_password',
              type: 'post',
              dataType: 'json',
              data: {
                id: $btn.data('id')
              }
            }).done(function(data) {
              bootbox.alert(data.message);
            });
          }
        }
    });
  });

  $('button[name="delete"]').click(function(e) {
    $btn = $(this);
    bootbox.confirm({
      message: '¿Estás seguro que quieres inactivar a este usuario?',
      buttons: {
        confirm: {
          label: 'Aceptar',
          className: 'btn-success'
        },
        cancel: {
          label: 'Cancelar',
          className: 'btn-danger'
        }
      },
      callback: function(result) {
          if (result){
            $.ajax({
              url: '/usuarios/disable_user',
              type: 'post',
              dataType: 'json',
              data: {
                id: $btn.data('id')
              }
            }).done(function(data) {
              bootbox.alert('Usuario desactivado satisfactoriamente.');
            });
            //location.reload();
          }
        }
    });
  });
})
