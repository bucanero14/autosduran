# Autos Durán #
Autos Durán es un sistema de cobranza diseñado para el José Durán, quien tiene un lote de autos de medio uso del mismo nombre.

## Módulos ##
El sistema cuenta con 8 diferentes módulos que permitirán el óptimo uso del mismo:

* Caja
* Pagos Extraordinarios
* Créditos
* Inversionistas
* Sucursales
* Administrador de Usuarios
* Historial de Actividades
* Reportes

### Caja ###
La caja es el módulo en el que se registran pagos de los clientes que tienen un crédito o una mensualidad pendiente de pago.

### Pagos Extraordinarios ###
Los pagos extraordinarios son pagos que generan una entrada de dinero en la caja, pero que no tienen ninguna relación con algún crédito.

### Inversionistas ###
Los inversionistas son los organismos o las personas que se encargan de realizar la compra de un crédito. No puede existir un crédito sin un inversionista ligado a él. Este módulo es un control de altas, bajas y cambios para dichos inversionistas.

### Sucursales ###
Las sucursales son los diferentes puntos de cobro que el sistema tendrá. Todos los créditos tienen que estar ligados a la sucursal a la que pasará a pagar sus respectivas letras. Este módulo es un control de altas, bajas y cambios para dichas sucursales.

### Administrador de Usuarios ###
Los usuarios son los organismos o personas que se encargan de utilizar el sistema de cobranza. Los usuarios solo pueden tener uno de los diferentes roles que el sistema ofrece y solamente puede tener una sucursal asignada. Este módulo es un control de altas, bajas y cambios de dichos usuarios.

### Historial de Actividades ###
Este módulo se encarga de mantener un registro de todas las actividades que se realicen en el sistema. Por el momento solo registra la ejecución de los trabajos de cron.

### Reportes ###
Existen 5 reportes:

* Cobranza por Sucursal
* Clientes Morosos
* Pagos del Día
* Total Cartera
* Cartera por Fecha

## Roles ##
El sistema cuenta con 2 roles que permitiran diferentes actividades dentro del sistema:

* Cajero
    * Caja
    * Pagos Extraordinarios
* Administrador
    * Caja
    * Pagos Extraordinarios
    * Créditos
    * Inversionistas
    * Sucursales
    * Administrador de Usuarios
    * Historial de Actividades
    * Reportes

## Tareas Programadas ##
Para que el sistema pueda ofrecer los ajustes de pagos realizados y de cálculo de créditos, es necesario configurar un trabajo de Cron que apunte a la url [http://www.autosduran.com/cron/calculosDelDia](http://www.autosduran.com/cron/calculosDelDia) y que se ejecute al menos una vez cada 24 horas.