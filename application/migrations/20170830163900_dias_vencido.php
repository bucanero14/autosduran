<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_dias_vencido extends CI_Migration {

	public function __construct() 
	{
		parent::__construct();

		//$this->load->dbforge();
	}

	public function up()
	{
	    $fields = array(
			'dias_vencido INT DEFAULT 0'
		);

		$this->dbforge->add_column('pagos_pendientes', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('pagos_pendientes', 'dias_vencido');
	}

}