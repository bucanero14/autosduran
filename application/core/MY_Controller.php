<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
  public $user;
  public $viewmodel;

  function __construct() {
    parent::__construct();

    $this->load->library('ion_auth');

    if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
    if ($this->ion_auth->is_admin())
    {
      $this->load->section('admin_menu', 'themes/admin_menu');
      $this->viewmodel['isadmin'] = true;
    }

    $this->user = $this->ion_auth->user()->row();
    $this->viewmodel['username'] = $this->user->username;
    $this->viewmodel['user_full_name'] = $this->user->full_name;
  }

  function setHistorial($desc) {
    $this->load->model('historial_model');

    $username = $this->ion_auth->user()->row()->username;

    $this->historial_model->setHistorial($username, $desc);
  }

  //AJAX Function
  function change_password() {
    $this->output->unset_template();
    $new = $this->input->post('password');

    $this->load->model('Ion_auth_model');

    echo json_encode($this->ion_auth_model->reset_password($this->user->username, $new));
  }
}

class Admin_Controller extends MY_Controller
{
  function __construct(){
    parent::__construct();

    if (!$this->ion_auth->is_admin())
    {
      show_404();
    }
  }
}

class Cron_Controller extends CI_Controller
{
  function __construct() {
    parent::__construct();
  }

  function setHistorialSystem($desc) {
    $this->load->model('historial_model');

    $this->historial_model->setHistorial('Sistema', $desc);
  }
}