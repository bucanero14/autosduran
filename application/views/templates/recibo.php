<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Recibo <?php echo $client['nombre']?></title>
    
    <style>
    .invoice-box{
        max-width:800px;
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        box-shadow:0 0 10px rgba(0, 0, 0, .15);
        font-size:16px;
        line-height:24px;
        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color:#555;
    }
    
    .invoice-box table{
        width:100%;
        line-height:inherit;
        text-align:left;
    }
    
    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }
    
    .invoice-box table tr td:nth-child(2){
        text-align:right;
    }
    
    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.top table td.title{
        font-size:45px;
        line-height:45px;
        color:#333;
    }
    
    .invoice-box table tr.information table td{
        padding-bottom:40px;
    }
    
    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }
    
    .invoice-box table tr.details td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }
    
    .invoice-box table tr.item.last td{
        border-bottom:none;
    }
    
    .invoice-box table tr.total td:nth-child(2){
        border-top:2px solid #eee;
        font-weight:bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td{
            width:100%;
            display:block;
            text-align:center;
        }
        
        .invoice-box table tr.information table td{
            width:100%;
            display:block;
            text-align:center;
        }
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="<?php echo base_url('assets/img/logo.jpg')?>" style="">
                            </td>
                            
                            <td>
                                Recibo #: <?php echo $pago['id']?><br>
                                Fecha: <?php echo date('d/m/Y', strtotime($pago['fecha']))?><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                Autos Durán<br>
                                Calle 25 Diagonal x 40<br>
                                Esquina #377 Colonia Mérida<br>
                                Mérida, Yucatán
                            </td>
                            
                            <td>
                                <?php echo $client['nombre']?><br>
                                <?php echo $client['email']?><br>
                                <?php echo $client['inversionista']?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Método de Pago
                </td>
                
                <td>
                    
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    <?php
                    if ($pago['tipo_pago'] == 1)
                        echo 'Efectivo';
                    elseif ($pago['tipo_pago'] == 2)
                        echo 'Depósito Bancario';
                    elseif ($pago['tipo_pago'] == 3)
                        echo 'Nota de Crédito';
                    ?>
                </td>
                
                <td>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Concepto
                </td>
                
                <td>
                    Importe
                </td>
            </tr>
            
            <?php foreach($conceptos as $concepto) {?>
            <tr class="item">
                <td>
                    <?php echo $concepto['concepto']?>
                </td>
                
                <td>
                    $<?php echo number_format($concepto['monto'],2)?>
                </td>
            </tr>
            <?php }?>
            
            <tr class="total">
                <td></td>
                
                <td>
                   Total: $<?php echo number_format($pago['monto'],2)?>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>