<div class="row">
	<div class="col-md-12">
  	<div class="box box-danger">
  		<div class="box-body">
        <table class="table table-reponsive table-condensed">
        <thead>
          <tr>
            <th>Fecha</th>
            <th>Usuario</th>
            <th>Descripción</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($historial as $data) { ?>
          <?php $data['fecha'] = date_format(new DateTime($data['fecha']), 'd/m/Y h:i:s A');?>
            <tr>
            <td><?php echo $data['fecha']; ?></td>
            <td><?php echo $data['usuario']; ?></td>
            <td><?php echo $data['descripcion']; ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
  		</div>
    </div>
  </div>
</div>