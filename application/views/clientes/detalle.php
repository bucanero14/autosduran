<?if (isset($message) && $message != ''){?>
  <div class="alert alert-danger">
    <?echo $message;?>
  </div>
<?}?>
<?if (isset($success) && $success != ''){?>
  <div class="alert alert-success">
    <?echo $success;?>
  </div>
<?}?>
<div class="row">
	<div class="col-md-6">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3 class="box-title">Pagos Pendientes</h3>
			</div>
			<div class="box-body">
				<table id="table-pagos-pendientes" class="table table-condensed table-responsive">
					<thead>
						<th>Letra</th>
						<th>Fecha de Pago</th>
						<th>Capital</th>
						<th>Interés</th>
						<th>Interés FMD</th>
						<th>Total</th>
					</thead>
					<tbody>
						<?foreach($pagos_pendientes as $pago){?>
						<?php $pago['fecha_limite'] = date_format(new DateTime($pago['fecha_limite']), 'd/m/Y');?>
						<tr>
							<td><?echo $pago['documento']?></td>
							<td><?echo $pago['fecha_limite']?></td>
							<td>$<?echo number_format($pago['capital'], 2)?></td>
							<td>$<?echo number_format($pago['interes'], 2)?></td>
							<td>$<?echo number_format($pago['interes_fmd'], 2)?></td>
							<td>$<?echo number_format($pago['monto'], 2)?></td>
						</tr>
						<?}?>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-6">
						<h5>Total: <strong>$<?echo number_format($total_monto, 2)?></strong></h5>
					</div>
					<div class="col-md-6" style="text-align: right;">
						<a href="<?php echo base_url('clientes/pagar/' . $idcredito); ?>" class="btn btn-flat btn-success open-modal" data-toggle="tooltip" data-original-title="Pagar"><i class="fa fa-dollar"></i> Pagar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3 class="box-title">Pagos Realizados</h3>
			</div>
			<div class="box-body">
				<table id="table-pagos-realizados" class="table table-condensed table-responsive">
					<thead>
						<th>Fecha</th>
						<th>Sucursal</th>
						<th>Monto</th>
						<th>Tipo de Pago</th>
						<th>Opciones</th>
					</thead>
					<tbody>
						<?php foreach($pagos_realizados as $pago_realizado) {?>
						<?php $pago_realizado['fecha'] = date_format(new DateTime($pago_realizado['fecha']), 'd/m/Y');?>
						<tr>
							<td><?echo $pago_realizado['fecha']?></td>
							<td><?echo $pago_realizado['nombre']?></td>
							<td>$<?echo number_format($pago_realizado['monto'], 2)?></td>
							<td><?echo $pago_realizado['tipo_pago']?></td>
							<td><a target="_blank" data-toggle="tooltip" data-original-title="Ver Recibo" href="<?php echo base_url('clientes/recibos/' . $pago_realizado['id'])?>" class="btn btn-flat btn-default ver-recibo open-modal print"><i class="fa fa-file-text"></i></a></td>
						</tr>
						<?}?>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<div class="row">
					<div class="col-md-12" style="text-align: right;">
						<a href="<?php echo base_url('clientes/liquidar/' . $idcredito); ?>" class="pull-right btn btn-flat btn-info open-modal liquidacion" data-toggle="tooltip" data-original-title="Liquidar"><i class="fa fa-dollar"></i> Liquidar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3 class="box-title">Observaciones</h3>
			</div>
			<div class="box-body">
				<table data-id="<?php echo $idcliente?>" class="table table-condensed table-responsive table-observaciones">
					<thead>
						<th>Fecha</th>
						<th>Obervación</th>
					</thead>
					<tbody>
						<?foreach ($observaciones as $observacion) {?>
						<?php $observacion['fecha'] = date_format(new DateTime($observacion['fecha']), 'd/m/Y h:i:s A');?>
						<tr>
							<td><?echo $observacion['fecha']?></td>
							<td><?echo $observacion['observacion']?></td>
						</tr>
						<?}?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>