<? if ($title!='') { ?>
<h1>
  <?php echo $title; ?>
  <small><?php echo $desc; ?></small>
</h1>
<? } ?>
<? if ($breadcrumb_header!='') { ?>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa <?php echo $icon_class;?>"></i> <?php echo $breadcrumb_header;?></a></li>
    <?php
      $numItems = count($breadcrumb_detail);
      $i = 0;
      foreach($breadcrumb_detail as $index => $value) {
      $last_index = ++$i === $numItems;
    ?>
      <li class="<?php echo $last_index ? 'active' : ''; ?>"><?php echo $value?></li>
    <?php } ?>
  </ol>
<? } ?>

