<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="theme-color" content="#3C8DBC" />
    <title>Autos Durán</title>

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url();?>assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url();?>assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url();?>assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url();?>assets/ico/apple-touch-icon-57-precomposed.png">
    <?php
      /** -- Copy from here -- */
      if(!empty($meta))
      foreach($meta as $name=>$content){
        echo "\n\t\t";
        ?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
           }
      echo "\n";

      if(!empty($canonical))
      {
        echo "\n\t\t";
        ?><link rel="canonical" href="<?php echo $canonical?>" /><?php

      }
      echo "\n\t";
      /** -- to here -- */
    ?>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/themes/admin/css/AdminLTE.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/themes/admin/css/skin-blue.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/pace/pace.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datetimepicker/datetimepicker.css'); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <?php
      foreach($css as $file){
        echo "\n\t\t";
        ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
      } echo "\n\t";
    ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini fixed">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">
            <b>A</b>D
          </span>

          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">
            <b>Autos</b> Durán
          </span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <i class="fa fa-user"></i> <?php echo $username;?>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <p>
                    <?php echo $user_full_name;?>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                  <div class="pull-left"><button id="change-password" class="btn btn-default btn-flat">Cambiar Contraseña</button></div>
                    <div class="pull-right">
                      <a href="<?php echo site_url('auth/logout');?>" class="btn btn-default btn-flat">Cerrar Sesión</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">MENÚ</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="<?php echo '' == 'home' ? 'active' : ''?>"><a href="<?php echo site_url('dashboard')?>"><i class="fa fa-dashboard"></i> <span>Caja</span></a></li>
            <li class="<?php echo '' == 'pagos_extra' ? 'active' : ''?>">
              <a href="<?php echo site_url('pagos_extra')?>">
                <i class="fa fa-dollar"></i> <span>Pagos Extraordinarios</span>
              </a>
            </li>
            <?php echo $this->load->get_section('admin_menu'); ?>
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <?php echo $this->load->get_section('text_header'); ?>
        </section>

        <!-- Main content -->
        <section class="content">

          <?php echo $output; ?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- Default to the left -->
        <strong>Copyright&copy; <?php echo (new DateTime())->format('Y'); ?> Autos Durán.</strong>
      </footer>
    </div><!-- ./wrapper -->
    <div id="box-widget"></div>

    <!-- REQUIRED JS SCRIPTS -->
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/PDFObject/pdfobject.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jQuery-2.1.4.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootbox.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/datetimepicker/moment.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/datetimepicker/datetimepicker.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/datetimepicker/datetimepicker.es.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/pace/pace.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/retinajs/retina.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/themes/admin/js/app.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/form-validator/jquery.form-validator.min.js'); ?>"></script>     
    <script type="text/javascript" src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
    <script type="text/javascript">
    $(document).ajaxStart(function() { Pace.restart(); })
    $(document).ready(function() {
      $('#change-password').click(function() {
        bootbox.prompt({
          title: "Escribe tu nueva contraseña",
          inputType: 'password',
          callback: function(result) {
            if (result != '') {
              $.ajax({
                  url: '/usuarios/change_password',
                  type: 'post',
                  dataType: 'json',
                  data: {
                    password: result
                  }
                }).done(function(data) {
                  bootbox.alert("Contraseña cambiada satisfactoriamente.");
                });
            }
            else
            {
              bootbox.alert("Contraseña no válida.");
            }
          }
        })
      })
    })
    </script>
    <?php
    foreach($js as $file){
        echo "\n\t\t";
        ?><script type="text/javascript" src="<?php echo $file; ?>"></script><?php
    } echo "\n\t";
    ?>
    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
  </body>
</html>
