<li class="header">ADMINISTRACIÓN</li>
<li class="<?php echo '' == 'creditos' ? 'active' : ''?>">
	<a href="<?php echo site_url('creditos')?>">
		<i class="fa fa-credit-card-alt"></i> <span>Créditos</span>
	</a>
</li>
<li class="<?php echo '' == 'inversionistas' ? 'active' : ''?>">
	<a href="<?php echo site_url('inversionistas')?>">
		<i class="fa fa-money"></i> <span>Inversionistas</span>
	</a>
</li>
<li class="<?php echo '' == 'usuarios' ? 'active' : ''?>">
	<a href="<?php echo site_url('sucursales')?>">
		<i class="fa fa-bank"></i> <span>Sucursales</span>
	</a>
</li>
<li class="<?php echo '' == 'usuarios' ? 'active' : ''?>">
	<a href="<?php echo site_url('usuarios')?>">
		<i class="fa fa-users"></i> <span>Administrador de Usuarios</span>
	</a>
</li>
<li class="<?php echo '' == 'usuarios' ? 'active' : ''?>">
	<a href="<?php echo site_url('historial')?>">
		<i class="fa fa-hourglass-half"></i> <span>Historial de Actividades</span>
	</a>
</li>
<li class="<?php echo '' == 'reportes' ? 'active' : ''?>">
	<a href="<?php echo site_url('reportes')?>">
		<i class="fa fa-table"></i> <span>Reportes</span>
    </a>
</li>