<div class="row">
	<div class="col-md-12">
		<?php echo form_open('clientes/add_pago', array('class' => 'form-pago'))?>
		<input type="hidden" id="idcredito" value="<?php echo $data->id?>">
			<div class="col-md-6">
				<div class="form-group">
					<label for="">Importe</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-dollar"></i></div>
						<input type="text" class="form-control input-pagos" data-validation="number" value="<? echo $data->monto; ?>" data-validation-allowing="positive,float" id="importe" name="data[importe]">
					</div>
				</div>
				<div class="form-group">
					<label for="">Dinero Recibido</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-dollar"></i></div>
						<input type="text" class="form-control input-pagos" data-validation="number" data-validation-allowing="positive,float" id="efectivo" name="data[efectivo]">
					</div>
				</div>
				<div class="col-md-6" style="padding-left:0;">
					<div class="form-group">
						<label for="">Forma de Pago</label>
						<select name="data[tipo_pago]" id="tipo-pago" class="form-control">
							<option value="1">Efectivo</option>
							<option value="2">Depósito Bancario</option>
							<option value="3">Nota de Crédito</option>
						</select>
					</div>
				</div>
				<div class="col-md-6" style="padding-right:0;">
					<div class="form-group">
						<label for="">Tipo de Pago</label>
						<select name="data[pendiente]" id="pendiente" class="form-control">
							<option value="0">Letra Vencida</option>
							<?php if (!$liquidacion) {?>
								<option value="1">Letra Adelantada</option>
								<option value="2">Letra Anticipada</option>
							<?php }?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="">Cambio</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-dollar"></i></div>
						<input type="text" class="form-control input-pagos" data-validation="number" data-validation-allowing="positive,float" readonly="true" id="cambio" name="data[cambio]">
					</div>
				</div>
				<input type="hidden" name="data[id]" value="<?php echo $data->idcliente; ?>" />
				<button type="submit" class="btn btn-flat btn-primary btn-pagar pull-right" data-id="" >Pagar</button>
			</div>
			<div class="col-md-6">
				<div class="">
					<h4 class="">Pagos Pendientes</h4>
				</div>
				<div class="">
					<table id="table-pagos-pendientes-modal" class="table table-condensed table-responsive">
						<thead>
							<th>Letra</th>
							<th>Fecha de Pago</th>
							<th>Capital</th>
							<th>Interés</th>
							<th>Interés FMD</th>
							<th>Total</th>
						</thead>
						<tbody>
							<?foreach($pagos_pendientes as $pago){?>
							<?php $pago['fecha_limite'] = date_format(new DateTime($pago['fecha_limite']), 'd/m/Y');?>
							<tr>
								<td><?echo $pago['documento']?></td>
								<td><?echo $pago['fecha_limite']?></td>
								<td>$<?echo number_format($pago['capital'], 2)?></td>
								<td>$<?echo number_format($pago['interes'], 2)?></td>
								<td>$<?echo number_format($pago['interes_fmd'], 2)?></td>
								<td>$<?echo number_format($pago['monto'], 2)?></td>
							</tr>
							<?}?>
						</tbody>
					</table>
				</div>
				<div class="box-footer">
					<div class="row">
						<div class="col-md-6">
							<h5>Total: <strong id="total">$<?echo number_format($total_monto, 2)?></strong></h5>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>