<?if (isset($message) && $message != ''){?>
  <div class="alert alert-danger">
    <?echo $message;?>
  </div>
<?}?>
<?if (isset($success) && $success != ''){?>
  <div class="alert alert-success">
    <?echo $success;?>
  </div>
<?}?>

<div class="row">
	<div class="col-md-12">
	<div class="box box-danger">
		<div class="box-body">
			<table class="table table-reponsive table-condensed">
            <thead>
              <tr>
                <th>Credito</th>
                <th>Cliente</th>
                <th>Telefono</th>
                <th>Celular</th>
                <th>Fecha Límite</th>
                <th>Importe</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($pagos as $data) { 
                $data['fecha_limite'] = date_format(new DateTime($data['fecha_limite']), 'd/m/Y');
              ?>
                <tr>
                  <td><?php echo $data['codigo']; ?></td>
                  <td><?php echo $data['nombre']; ?></td>
                  <td><?php echo $data['telefono']; ?></td>
                  <td><?php echo $data['movil']; ?></td>
                  <td><?php echo $data['fecha_limite']; ?></td>
                  <td><?php echo '$'.number_format($data['monto'],2); ?></td>
                  <td>
                    <a href="<?php echo base_url('clientes/detalle/' . $data['id']); ?>" class="btn btn-flat btn-default" data-toggle="tooltip" data-original-title="Detalles"><i class="fa fa-list"></i></a>
                    <a href="<?php echo base_url('dashboard/liquidacion/' . $data['pp_id']); ?>" class="btn btn-flat btn-success liquidacion" data-toggle="tooltip" data-original-title="Liquidación"><i class="fa fa-check-square-o"></i></a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
		</div>
</div>