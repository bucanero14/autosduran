<?if (isset($message) && $message != ''){?>
  <div class="alert alert-danger">
    <?echo $message;?>
  </div>
<?}?>
<?if (isset($success) && $success != ''){?>
  <div class="alert alert-success">
    <?echo $success;?>
  </div>
<?}?>
<div class="row">
	<div class="col-md-12">
  	<div class="box box-danger">
  		<div class="box-body">
              <table class="table table-reponsive table-condensed">
              <thead>
                <tr>
                  <th>#Credito</th>
                  <th>Cliente</th>
                  <th>Telefono</th>
                  <th>Status</th>
                  <th>Saldo</th>
                  <th>Monto</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($creditos as $data) { ?>
                  <tr>
                  <td><?php echo $data['codigo']; ?></td>
                  <td><?php echo $data['nombre']; ?></td>
                    <td><?php echo $data['telefono']; ?></td>
                    <td><?php 
                      if ($data['status']==0) { echo '<span class="label label-success">Activo</span>'; } 
                      if ($data['status']==1) { echo '<span class="label label-info">Liquidado</span>'; } 
                      if ($data['status']==2) { echo '<span class="label label-warning">Trunco</span>'; } 
                    ?></td>
                    <td>$<?php echo number_format($data['saldo'], 2); ?></td>
                    <td>$<?php echo number_format($data['monto'], 2); ?></td>
                    
                    <td><a href="<?php echo base_url('creditos/editar/' . $data['id']); ?>" class="btn btn-flat btn-warning" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                    <a href="<?php echo base_url('creditos/detalles/' . $data['id']); ?>" class="btn btn-flat btn-info open-modal" data-toggle="tooltip" data-original-title="Info"><i class="fa fa-info"></i></a>
                    <a href="<?php echo base_url('creditos/pagos_pendientes/' . $data['id']); ?>" class="btn btn-flat btn-primary pagos-pendientes" data-toggle="tooltip" data-original-title="Pagos Pendientes"><i class="fa fa-list"></i></a>
                    <a href="<?php echo base_url('creditos/liquidacion/' . $data['id']); ?>" class="btn btn-flat btn-success liquidacion" data-toggle="tooltip" data-original-title="Liquidación"><i class="fa fa-check-square-o"></i></a>
                    <a href="<?php echo base_url('creditos/truncate/' . $data['id']); ?>" class="btn btn-flat btn-danger btn-truncate" data-toggle="tooltip" data-original-title="Truncar"><i class="fa fa-ban"></i></a>
                    <a href="<?php echo base_url('creditos/ajustes/' . $data['id']); ?>" class="btn btn-flat btn-default" data-toggle="tooltip" data-original-title="Ajustes"><i class="fa fa-adjust"></i></a></td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
  		</div>
    </div>
  </div>
</div>