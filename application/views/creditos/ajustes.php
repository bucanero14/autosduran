<div class="row">
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3 class="box-title">
					<?php echo $cliente->nombre; ?>
				</h3>
			</div>
			<div class="box-body">
				<table id="table-pagos-pendientes" class="table table-condensed table-responsive">
					<thead>
						<th>Letra</th>
						<th>Fecha de Pago</th>
						<th>Capital</th>
						<th>Interés</th>
						<th>Interés FMD</th>
						<th>Total</th>
						<th>Opciones</th>
					</thead>
					<tbody>
						<?foreach($pagos_pendientes as $pago){?>
						<?php $pago['fecha_limite'] = date_format(new DateTime($pago['fecha_limite']), 'd/m/Y');?>
						<tr>
							<td><?echo $pago['documento']?></td>
							<td><?echo $pago['fecha_limite']?></td>
							<td>$<?echo number_format($pago['capital'], 2)?></td>
							<td>$<?echo number_format($pago['interes'], 2)?></td>
							<td>$<?echo number_format($pago['interes_fmd'], 2)?></td>
							<td>$<?echo number_format($pago['monto'], 2)?></td>
							<td>
								<a href="<?php echo base_url('creditos/form_ajustes/' . $pago['id']); ?>" class="btn btn-flat btn-warning btn-ajuste" data-toggle="tooltip" data-original-title="Ajustar FMD"><i class="fa fa-adjust"></i></a>
								<a href="<?php echo base_url('creditos/form_ajustes_interes/' . $pago['id']); ?>" class="btn btn-flat btn-danger btn-ajuste-interes" data-toggle="tooltip" data-original-title="Ajustar Interés"><i class="fa fa-bank"></i></a>
							</td>
						</tr>
						<?}?>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
			</div>
		</div>
	</div>
</div>