	<table id="pagos" class="table table-striped table-responsive">
		<thead>
			<th>Documento</th>
			<th>Mensualidad</th>
			<th>FMD</th>
			<th>Capital</th>
			<th>Interés</th>
			<th>Interés FMD</th>
			<th>Fecha límite</th>
		</thead>
		<tbody>
			<?php foreach ($payments as $payment) { ?>
			<?php $payment['fecha_limite'] = date_format(new DateTime($payment['fecha_limite']), 'd/m/Y')?>
			<tr>
				<td><?php echo $payment['documento']?></td>
				<td>$<?php echo number_format($payment['monto'], 2)?></td>
				<td>$<?php echo number_format($payment['fmd'], 2)?></td>
				<td>$<?php echo number_format($payment['capital'], 2)?></td>
				<td>$<?php echo number_format($payment['interes'], 2)?></td>
				<td>$<?php echo number_format($payment['interes_fmd'], 2)?></td>
				<td><?php echo $payment['fecha_limite']?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
