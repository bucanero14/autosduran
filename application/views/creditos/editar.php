<?
if (isset($message) && $message != ''){?>
  <div class="alert alert-danger">
    <?echo $message;?>
  </div>
<?}?>
<?if (isset($success) && $success != ''){?>
  <div class="alert alert-success">
    <?echo $success;?>
  </div>
<?}?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-body">
			<?php echo form_open(base_url($submit_url), array('class' => 'form'));?>
			<div class="row">
				<div class="col-md-3 ">
					<h2>Info</h2>
					<div class="form-group">
						<div class="">
							<label for="">Fecha de próximo pago</label>
							<?php echo form_input($data['fecha_pago']); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<label for="">Sucursal</label>
							<?php echo form_dropdown($data['sucursales'],$sucursales,$select['sucursal']); ?>
						</div>
					</div>
				</div>
			</div>
	       	<div class="row col-md-12">
       		<?php 
				echo form_submit(array('class' => 'btn btn-flat btn-success pull-left', 'value' => 'Guardar'));
			?>
	       	</div>		
			<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>