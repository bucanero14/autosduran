<div class="row">
	<div class="col-md-12">
		<ul>
			<li><strong>Letra:</strong> <?php echo $documento; ?></li>
			<li><strong>Capital:</strong> $<?php echo number_format($capital, 2); ?></li>
			<li><strong>Interés:</strong> $<?php echo number_format($interes, 2); ?></li>
			<li><strong>Interés FMD:</strong> $<?php echo number_format($interes_fmd, 2); ?></li>
			<li><strong>Total:</strong> $<?php echo number_format($monto, 2); ?></li>
		</ul>

		<form id="form-ajuste" action="">
			<div class="form-group">
				<label for="">Interés</label>
				<div class="input-group">
					<div class="input-group-addon"><i class="fa fa-dollar"></i></div>
					<input type="text" class="form-control input-pagos" data-validation="number" value="<? echo $interes; ?>" data-validation-allowing="positive,float" id="monto" name="monto">
				</div>
			</div>

			<button type="button" class="btn btn-flat btn-primary btn-ajustar pull-right" data-id="<?php echo $id; ?>" >Ajustar</button>
		</form>
	</div>
</div>