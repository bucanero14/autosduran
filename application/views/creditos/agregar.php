<?
if (isset($message) && $message != ''){?>
  <div class="alert alert-danger">
    <?echo $message;?>
  </div>
<?}?>
<?if (isset($success) && $success != ''){?>
  <div class="alert alert-success">
    <?echo $success;?>
  </div>
<?}?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<?php if (strpos($submit_url, 'editar/') !== false) {?>
						<a id="eliminar-credito" href="<?php echo base_url('creditos/eliminar/'.$id); ?>" class="btn btn-flat btn-danger pull-right"><i class="fa fa-times"></i> Eliminar Crédito</a>
					<?php } ?>
				</div>
			</div>
			<?php echo form_open(base_url($submit_url), array('class' => 'form'), $hidden); ?>
			<div class="row">
				<div class="col-md-5">
					<h2>Info</h2>
					<div class="form-group">
						<div class="">
							<label for="">Código</label>
							<?php echo form_input($data['codigo']); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<label for="">Tasa del FMD</label>
							<div class="input-group">
								<?php echo form_input($data['tasa_fmd']); ?>
								<div class="input-group-addon"><i class="fa fa-percent"></i></div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<label for="">Inversionista</label>
							<?php echo form_dropdown($data['inversionistas'],$inversionistas, $idinversionista != null ? $idinversionista : set_value('data[inversionistas]')); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<label for="">Letras Pagadas</label>
							<?php echo form_input($data['letras_pagadas']); ?>
						</div>
					</div>
				</div>
				<div class="col-md-5 col-md-offset-2">
					<h2>&nbsp;</h2>
					<div class="form-group">
						<div class="">
							<label for="">Fecha de primer pago</label>
							<?php echo form_input($data['fecha_inicial']); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<label for="">Sucursal</label>
							<?php echo form_dropdown($data['sucursales'],$sucursales, $idsucursal != null ? $idsucursal : set_value('data[sucursales]')); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<label for="">Letras Adelantadas</label>
							<?php echo form_input($data['letras_adelantadas']); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					<h2>Cliente</h2>
					<div class="form-group">
						<div class="">
							<label for="">Nombre</label>
							<?php echo form_input($cliente['nombre']); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<label for="">Dirección</label>
							<?php echo form_input($cliente['direccion']); ?>
						</div>
					</div>	
					<div class="form-group">
						<div class="">
							<label for="">Telefono</label>
							<?php echo form_input($cliente['telefono']); ?>
						</div>
					</div>		
					<div class="form-group">
						<div class="">
							<label for="">Correo</label>
							<?php echo form_input($cliente['email']); ?>
						</div>
					</div>			
					<div class="form-group">
						<div class="">
							<label for="">Movil</label>
							<?php echo form_input($cliente['movil']); ?>
						</div>
					</div>
					<div class="checkbox">
						<label style="padding-left:inherit;">
					    	<input id="cliente-check" type="checkbox"> Usar los datos del cliente para el aval
					    </label>
					</div>

				</div>
				<div class="col-md-5 col-md-offset-2">
					<h2>Aval</h2>
					<div class="form-group">
						<div class="">
							<label for="">Nombre</label>
							<?php echo form_input($aval['nombre']); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<label for="">Dirección</label>
							<?php echo form_input($aval['direccion']); ?>
						</div>
					</div>	
					<div class="form-group">
						<div class="">
							<label for="">Telefono</label>
							<?php echo form_input($aval['telefono']); ?>
						</div>
					</div>		
					<div class="form-group">
						<div class="">
							<label for="">Correo</label>
							<?php echo form_input($aval['email']); ?>
						</div>
					</div>			
					<div class="form-group">
						<div class="">
							<label for="">Movil</label>
							<?php echo form_input($aval['movil']); ?>
						</div>
					</div>       	
		       	</div>
	       	</div>
	       	<div class="row">
		       	<div class="col-md-5">
		       		<h2>Credito</h2>
		       		<div class="form-group">
						<div class="">
							<label for="">Monto a Financiar</label>
							<div class="input-group">
								<div class="input-group-addon"><i class="fa fa-dollar"></i></div>
								<?php echo form_input($credito['monto']); ?>
							</div>
						</div>
					</div>  
					<div class="form-group">
						<div class="">
							<label for="">Plazo</label>
							<?php echo form_input($credito['plazo']); ?>
						</div>
					</div>   
					<div class="form-group">
						<div class="">
							<label for="">Tasa</label>
							<div class="input-group">
								<?php echo form_input($credito['tasa']); ?>
								<div class="input-group-addon"><i class="fa fa-percent"></i></div>
							</div>
						</div>
					</div>   
					<div class="form-group">
						<div class="">
							<label for="">Enganche</label>
							<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-dollar"></i></div>
							<?php echo form_input($credito['enganche']); ?>
							</div>
						</div>
					</div>  		       	
		       	</div>
		       	<div class="col-md-5 col-md-offset-2">
		       		<h2>Vehiculo</h2>
		       		<div class="form-group">
						<div class="">
							<label for="">Marca</label>
							<?php echo form_input($vehiculo['marca']); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<label for="">Modelo</label>
							<?php echo form_input($vehiculo['modelo']); ?>
						</div>
					</div>
					<div class="form-group">
						<div class="">
							<label for="">Año</label>
							<?php echo form_input($vehiculo['year']); ?>
						</div>
					</div> 
					<div class="form-group">
						<div class="">
							<label for="">Valor del Vehículo</label>
							<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-dollar"></i></div>
							<?php echo form_input($vehiculo['valor']); ?>
							</div>
						</div>
					</div>    		       	
		       	</div>
	       	</div>
	       	<div class="row col-md-12">
       		<?php 
				echo form_submit(array('class' => 'btn btn-flat btn-success pull-left', 'value' => 'Guardar'));
			?>
	       	</div>		
			<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>