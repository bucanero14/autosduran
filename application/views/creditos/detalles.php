<?
 /* $fecha_inicial = date_create($info->fecha_inicial);
  $fecha_inicial = date_format($fecha_inicial,'F j, Y');*/
?>

<div class="row">
	<div class="col-md-12">
    <div class="col-md-12">
      <h3>Información General</h3>
      <ul class="list-unstyled col-md-4">
         <li class=""><strong>Inversionista:</strong> <? echo $info->inversionista; ?></li>
      </ul> 
      <ul class="list-unstyled col-md-4">
        <li class=""><strong>Sucursal:</strong> <? echo $info->sucursal; ?></li>
      </ul>
      <ul class="list-unstyled col-md-4">
        <li><strong>Tasa FMD:</strong> <? echo $info->tasa_fmd; ?>%</li>
      </ul>
    </div>
  	<div class="col-md-6">
      <h3>Cliente</h3>
      <ul class="list-unstyled">
         <li><strong>Nombre:</strong> <? echo $info->cnombre; ?></li>
         <li><strong>Dirección:</strong> <? echo $info->cdireccion; ?></li>
         <li><strong>Tel:</strong> <? echo $info->ctelefono; ?></li>
         <li><strong>Cel:</strong> <? echo $info->cmovil; ?></li>
      </ul> 
    </div>
    <div class="col-md-6">
      <h3>Aval</h3>
      <ul class="list-unstyled">
         <li><strong>Nombre:</strong> <? echo $info->anombre; ?></li>
         <li><strong>Dirección:</strong> <? echo $info->adireccion; ?></li>
         <li><strong>Tel:</strong> <? echo $info->atelefono; ?></li>
         <li><strong>Cel:</strong> <? echo $info->amovil; ?></li>
      </ul> 
    </div>
    <div class="col-md-12">
      <h3>Datos del Vehiculo</h3>
      <ul class="list-unstyled">
         <li class="col-md-4" style="padding: 0"><strong>Marca:</strong> <? echo $info->marca_vehiculo; ?></li>
         <li class="col-md-4" style="padding: 0"><strong>Model:</strong> <? echo $info->modelo_vehiculo; ?></li>
         <li class="col-md-4" style="padding: 0"><strong>Año:</strong> <? echo $info->year_vehiculo; ?></li>
       </ul> 
    </div>
    <div class="col-md-12">
      <h3>Datos del Financiamiento</h3>
      <ul class="list-unstyled col-md-4">
         <li class=""><strong>Plazo:</strong> <? echo $info->plazo; ?></li>
         <li class=""><strong>Precio:</strong> $<? echo number_format($info->valor_vehiculo,2); ?></li>
      </ul> 
      <ul class="list-unstyled col-md-4">
         <li class=""><strong>Int. Mensual:</strong> <? echo $info->tasa.'%'; ?></li>
         <li class=""><strong>Enganche:</strong> $<? echo number_format($info->enganche,2); ?></li>
      </ul>
      <ul class="list-unstyled col-md-4">
         <li class=""><strong>Fecha de Inicio:</strong> <? echo $this->custom_functions->fecha_completa($info->fecha_inicial); ?></li>
         <li class=""><strong>Saldo a Financiar:</strong> $<? echo number_format($info->monto,2); ?></li>
      </ul> 
    </div>
  </div>
</div>