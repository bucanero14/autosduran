<?if (isset($message) && $message != ''){?>
  <div class="alert alert-danger">
    <?echo $message;?>
  </div>
<?}?>
<?if (isset($success) && $success != ''){?>
  <div class="alert alert-success">
    <?echo $success;?>
  </div>
<?}?>
<div class="row">
	<div class="col-md-12">
	<div class="box box-danger">
		<div class="box-body">
			<table class="table table-reponsive table-condensed">
            <thead>
              <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($branches as $branch) { ?>
                <tr>
                <td><?php echo $branch['id']; ?></td>
                  <td><?php echo $branch['nombre']; ?></td>
                  <td>
                  	<a href="<?php echo base_url('sucursales/editar/' . $branch['id']); ?>" class="btn btn-flat btn-warning" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
		</div>
</div>