<?if (isset($message) && $message != ''){?>
  <div class="alert alert-danger">
    <?echo $message;?>
  </div>
<?}?>
<?if (isset($success) && $success != ''){?>
  <div class="alert alert-success">
    <?echo $success;?>
  </div>
<?}?>
<div class="row">
	<div class="col-md-12">
  	<div class="box box-danger">
  		<div class="box-body">
  			<table class="table table-reponsive table-condensed">
              <thead>
                <tr>
                  <th>Usuario</th>
                  <th>Nombre</th>
                  <th>Sucursal</th>
                  <th>Rol</th>
                  <th>Opciones</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($users as $user) { ?>
                  <tr>
                  <td><?php echo $user['username']; ?></td>
                    <td><?php echo $user['full_name']; ?></td>
                    <td><?php echo $user['nombre']; ?></td>
                    <td><?php echo $user['rolename']; ?></td>
                    <td>
                    	<button name="reset" data-id="<?php echo $user['id']; ?>" class="btn btn-flat btn-default" data-toggle="tooltip" data-original-title="Restablecer Contraseña"><i class="fa fa-key"></i></button>
                    	<a href="<?php echo base_url('usuarios/editar/' . $user['id']); ?>" class="btn btn-flat btn-warning" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                    	<button name="delete" data-id="<?php echo $user['id']; ?>" data-toggle="tooltip" data-original-title="Desactivar" class="btn btn-flat btn-danger"><i class="fa fa-trash-o"></i></button>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
  		</div>
    </div>
  </div>
</div>