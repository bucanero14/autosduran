<div id="response" style="display: none">
	<div class="alert">

   	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<form action="" class="form-pago">
			<div class="col-md-12">
				
				<div class="form-group">
					<label for="">Monto a Pagar</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-dollar"></i></div>
						<input type="text" class="form-control input-pagos" data-validation="number" data-validation-allowing="positive,float" id="importe" name="data[importe]">
					</div>
				</div>
				<div class="form-group">
					<label for="">Dinero Recibido</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-dollar"></i></div>
						<input type="text" class="form-control input-pagos" data-validation="number" data-validation-allowing="positive,float" id="efectivo" name="data[efectivo]">
					</div>
				</div>
				<div class="form-group">
					<label for="">Cambio</label>
					<div class="input-group">
						<div class="input-group-addon"><i class="fa fa-dollar"></i></div>
						<input type="text" class="form-control input-pagos" data-validation="number" data-validation-allowing="positive,float" readonly="true" id="cambio" name="data[cambio]">
					</div>
				</div>
				<div class="form-group">
					<label for="">Descripción</label>
					<input type="text" class="form-control input-pagos" name="data[descripcion]">
				</div>
				<button type="button" class="btn btn-danger btn-flat btn-cerrar">Cerrar</button>

				<button type="button" class="btn btn-success btn-flat btn-pagar pull-right">Pagar</button>
				
			</div>
		</form>
	</div>
</div>