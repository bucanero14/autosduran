<table class="table table-reponsive table-condensed">
  <thead>
    <tr>
      <th>Descripción</th>
      <th>Fecha</th>
      <th>Importe</th>
      <th>Opciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($pagos as $data) { 
      $data->fecha = date_format(new DateTime($data->fecha), 'd/m/Y');
    ?>
      <tr>
        <td><?php echo $data->fecha; ?></td>
        <td><?php echo $data->descripcion; ?></td>
        <td><?php echo '$'.number_format($data->monto,2); ?></td>
        <td>
          <a href="<?php echo base_url('/pagos_extra/recibos/' . $data->id); ?>" class="btn btn-flat btn-default open-modal" data-toggle="tooltip" data-original-title="Ver Recibo"><i class="fa fa-file-text"></i></a>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>