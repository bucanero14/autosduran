<?if (isset($message) && $message != ''){?>
  <div class="alert alert-danger">
    <?echo $message;?>
  </div>
<?}?>
<?if (isset($success) && $success != ''){?>
  <div class="alert alert-success">
    <?echo $success;?>
  </div>
<?}?>

<div class="row">
	<div class="col-md-12">
	<div class="box box-danger">
		<div class="box-body">
    <div class="block">
      <a href="<?php echo base_url('pagos_extra/pagar'); ?>" id="realizar-pago" class="btn btn-flat btn-primary block" data-toggle="tooltip" data-original-title="Pago Extraordinario">Realizar Pago</a>
    </div>
    <div id="tabla-pagos-extra">
      
    </div>
		</div>
</div>