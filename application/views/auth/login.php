<div class="login-box-body">
  <p class="login-box-msg">Iniciar Sesión</p>
  <?php if ($message) {?>
  <div class="alert alert-danger">
    <?php echo $message;?>
  </div>
  <?php }?>
  <?php echo form_open('auth/login');?>
  <div class="form-group has-feedback">
    <?php echo form_input($identity);?>
    <span class="form-control-feedback"><i class="fa fa-user"></i></span>
  </div>
  <div class="form-group has-feedback">
    <?php echo form_input($password);?>
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <div class="row">
    <div class="col-xs-8">
      <div class="checkbox icheck">
        <label>
          <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?> Recuérdame
        </label>
      </div>
    </div><!-- /.col -->
    <div class="col-xs-4">
      <button type="submit" class="btn btn-primary btn-block btn-flat">Iniciar</button>
    </div><!-- /.col -->
  </div>
  <?php echo form_close();?>
</div><!-- /.login-box-body -->
