<div class="login-box-body">
  <p class="login-box-msg">Olvidé mi Contraseña</p>
  <p class="login-box-msg" style="padding-bottom: 0; font-size:12px;">Ingresa tu Email para enviarte un correo de cambio de contraseña.</p>
  <?php if ($message) {?>
  <div class="alert alert-danger">
    <?php echo $message;?>
  </div>
  <?php }?>
  <?php echo form_open('auth/forgot_password');?>
  <div class="form-group has-feedback">
    <?php echo form_input($identity);?>
    <span class="form-control-feedback"><i class="fa fa-envelope-o"></i></span>
  </div>
  <div class="row">
    <div class="col-xs-4 col-xs-offset-8">
      <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar</button>
    </div><!-- /.col -->
  </div>
  <?php echo form_close();?>

  <a href="<?php echo site_url('auth/login');?>" class="text-center">Regresar</a>
</div><!-- /.login-box-body -->
