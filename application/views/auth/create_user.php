<div class="login-box-body">
  <p class="login-box-msg">Registro</p>
  <?php if ($message) {?>
  <div class="alert alert-danger">
    <?php echo $message;?>
  </div>
  <?php }?>
  <?php echo form_open('auth/create_user');?>
  <div class="form-group has-feedback">
    <?php echo form_input($full_name);?>
    <span class="glyphicon glyphicon-user form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
    <?php echo form_input($identity);?>
    <span class="form-control-feedback"><i class="fa fa-gamepad"></i></span>
  </div>
  <div class="form-group has-feedback">
    <?php echo form_input($email);?>
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
    <?php echo form_input($password);?>
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
    <?php echo form_input($password_confirm);?>
    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-block btn-flat">Registrar</button>
  </div>
  <?php echo form_close();?>

  <!--<div class="social-auth-links text-center">
    <p>- O -</p>
    <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Regístrate con Facebook</a>
    <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Regístrate con Google+</a>
  </div><!-- /.social-auth-links -->

  <a href="<?php echo site_url('auth/login');?>" class="text-center">Ya tengo una cuenta</a>
</div><!-- /.login-box-body -->
