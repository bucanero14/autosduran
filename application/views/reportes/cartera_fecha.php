<div class="row">
  <div class="col-md-12">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Filtros</h3>
      </div>
      <div class="box-body">
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Inversionistas</label>
            <?php echo form_dropdown($attr_dropdown, $data_dropdown); ?>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Fecha Inicial</label>
            <?php echo form_input($fecha_start); ?>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Fecha Final</label>
            <?php echo form_input($fecha_end); ?>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <button id="generar-reporte" style="margin-top:25px;" class="btn btn-flat btn-info">Generar Reporte</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Tabla</h3>
      </div>
      <div class="box-body">
        <table class="table table-condensed">
          <thead>
            <th>Fecha</th>
            <th>Recibo</th>
            <th>Nombre</th>
            <th>Documento</th>
            <th>Capital</th>
            <th>Interes</th>
          </thead>
          <tbody></tbody>
        </table>
      </div>
      <div id="totales"></div>
    </div>
  </div>
</div>