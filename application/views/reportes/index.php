<div class="row">
	<div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3>&nbsp;</h3>

        <p>Cobranza por sucursal</p>
      </div>
      <div class="icon">
        <i class="fa fa-building-o"></i>
      </div>
      <a href="<?php echo site_url('reportes/cobranza_sucursal')?>" class="small-box-footer">
        Detalles <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
      <div class="inner">
        <h3>&nbsp;</h3>

        <p>Clientes morosos</p>
      </div>
      <div class="icon">
        <i class="fa fa-dollar"></i>
      </div>
      <a href="<?php echo site_url('reportes/clientes_morosos')?>" class="small-box-footer">
        Detalles <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3>&nbsp;</h3>

        <p>Pagos del día</p>
      </div>
      <div class="icon">
        <i class="fa fa-dollar"></i>
      </div>
      <a href="<?php echo site_url('reportes/pagos_del_dia')?>" class="small-box-footer">
        Detalles <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3>&nbsp;</h3>

        <p>Total Cartera</p>
      </div>
      <div class="icon">
        <i class="fa fa-building-o"></i>
      </div>
      <a href="<?php echo site_url('reportes/cartera')?>" class="small-box-footer">
        Detalles <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>  
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3>&nbsp;</h3>

        <p>Cartera X Fecha</p>
      </div>
      <div class="icon">
        <i class="fa fa-building-o"></i>
      </div>
      <a href="<?php echo site_url('reportes/cartera_fecha')?>" class="small-box-footer">
        Detalles <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
</div>