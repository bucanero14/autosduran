<div class="row">
  <div class="col-md-12">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Parámetros</h3>
      </div>
      <div class="box-body">
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Sucursal</label>
            <?php echo form_dropdown($sucursal, $sucursales); ?>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <button id="generar-reporte" style="margin-top:25px;" class="btn btn-flat btn-info">Generar Reporte</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Reporte</h3>
      </div>
      <div class="box-body">
        <table class="table table-condensed">
          <thead>
            <th>ID</th>
            <th></th>
            <th>Nombre</th>
            <th>Teléfono</th>
            <th>Celular</th>
            <th>Sucursal</th>
            <th>Fecha de Pago</th>
            <th>Letras Retrasadas</th>
            <th>Total</th>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>