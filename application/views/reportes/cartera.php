<div class="row">
  <div class="col-md-12">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Filtros</h3>
        <h3 class="box-title pull-right">Total de Cartera: <?php echo '$'.number_format($total_cartera->total,2,'.',','); ?></h3>
      </div>
      <div class="box-body">
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Inversionistas</label>
            <?php echo form_dropdown($attr_dropdown, $data_dropdown); ?>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <button id="generar-reporte" style="margin-top:25px;" class="btn btn-flat btn-info">Generar Reporte</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="box box-danger" id="info-box">
      <div class="box-header with-border">
        <h3 class="box-title">INFO. Inversionista</h3>
      </div>
      <div class="box-body">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Tabla</h3>
      </div>
      <div class="box-body">
        <table class="table data-table table-condensed">
          <thead>
            <th>Código</th>
            <th>Nombre</th>
            <th>Monto</th>
            <th>Saldo</th>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>