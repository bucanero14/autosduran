<div class="row">
  <div class="col-md-12">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Parámetros</h3>
      </div>
      <div class="box-body">
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Sucursal</label>
            <?php echo form_dropdown($sucursal, $sucursales); ?>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label for="">Fecha</label>
            <?php echo form_input($fecha); ?>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <button id="generar-reporte" style="margin-top:25px;" class="btn btn-flat btn-info">Generar Reporte</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Reporte</h3>
      </div>
      <div class="box-body">
        <table class="table table-condensed">
          <thead>
            <th>Recibo</th>
            <th>Nombre</th>
            <th>Monto</th>
            <th>Forma de Pago</th>
            <th>Tipo de Pago</th>
            <th>Opciones</th>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>