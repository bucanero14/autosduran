<?if (isset($message) && $message != ''){?>
  <div class="alert alert-danger">
    <?echo $message;?>
  </div>
<?}?>
<?if (isset($success) && $success != ''){?>
  <div class="alert alert-success">
    <?echo $success;?>
  </div>
<?}?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-danger">
			<div class="box-body">
				<?php echo form_open(base_url($submit_url), array('class' => 'form'));?>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
			        	<div class="">
			            	<label for="name">Nombre Completo</label>
			            	<?php echo form_input($name); ?>
			          	</div>
			        </div>
			        <div class="form-group">
			        	<div class="">
			            	<label for="identity">Teléfono</label>
			            	<?php echo form_input($phone); ?>
			          	</div>
			        </div>
			        <div class="form-group">
			        	<div class="">
			            	<label for="branch">Celular</label>
			            	<?php echo form_input($mobile); ?>
			          	</div>
			        </div>
			        <div class="form-group">
			        	<div class="">
			            	<label for="role">Email</label>
			            	<?php echo form_input($email); ?>
			          	</div>
			        </div>
					</div>
				</div>
			        <?php 
			        echo form_submit(array('class' => 'btn btn-flat btn-success pull-left', 'value' => 'Guardar'));
			        ?>
				<?php echo form_close();?>
			</div>
		</div>
	</div>
</div>