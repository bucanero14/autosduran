<?if (isset($message) && $message != ''){?>
  <div class="alert alert-danger">
    <?echo $message;?>
  </div>
<?}?>
<?if (isset($success) && $success != ''){?>
  <div class="alert alert-success">
    <?echo $success;?>
  </div>
<?}?>
<div class="row">
	<div class="col-md-12">
	<div class="box box-danger">
		<div class="box-body">
			<table class="table table-reponsive table-condensed">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Celular</th>
                <th>Email</th>
                <th>Opciones</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($investors as $investor) { ?>
                <tr>
                <td><?php echo $investor['nombre']; ?></td>
                  <td><?php echo $investor['telefono']; ?></td>
                  <td><?php echo $investor['movil']; ?></td>
                  <td><?php echo $investor['email']; ?></td>
                  <td>
                  	<a href="<?php echo base_url('inversionistas/editar/' . $investor['id']); ?>" class="btn btn-flat btn-warning" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
		</div>
</div>