<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migrate extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->library('migration');

		if (!$this->migration->latest()) {
			log_message('error', $this->migration->error_string());
			show_error($this->migration->error_string());
		}
	}
}

/* End of file Migrate.php */
/* Location: ./application/controllers/Migrate.php */