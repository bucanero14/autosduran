<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->output->set_template('admin');

		$this->viewmodel['icon_class'] = 'fa-table';
		$this->viewmodel['breadcrumb_header'] = 'Reportes';
	}

	//ACTION Functions
	public function index()
	{
		$this->load->view('reportes/index', $this->viewmodel);
	}

	private function _load()
	{
		$this->load->js('assets/plugins/datetimepicker/moment.js');
		$this->load->js('assets/plugins/datetimepicker/datetimepicker.js');
		$this->load->js('assets/plugins/datetimepicker/datetimepicker.es.js');
		$this->load->js('assets/plugins/morris/raphael.js');
		$this->load->js('assets/plugins/morris/morris.min.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Moment/datetime-moment.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/dataTables.buttons.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/buttons.bootstrap.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.min.js');
		$this->load->js('assets/plugins/datatables/languages/spanish.js');

		$this->load->js('assets/plugins/datatables/extensions/Buttons/jszip.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/buttons.html5.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/buttons.print.js');

		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.min.css');
		$this->load->css('assets/plugins/datatables/extensions/Buttons/buttons.bootstrap.min.css');
		$this->load->css('assets/plugins/morris/morris.css');
		$this->load->css('assets/plugins/datetimepicker/datetimepicker.css');
	}

	public function cobranza_sucursal() 
	{
		$this->viewmodel['title'] = 'Cobranza por Sucursal';
		$this->viewmodel['desc'] = 'Reporte';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->_load();
		$this->load->js('assets/themes/admin/js/reportes/sucursal_cobranza.js');

		$this->load->model('creditos_model');

		$this->viewmodel['sucursales'] = $this->creditos_model->getAllSucursales();

		$this->viewmodel['sucursal'] = array(
		    'name'  => 'sucursal',
		    'id'    => 'sucursal',
		    'class' => 'form-control',
		);
		$this->viewmodel['fecha'] = array(
			'name'  => 'fecha',
		    'id'    => 'fecha',
		    'class' => 'form-control',
		    'type'  => 'text'
		);

		$this->load->view('reportes/cobranza_sucursal', $this->viewmodel);
	}	

	public function cartera() 
	{
		$this->viewmodel['title'] = 'Cartera';
		$this->viewmodel['desc'] = 'Reporte';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->_load();
		$this->load->js('assets/themes/admin/js/reportes/cartera.js');

		$this->load->model('inversionistas_model');
		$this->load->model('reportes_model');

		$this->viewmodel['data_dropdown'] = $this->inversionistas_model->getAllInversionistas_dropDown();
		$this->viewmodel['total_cartera'] = $this->reportes_model->get_total_cartera();
		//$this->viewmodel['cartera_inversionista'] = $this->reportes_model->get_cartera_inversionista();

		$this->viewmodel['attr_dropdown'] = array(
		    'name'  => 'inversionista',
		    'id'    => 'inversionista',
		    'class' => 'form-control',
		);

		$this->load->view('reportes/cartera', $this->viewmodel);
	}	

	public function cartera_fecha() 
	{
		$this->viewmodel['title'] = 'Cartera';
		$this->viewmodel['desc'] = 'Reporte';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->_load();
		$this->load->js('assets/themes/admin/js/reportes/cartera_fecha.js');

		$this->load->model('inversionistas_model');
		$this->load->model('reportes_model');

		$this->viewmodel['data_dropdown'] = $this->inversionistas_model->getAllInversionistas_dropDown();

		$this->viewmodel['attr_dropdown'] = array(
		    'name'  => 'inversionista',
		    'id'    => 'inversionista',
		    'class' => 'form-control',
		);		
		$this->viewmodel['fecha_start'] = array(
			'name'  => 'fecha_start',
		    'id'    => 'fecha_start',
		    'class' => 'form-control',
		    'type'  => 'text'
		);
		$this->viewmodel['fecha_end'] = array(
			'name'  => 'fecha_end',
		    'id'    => 'fecha_end',
		    'class' => 'form-control',
		    'type'  => 'text'
		);

		$this->load->view('reportes/cartera_fecha', $this->viewmodel);
	}

	public function clientes_morosos()
	{
		$this->viewmodel['title'] = 'Clientes morosos';
		$this->viewmodel['desc'] = 'Reporte';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->_load();
		$this->load->js('assets/themes/admin/js/reportes/clientes_morosos.js');

		$this->load->model('creditos_model');

		$this->viewmodel['sucursales'] = $this->creditos_model->getAllSucursales();

		$this->viewmodel['sucursal'] = array(
		    'name'  => 'sucursal',
		    'id'    => 'sucursal',
		    'class' => 'form-control',
		);

		$this->load->view('reportes/clientes_morosos', $this->viewmodel);
	}

	public function pagos_del_dia()
	{
		$this->viewmodel['title'] = 'Pagos del día';
		$this->viewmodel['desc'] = 'Reporte';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->_load();
		$this->load->js('assets/themes/admin/js/reportes/pagos_del_dia.js');

		$this->load->model('creditos_model');

		$this->viewmodel['sucursales'] = $this->creditos_model->getAllSucursales();

		$this->viewmodel['sucursal'] = array(
		    'name'  => 'sucursal',
		    'id'    => 'sucursal',
		    'class' => 'form-control',
		);
		$this->viewmodel['fecha'] = array(
			'name'  => 'fecha',
		    'id'    => 'fecha',
		    'class' => 'form-control',
		    'type'  => 'text'
		);

		$this->load->view('reportes/pagos_del_dia', $this->viewmodel);
	}

	public function delete_pago($id)
	{
		$this->load->model('pagos_model');

		$this->pagos_model->delete_pago($id);

		redirect('reportes/pagos_del_dia');
	}

	//AJAX Functions
	public function get_cobranza_sucursal()
	{
		$this->output->unset_template();
		$idsucursal = $this->input->post('idsucursal');

		$this->load->model('reportes_model');
		$data = $this->reportes_model->cobranza_sucursal($idsucursal, $this->input->post('fecha'));
		$data->data = $this->reportes_model->usuarios_sucursal($idsucursal);

		echo json_encode($data);
	}

	public function get_cartera()
	{
		$this->output->unset_template();
		$id = $this->input->post('id');

		$this->load->model('reportes_model');

		$data = $this->reportes_model->get_cartera_inversionista($id);

		echo json_encode($data);
	}	

	public function get_cartera_fecha()
	{
		$this->output->unset_template();
		$id = $this->input->post('id');
		$fecha_start = $this->input->post('fecha_start');
		$fecha_end = $this->input->post('fecha_end');

		$this->load->model('reportes_model');

		$data = $this->reportes_model->get_cartera_inversionista_fecha($id,$fecha_start,$fecha_end);

		echo json_encode($data);
	}	

	public function get_total_cartera_fecha()
	{
		$this->output->unset_template();
		$id = $this->input->post('id');
		$fecha_start = $this->input->post('fecha_start');
		$fecha_end = $this->input->post('fecha_end');

		$this->load->model('reportes_model');

		$total_capital = $this->reportes_model->get_capital_cartera_inversionista_fecha($id,$fecha_start,$fecha_end);
		$total_interes = $this->reportes_model->get_interes_cartera_inversionista_fecha($id,$fecha_start,$fecha_end);

		$data['total_capital'] = number_format($total_capital, 2);
		$data['total_interes'] = number_format($total_interes, 2);
		$data['total'] = number_format(bcadd($total_capital, $total_interes, 2), 2);

		echo json_encode($data);
	}

	public function get_clientes_morosos()
	{
		$this->output->unset_template();
		$idsucursal = $this->input->post('idsucursal');

		$this->load->model('reportes_model');
		$data = $this->reportes_model->clientes_morosos($idsucursal);

		echo json_encode($data);
	}

	public function get_clientes_morosos_detalle()
	{
		$this->output->unset_template();

		$this->load->model('reportes_model');
		$data = $this->reportes_model->clientes_morosos_detalle($this->input->post('idcredito'));

		echo json_encode($data);
	}

	public function get_pagos_del_dia()
	{
		$this->output->unset_template();
		$idsucursal = $this->input->post('idsucursal');

		$this->load->model('reportes_model');
		$data = $this->reportes_model->pagos_del_dia($idsucursal, $this->input->post('fecha'));

		echo json_encode($data);
	}

	public function get_detalle_pago()
	{
		$this->output->unset_template();
		$recibo = $this->input->post('recibo');

		$this->load->model('reportes_model');
		$data = $this->reportes_model->get_detalle_pago($recibo);

		echo json_encode($data);
	}
}

/* End of file Reportes.php */
/* Location: ./application/controllers/Reportes.php */