<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends Cron_Controller {

	public function __construct()
	{
		parent::__construct();
	}

    public function CalculosDelDia() 
    {
        $this->EstablecerPagos(0);
        $this->EstablecerPagos(1);
        $this->EstablecerPagos(2);
        $this->RecalcularCreditos();
    }

	public function EstablecerPagos($pendiente)
	{
	    $this->load->model('pagos_model');

	    $pagos_realizados = $this->pagos_model->getUnrelatedPayments($pendiente);

	    foreach ($pagos_realizados as $pago)
	    {
	    	$pagos_pendientes = $this->pagos_model->getPendingPaymentsByClient($pago['idcliente'], $pendiente);

	    	$monto_pagado = $pago['monto'];
            $suma_total = 0;

	    	//Pagos retrasados o a tiempo
    		foreach ($pagos_pendientes as $pp)
    		{
                $suma_interes = 0;
                $suma_capital = 0;

    			//Primero se paga FMD
    			$monto_pagado = bcsub($monto_pagado, $pp['interes_fmd'], 2);//$monto_pagado - $pp['interes_fmd'];
    			if ($monto_pagado > 0)
                {
                    $suma_interes = bcadd($suma_interes, $pp['interes_fmd'], 2);//$suma_interes += $pp['interes_fmd'];
                    $suma_total = bcadd($suma_total, $pp['interes_fmd'], 2);//$suma_total += $pp['interes_fmd'];
    				$pp['interes_fmd'] = 0;
                }
    			else
    			{
    				$pp['interes_fmd'] = abs($monto_pagado);

                    $suma_interes = bcadd($suma_interes, bcsub($pago['monto'], $suma_total, 2), 2);//$suma_interes += $pago['monto'] - $suma_total;

    				$pp['monto'] = bcadd(bcadd($pp['capital'], $pp['interes'], 2), $pp['interes_fmd'], 2);//$pp['monto'] = $pp['capital'] + $pp['interes'] + $pp['interes_fmd'];
    				$this->pagos_model->updatePendingPayment($pp);
    				$this->pagos_model->createPendienteRealizado($pp['id'], $pago['id'], $suma_interes, $suma_capital);
                    $this->CalcularSaldo($pp['idcredito']);
    				break 1;
    			}

    			//Segundo se paga interés
    			$monto_pagado = bcsub($monto_pagado, $pp['interes'], 2);//$monto_pagado - $pp['interes'];
    			if ($monto_pagado > 0)
                {
                    $suma_interes = bcadd($suma_interes, $pp['interes'], 2);//$suma_interes += $pp['interes'];
                    $suma_total = bcadd($suma_total, $pp['interes'], 2);//$suma_total += $pp['interes'];
    				$pp['interes'] = 0;
                }
    			else
    			{
    				$pp['interes'] = abs($monto_pagado);

                    $suma_interes = bcadd($suma_interes, bcsub($pago['monto'], $suma_total, 2), 2);//$suma_interes += $pago['monto'] - $suma_total;

    				$pp['monto'] = bcadd(bcadd($pp['capital'], $pp['interes'], 2), $pp['interes_fmd'], 2);//$pp['monto'] = $pp['capital'] + $pp['interes'] + $pp['interes_fmd'];
    				$this->pagos_model->updatePendingPayment($pp);
    				$this->pagos_model->createPendienteRealizado($pp['id'], $pago['id'], $suma_interes, $suma_capital);
                    $this->CalcularSaldo($pp['idcredito']);
    				break 1;
    			}

    			//Por último se paga capital
    			$monto_pagado = bcsub($monto_pagado, $pp['capital'], 2);//$monto_pagado - $pp['capital'];
    			if ($monto_pagado > 0)
                {
                    $suma_capital = bcadd($suma_capital, $pp['capital'], 2);//$suma_capital += $pp['capital'];
                    $suma_total = bcadd($suma_total, $pp['capital'], 2);//$suma_total += $pp['capital'];
    				$pp['capital'] = 0;
                }
    			else
    			{
                    if ($monto_pagado == 0)
                        $pp['fmd'] = 0;

    				$pp['capital'] = abs($monto_pagado);

                    $suma_capital = bcadd($suma_capital, bcsub($pago['monto'], $suma_total, 2), 2);//$suma_capital += $pago['monto'] - $suma_total;

    				$pp['monto'] = bcadd(bcadd($pp['capital'], $pp['interes'], 2), $pp['interes_fmd'], 2);//$pp['monto'] = $pp['capital'] + $pp['interes'] + $pp['interes_fmd'];
    				$this->pagos_model->updatePendingPayment($pp);
    				$this->pagos_model->createPendienteRealizado($pp['id'], $pago['id'], $suma_interes, $suma_capital);
                    $this->CalcularSaldo($pp['idcredito']);
    				break 1;
    			}

                $pp['fmd'] = 0;
    			$pp['monto'] = 0;
    			$this->pagos_model->updatePendingPayment($pp);
    			$this->pagos_model->createPendienteRealizado($pp['id'], $pago['id'], $suma_interes, $suma_capital);
                $this->CalcularSaldo($pp['idcredito']);
    		}

    		if ($monto_pagado <= 0)
    		{
    			continue;
    		}

    		$pagos_adelantados = $this->pagos_model->getAdvancePaymentsByClient($pago['idcliente']);

    		//Pagos adelantados
    		foreach($pagos_adelantados as $pa)
    		{
                $suma_capital = 0;

    			$monto_pagado = bcsub($monto_pagado, $pa['capital'], 2);//$monto_pagado - $pa['capital'];
    			if ($monto_pagado > 0)
                {
                    $suma_capital = bcadd($suma_capital, $pa['capital'], 2);//$suma_capital += $pa['capital'];
                    $suma_total = bcadd($suma_total, $pa['capital'], 2);
    				$pa['capital'] = 0;
                }
    			else
    			{
                    if ($monto_pagado == 0)
                    {
                        $pa['fmd'] = 0;
                        $pa['interes'] = 0;
                    }
                    
                    $suma_capital = bcsub($pago['monto'], $suma_total, 2);//$pago['monto'] - $suma_total;

    				$pa['capital'] = abs($monto_pagado);
    				$pa['monto'] = bcadd($pa['capital'], $pa['interes'], 2);//$pa['capital'] + $pa['interes'];

    				$this->pagos_model->updatePendingPayment($pa);
    				$this->pagos_model->createPendienteRealizado($pa['id'], $pago['id'], 0, $suma_capital);
                    $this->CalcularSaldo($pa['idcredito']);
    				break 1;
    			}

    			$pa['fmd'] = 0;
    			$pa['monto'] = 0;
    			$pa['interes'] = 0;
    			$this->pagos_model->updatePendingPayment($pa);
    			$this->pagos_model->createPendienteRealizado($pa['id'], $pago['id'], 0, $suma_capital);
                $this->CalcularSaldo($pa['idcredito']);
    		}
	    }

        $this->setHistorialSystem('Pagos procesados exitosamente');
	    echo date('d-m-Y H:i:s') . " -> Pagos procesados exitosamente".PHP_EOL;
	}

	public function RecalcularCreditos()
	{
	    $this->load->model('pagos_model');

	    //Recalcular pagos alterados pero no liquidados, incluyendo adelantados
	    $pagos_pendientes_realizados = $this->pagos_model->getEvaluatedPendingPayments();

	    foreach($pagos_pendientes_realizados as $pago)
	    {
	    	$this->load->model('creditos_model');

	    	$tasa_fmd = $this->creditos_model->get_tasa_fmd_by_id($pago['idcredito']) / 100;

	    	$pago['fmd'] = bcdiv(bcmul($tasa_fmd, bcadd($pago['capital'], $pago['interes'], 2), 2), '30', 2);

	    	$today = new DateTime(date('Y-m-d'));
	    	$fecha_limite = new DateTime($pago['fecha_limite']);
	    	$dias_vencido = $today->diff($fecha_limite)->format('%a');
	    	//$monto_fmd = bcmul($pago['fmd'], $dias_vencido, 2);
            //$monto_fmd = bcadd($pago['interes_fmd'], $pago['fmd'], 2);
            $monto_fmd = $pago['interes_fmd'];

            if ($dias_vencido > $pago['dias_vencido'])
            {
                $monto_fmd += bcmul($pago['fmd'], bcsub($dias_vencido, $pago['dias_vencido']), 2);
            }

	    	$pago['monto'] = bcadd($pago['capital'], bcadd($pago['interes'], $monto_fmd, 2), 2);

	    	$pago_pendiente = array(
	    		'monto' => $pago['monto'],
	    		'interes' => $pago['interes'],
	    		'fmd' => $pago['fmd'],
	    		'interes_fmd' => $monto_fmd,
	    		'dias_vencido' => $dias_vencido,
	    		'id' => $pago['id']
	    	);

	    	$this->pagos_model->updatePendingPayment($pago_pendiente);
	    }

	    //Recalcular todos los pagos vencidos excepto los alterados
	    $pagos_vencidos = $this->pagos_model->getUnrelatedExpiredPendingPayments();
	    foreach($pagos_vencidos as $pv)
	    {
	    	$today = new DateTime(date('Y-m-d'));
	    	$fecha_limite = new DateTime($pv['fecha_limite']);
	    	$dias_vencido = $today->diff($fecha_limite)->format('%a');

	    	$monto_fmd = bcmul($pv['fmd'], $dias_vencido, 2);

	    	$monto = bcadd($pv['capital'], bcadd($pv['interes'], $monto_fmd, 2), 2);

	    	$pago_pendiente = array(
	    		'monto' => $monto,
	    		'interes_fmd' => $monto_fmd,
	    		'dias_vencido' => $dias_vencido,
	    		'id' => $pv['id']
	    	);

	    	$this->pagos_model->updatePendingPayment($pago_pendiente);
	    }

        $this->setHistorialSystem('Creditos recalculados exitosamente');
	    echo date('d-m-Y H:i:s') . " -> Creditos recalculados exitosamente".PHP_EOL;
	}

    private function CalcularSaldo($id)
    {
        $this->load->model('pagos_model');

        $cr = $this->pagos_model->getSaldoCredito($id);

        $cr['id'] = $id;

        // actualizamos estado del credito si ya esta saldado
        // status = 0 = Activo
        // status = 1 = Pagado
        // status = 2 = Truncado
        if ($cr['saldo']==0) {

            $cr['status'] = 1;
        }
        
        $this->pagos_model->updateSaldo($cr);
    }
}

/* End of file Cron.php */
/* Location: ./application/controllers/Cron.php */