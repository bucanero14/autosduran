<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sucursales extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->output->set_template('admin');

		$this->viewmodel['icon_class'] = 'fa-bank';
    	$this->viewmodel['breadcrumb_header'] = 'Inicio';
	}

	//Common Functions
	function _load() {
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.min.css');
		$this->load->css('assets/plugins/datatables/extensions/Buttons/buttons.bootstrap.min.css');

		$this->load->js('assets/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/dataTables.buttons.min.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.min.js');
		$this->load->js('assets/plugins/datatables/languages/spanish.js');
	}

	//Controller Actions
	public function index()
	{
		$this->_load();

		$this->viewmodel['title'] = 'Agregar Crédito';
		$this->viewmodel['desc'] = 'Nuevo crédito';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->model('sucursales_model');

		$this->viewmodel['branches'] = $this->sucursales_model->getSucursales();

		$this->load->js('assets/themes/admin/js/sucursales.js');

		$this->load->view('sucursales/index', $this->viewmodel);
	}

	public function crear() {
		$this->viewmodel['title'] = 'Agregar Inversionista';
		$this->viewmodel['desc'] = 'Formulario';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->library('form_validation');
		$this->load->model('sucursales_model');

		$this->form_validation->set_rules('name', 'Nombre', 'required');
		
		if ($this->form_validation->run() == TRUE){
			$name = $this->input->post('name');

			$this->sucursales_model->insertSucursal($name);

			$this->viewmodel['success'] = 'Sucursal creada con éxito.';

			redirect('sucursales');
		}
		else {
	    	$this->viewmodel['message'] = validation_errors();
	    }

	    $this->viewmodel['name'] = array(
	        'name'  => 'name',
	        'id'    => 'name',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Nombre',
	        'data-validation' => 'required',
	        'value' => $this->form_validation->set_value('name'),
	    );

	    $this->viewmodel['submit_url'] = 'sucursales/crear';

		$this->load->view('sucursales/form', $this->viewmodel);
	}

	public function editar($id) {
		$this->viewmodel['title'] = 'Editar Sucursal';
		$this->viewmodel['desc'] = 'Formulario';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Nombre', 'required');

		$this->load->model('sucursales_model');
		if ($this->form_validation->run() == TRUE) {

			$additional_data = array(
				'nombre' => $this->input->post('name')
				);

			$this->sucursales_model->updateSucursal($id, $additional_data);

			$this->viewmodel['success'] = 'Sucursal editada con éxito.';
		}
		else {
			$this->viewmodel['message'] = validation_errors();
		}

		$editedInvestor = $this->sucursales_model->getSucursalById($id);
		$this->viewmodel['name'] = array(
	        'name'  => 'name',
	        'id'    => 'name',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Nombre',
	        'data-validation' => 'required',
	        'value' => $editedInvestor->nombre
	    );

	    $this->viewmodel['submit_url'] = 'sucursales/editar/' . $id;

	    $this->load->view('sucursales/form', $this->viewmodel);
	}
}