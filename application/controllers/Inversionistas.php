<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inversionistas extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->output->set_template('admin');

		$this->viewmodel['icon_class'] = 'fa-money';
    	$this->viewmodel['breadcrumb_header'] = 'Inicio';
	}

	//Common Functions
	function _load() {
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.min.css');
		$this->load->css('assets/plugins/datatables/extensions/Buttons/buttons.bootstrap.min.css');

		$this->load->js('assets/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/dataTables.buttons.min.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.min.js');
		$this->load->js('assets/plugins/datatables/languages/spanish.js');
	}

	//Controller Actions
	public function index()
	{
		$this->_load();

		$this->viewmodel['title'] = 'Agregar Crédito';
		$this->viewmodel['desc'] = 'Nuevo crédito';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->model('inversionistas_model');

		$this->viewmodel['investors'] = $this->inversionistas_model->getAllInversionistas();

		$this->load->js('assets/themes/admin/js/inversionistas.js');

		$this->load->view('inversionistas/index', $this->viewmodel);
	}

	public function crear() {
		$this->viewmodel['title'] = 'Agregar Inversionista';
		$this->viewmodel['desc'] = 'Formulario';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->library('form_validation');
		$this->load->model('inversionistas_model');

		$this->form_validation->set_rules('name', 'Nombre', 'required');
		$this->form_validation->set_rules('phone', 'Teléfono', 'numeric');
		$this->form_validation->set_rules('mobile', 'Celular', 'numeric');
		$this->form_validation->set_rules('email', 'Email', 'valid_email');

		if ($this->form_validation->run() == TRUE){
			$name = $this->input->post('name');
			$phone = $this->input->post('phone');
			$mobile = $this->input->post('mobile');
			$email = $this->input->post('email');

			$this->inversionistas_model->insertInversionista($name, $phone, $mobile, $email);

			$this->viewmodel['success'] = 'Inversionista creado con éxito.';

			redirect('inversionistas');
		}
		else {
	    	$this->viewmodel['message'] = validation_errors();
	    }

	    $this->viewmodel['name'] = array(
	        'name'  => 'name',
	        'id'    => 'name',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Nombre',
	        'data-validation' => 'required',
	        'value' => $this->form_validation->set_value('name'),
	    );
	    $this->viewmodel['phone'] = array(
	        'name'  => 'phone',
	        'id'    => 'phone',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Teléfono',
	        'data-validation' => 'length, number',
	        'data-validation-length' => '7-10',
	        'value' => $this->form_validation->set_value('phone'),
	    );
	    $this->viewmodel['mobile'] = array(
	        'name'  => 'mobile',
	        'id'    => 'mobile',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Celular',
	        'data-validation' => 'length, number',
	        'data-validation-length' => '7-10',
	        'value' => $this->form_validation->set_value('mobile'),
	    );
	    $this->viewmodel['email'] = array(
	        'name'  => 'email',
	        'id'    => 'email',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Email',
	        'data-validation' => 'email',
	        'value' => $this->form_validation->set_value('email'),
	    );

	    $this->viewmodel['submit_url'] = 'inversionistas/crear';

		$this->load->view('inversionistas/form', $this->viewmodel);
	}

	public function editar($id) {
		$this->viewmodel['title'] = 'Editar Inversionista';
		$this->viewmodel['desc'] = 'Formulario';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->library('form_validation');

		$this->form_validation->set_rules('phone', 'Teléfono', 'numeric');
		$this->form_validation->set_rules('mobile', 'Celular', 'numeric');
		$this->form_validation->set_rules('email', 'Email', 'valid_email');

		$this->load->model('inversionistas_model');
		if ($this->form_validation->run() == TRUE) {

			$additional_data = array(
				'telefono' => $this->input->post('phone'),
				'movil' => $this->input->post('mobile'),
				'email' => $this->input->post('email')
			);

			$this->inversionistas_model->updateInversionista($id, $additional_data);

			$this->viewmodel['success'] = 'Inversionista editado con éxito.';
		}
		else {
			$this->viewmodel['message'] = validation_errors();
		}

		$editedInvestor = $this->inversionistas_model->getInversionistaById($id);
		$this->viewmodel['name'] = array(
	        'name'  => 'name',
	        'id'    => 'name',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Nombre',
	        'disabled' => 'disabled',
	        'value' => $editedInvestor->nombre
	    );
	    $this->viewmodel['phone'] = array(
	        'name'  => 'phone',
	        'id'    => 'phone',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Teléfono',
	        'data-validation' => 'length, number',
	        'data-validation-length' => '7-10',
	        'value' => $editedInvestor->telefono
	    );
	    $this->viewmodel['mobile'] = array(
	        'name'  => 'mobile',
	        'id'    => 'mobile',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Celular',
	        'data-validation' => 'length, number',
	        'data-validation-length' => '7-10',
	        'value' => $editedInvestor->movil
	    );
	    $this->viewmodel['email'] = array(
	        'name'  => 'email',
	        'id'    => 'email',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Email',
	        'data-validation' => 'email',
	        'value' => $editedInvestor->email
	    );

	    $this->viewmodel['submit_url'] = 'inversionistas/editar/' . $id;

	    $this->load->view('inversionistas/form', $this->viewmodel);
	}
}