<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->output->set_template('admin');

		$this->viewmodel['icon_class'] = '';
    	$this->viewmodel['breadcrumb_header'] = '';
	}

	//Common Functions
	function _load() {
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.min.css');
		$this->load->css('assets/plugins/datatables/extensions/Buttons/buttons.bootstrap.min.css');
		$this->load->css('assets/plugins/iCheck/square/red.css');
		
		$this->load->js('assets/plugins/iCheck/icheck.min.js');
		$this->load->js('assets/plugins/datetimepicker/moment.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Moment/datetime-moment.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/dataTables.buttons.min.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.min.js');
		$this->load->js('assets/plugins/datatables/languages/spanish.js');
	}

	public function index()
	{
		$this->_load();

		$this->load->library('custom_functions');

		$this->viewmodel['title'] = 'Caja';
		$this->viewmodel['desc'] = '';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->model('dashboard_model');

		$this->viewmodel['pagos'] = $this->dashboard_model->get_pagos_semana();

		$this->load->js('assets/themes/admin/js/dashboard.js');

		$this->load->view('dashboard/index', $this->viewmodel);
	}

	public function detalles($id)
	{
		$this->load->library('custom_functions');

		$data = [];
		$this->output->unset_template();
		$this->load->model('creditos_model');
		$data['info'] = $this->creditos_model->get_detalles($id);
		$this->load->view('creditos/detalles',$data);
	}

	//AJAX
	public function liquidacion($id)
	{
		$this->output->unset_template();

		$this->load->model('creditos_model');

		$this->viewmodel['credito'] = $this->creditos_model->get_detalles($id);
		$this->viewmodel['conceptos'] = $this->creditos_model->get_monto_liquidacion($id);


		$this->load->view('templates/liquidacion', $this->viewmodel);
	}
}
