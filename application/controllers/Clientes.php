<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->output->set_template('admin');

		$this->viewmodel['icon_class'] = 'fa-users';
    	$this->viewmodel['breadcrumb_header'] = 'Detalle de Cliente';
	}

	function _load() {
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.min.css');
		$this->load->css('assets/plugins/datatables/extensions/Buttons/buttons.bootstrap.min.css');
		$this->load->css('assets/plugins/iCheck/square/red.css');

		$this->load->js('assets/plugins/iCheck/icheck.min.js');
		$this->load->js('assets/plugins/datetimepicker/moment.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Moment/datetime-moment.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/dataTables.buttons.min.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.min.js');
		$this->load->js('assets/plugins/datatables/languages/spanish.js');
		$this->load->js('assets/plugins/print/jquery.print.js');
	}

	public function index()
	{
			
	}

	public function detalle($id)
	{
		$this->_load();

		$this->viewmodel['message'] = $this->session->flashdata('message');
		$this->viewmodel['success'] = $this->session->flashdata('success');

		$this->viewmodel['breadcrumb_detail'] = array();

		$this->load->model('clientes_model');
		$client = $this->clientes_model->getClientInfo($id);
		$vehiculo = $this->clientes_model->getInfoVehiculo($id);
		
		$this->viewmodel['pagos_pendientes'] = $this->clientes_model->getPendingPayments($id);
		$this->viewmodel['desc'] = '<strong>'.$vehiculo->marca_vehiculo.' '.$vehiculo->modelo_vehiculo.'</strong> - '.$vehiculo->year_vehiculo;
		$this->viewmodel['title'] = $client['nombre'];
		$this->viewmodel['idcliente'] = $client['id'];
		$this->viewmodel['idcredito'] = $vehiculo->id;

		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->viewmodel['total_monto'] = 0;
		foreach ($this->viewmodel['pagos_pendientes'] as $pago)
		{
			$this->viewmodel['total_monto'] += $pago['monto'];
		}

		$this->viewmodel['pagos_realizados'] = $this->clientes_model->getPayments($id);

		$this->viewmodel['observaciones'] = $this->clientes_model->getObservations($id);

		$this->load->js('assets/themes/admin/js/clientes/detalle.js');

		$this->load->view('clientes/detalle', $this->viewmodel);
	}

	public function recibos($id)
	{
		$this->output->unset_template();

		$this->load->model('clientes_model');
		$this->load->model('conceptos_model');

		$pago = $this->clientes_model->getPaymentById($id);
		$client = $this->clientes_model->getClientInfo($pago['idcliente']);
		$conceptos = $this->conceptos_model->get_conceptos($pago['id']);

		$this->viewmodel['pago'] = $pago;
		$this->viewmodel['client'] = $client;
		$this->viewmodel['conceptos'] = $conceptos;


		$this->load->view('templates/recibo', $this->viewmodel);
	}

	public function add_pago()
	{
		$this->load->model('dashboard_model');
		$data = $this->input->post('data');

		if ($result = $this->dashboard_model->insert_pago($data)) {
			$this->calcular_pago($data['id'], $data['pendiente']);
			$this->session->set_flashdata('success', 'Pago realizado con éxito.');
		}
		else
		{
			$this->session->set_flashdata('message', 'Ocurrió un error al realizar el pago.');
		}

		redirect(base_url('clientes/detalle/'.$data['id']));
	}

	//AJAX
	public function pagar($id_pago_pendiente)
	{
		$this->output->unset_template();

		$this->load->model('dashboard_model');
		$this->load->helper('form');
		$data['data'] = $this->dashboard_model->get($id_pago_pendiente);

		$this->load->model('clientes_model');
		$data['pagos_pendientes'] = $this->clientes_model->getPendingPayments($data['data']->idcliente);
		$data['total_monto'] = 0;
		$data['letras_atrasadas'] = $this->dashboard_model->get_letras_atrasadas_count($data['data']->id);
		$data['liquidacion'] = false;

		foreach ($data['pagos_pendientes'] as $pago)
		{
			$data['total_monto'] += $pago['monto'];
		}

		$this->load->view('dashboard/form_pago',$data);
	}

	public function agregarObservacion()
	{
		$this->output->unset_template();

		$observacion = $this->input->post('observacion');
		$idcliente = $this->input->post('id');

		$this->load->model('clientes_model');
		$this->clientes_model->addObservation($idcliente, $observacion);

		echo json_encode(true);
	}

	public function liquidar($idcredito)
	{
		$this->output->unset_template();

		$this->load->model('clientes_model');
		$this->load->helper('form');
		$data['pagos_pendientes'] = $this->clientes_model->getTotalPendingPayments($idcredito);
		$data['data'] = $this->clientes_model->getMontoLiquidacion($idcredito, 0);
		$data['total_monto'] = $data['data']->monto;
		$data['liquidacion'] = true;

		$this->load->view('dashboard/form_pago',$data);
	}

	public function update_monto_liquidacion($idcredito)
	{
		$this->output->unset_template();

		$this->load->model('clientes_model');
		$data = $this->clientes_model->getMontoLiquidacion($idcredito, $this->input->post('pendiente'));
		$data->monto_format = number_format($data->monto, 2);

		echo json_encode($data);
	}

	//Helper
	function calcular_pago($idcliente, $pendiente)
	{
		$this->load->model('pagos_model');
		$this->load->model('conceptos_model');

	    $pagos_realizados = $this->pagos_model->getUnrelatedPaymentsByClient($idcliente, $pendiente);
	    $pagos_pendientes = $this->pagos_model->getPendingPaymentsByClient($idcliente, $pendiente);
	    $pagos_adelantados = $this->pagos_model->getAdvancePaymentsByClient($idcliente);

	    foreach ($pagos_realizados as $pago)
	    {
	    	$monto_pagado = $pago['monto'];

	    	$conceptos = array();
	    	$montos = array();

	    	//Se calculan primero los pagos retrasados o a tiempo
    		for ($i = 0; $i < count($pagos_pendientes); $i++)
    		{
    			if ($monto_pagado <= 0)
    				break 1;

    			$monto_pagado = bcsub($monto_pagado, $pagos_pendientes[$i]['monto'], 2);//$monto_pagado - $pagos_pendientes[$i]['monto'];

    			log_message('error', 'monto_'.$i.'_pendiente: '. $monto_pagado);

    			if ($pagos_pendientes[$i]['monto'] > 0)
    			{
    				$pagos_pendientes[$i]['fecha_limite'] = date_format(new DateTime($pagos_pendientes[$i]['fecha_limite']), 'd/m/Y');
	    			if($monto_pagado >= 0) //Si sobra, se genera un concepto por el total del pago pendiente
	    			{
	    				array_push($conceptos, 'Pago de letra ' . $pagos_pendientes[$i]['documento'] . ' con vencimiento el día ' . $pagos_pendientes[$i]['fecha_limite'] . '.');
	    				array_push($montos, $pagos_pendientes[$i]['monto']);

	    				$pagos_pendientes[$i]['monto'] = 0;
	    			}
	    			else //Si no, se genera un concepto de abono por el pago pendiente
	    			{
	    				//$monto_pagado = abs($monto_pagado);
	    				array_push($conceptos, 'Abono a letra ' . $pagos_pendientes[$i]['documento'] . ' con vencimiento el día ' . $pagos_pendientes[$i]['fecha_limite'] . '.');
	    				array_push($montos, bcsub($pagos_pendientes[$i]['monto'], abs($monto_pagado), 2));

	    				$pagos_pendientes[$i]['monto'] = abs($monto_pagado);
	    				break 1;
	    			}
	    		}
    		}

    		if ($monto_pagado <= 0)
    		{
    			//Verificar si existen conceptos de este pago
				$count = $this->conceptos_model->get_conceptos_count_by_pago($pago['id']);

				//Si no tiene un concepto asignado, crearle uno
				if ($count <= 0)
				{
					for ($i = 0; $i < count($conceptos); $i++)
					{
						$data['idpagorealizado'] = $pago['id'];
						$data['concepto'] = $conceptos[$i];
						$data['monto'] = $montos[$i];
						$this->conceptos_model->create_concepto($data);
					}
				}

    			continue;
    		}

    		//Luego se calculan los pagos adelantados
    		//foreach($pagos_adelantados as $pa)
    		for ($i = 0; $i < count($pagos_adelantados); $i++)
    		{
    			if ($pagos_adelantados[$i]['capital'] > 0)
    			{
	    			$monto_pagado = bcsub($monto_pagado, $pagos_adelantados[$i]['capital'], 2);
	    			$pagos_adelantados[$i]['fecha_limite'] = date_format(new DateTime($pagos_adelantados[$i]['fecha_limite']), 'd/m/Y');
	    			if ($monto_pagado >= 0) //Si sobra, se genera un concepto por el total del pago adelantado
	    			{
	    				array_push($conceptos, 'Adelanto de letra ' . $pagos_adelantados[$i]['documento'] . ' con vencimiento el día ' . $pagos_adelantados[$i]['fecha_limite'] . '.');
	    				array_push($montos, $pagos_adelantados[$i]['capital']);

	    				$pagos_adelantados[$i]['capital'] = 0;
	    			}
	    			else //Si no, se genera un concepto de abono por el pago adelantado
	    			{
	    				//$monto_pagado = abs($monto_pagado);
						array_push($conceptos, 'Abono a letra ' . $pagos_adelantados[$i]['documento'] . ' con vencimiento el día ' . $pagos_adelantados[$i]['fecha_limite'] . '.');
						array_push($montos, bcsub($pagos_adelantados[$i]['capital'], abs($monto_pagado), 2));

						$pagos_adelantados[$i]['capital'] = abs($monto_pagado);

						break 1;
	    			}
	    		}
    		}

    		if ($monto_pagado <= 0)
    		{
	    		//Verificar si existen conceptos de este pago
				$count = $this->conceptos_model->get_conceptos_count_by_pago($pago['id']);

				//Si no tiene un concepto asignado, crearle uno
				if ($count <= 0)
				{
					for ($i = 0; $i < count($conceptos); $i++)
					{
						$data['idpagorealizado'] = $pago['id'];
						$data['concepto'] = $conceptos[$i];
						$data['monto'] = $montos[$i];
						$this->conceptos_model->create_concepto($data);
					}
				}
			}
	    }
	}
}

/* End of file Clientes.php */
/* Location: ./application/controllers/Clientes.php */