<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Creditos extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->output->set_template('admin');

		$this->viewmodel['icon_class'] = 'fa-money';
    		$this->viewmodel['breadcrumb_header'] = 'Inicio';
	}

	//Common Functions
	function _load() {
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.min.css');
		$this->load->css('assets/plugins/datatables/extensions/Buttons/buttons.bootstrap.min.css');

		$this->load->js('assets/plugins/datetimepicker/moment.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Moment/datetime-moment.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/dataTables.buttons.min.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.min.js');
		$this->load->js('assets/plugins/datatables/languages/spanish.js');
		$this->load->js('assets/js/bootbox.min.js');
		$this->load->js('assets/plugins/print/jquery.print.js');
	}

	//Controller Actions
	public function index()
	{
		$this->_load();

		$this->viewmodel['title'] = 'Agregar Crédito';
		$this->viewmodel['desc'] = 'Crea, edita, elimina';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->model('creditos_model');

		$this->viewmodel['creditos'] = $this->creditos_model->getAll();

		$this->load->js('assets/themes/admin/js/admin_creditos.js');

		//$this->setHistorial('Acceso a la sección de Créditos.');

		$this->load->view('creditos/index', $this->viewmodel);
	}

	public function detalles($id)
	{
		$this->load->library('custom_functions');

		$data = [];
		$this->output->unset_template();
		$this->load->model('creditos_model');
		$data['info'] = $this->creditos_model->get_detalles($id);
		$this->load->view('creditos/detalles',$data);
	}

	public function confirmar_credito()
	{
		$this->load->library('custom_functions');
		$this->load->model('inversionistas_model');
		$this->load->model('sucursales_model');

		$viewmodel = [];
		$this->output->unset_template();

		$cliente = $this->input->post('cliente');
		$credito = $this->input->post('credito');
		$info = $this->input->post('data');
		$vehiculo = $this->input->post('vehiculo');
		$aval = $this->input->post('aval');

		$inversionista = $this->inversionistas_model->getInversionistaById($info['inversionistas'])->nombre;
		$sucursal = $this->sucursales_model->getSucursalById($info['sucursales'])->nombre;

		$viewmodel['info'] = new stdClass();

		$viewmodel['info']->cnombre = $cliente['nombre'];
		$viewmodel['info']->cdireccion = $cliente['direccion'];
		$viewmodel['info']->ctelefono = $cliente['telefono'];
		$viewmodel['info']->cmovil = $cliente['movil'];

		$viewmodel['info']->anombre = $aval['nombre'];
		$viewmodel['info']->adireccion = $aval['direccion'];
		$viewmodel['info']->atelefono = $aval['telefono'];
		$viewmodel['info']->amovil = $aval['movil'];

		$viewmodel['info']->marca_vehiculo = $vehiculo['marca'];
		$viewmodel['info']->modelo_vehiculo = $vehiculo['modelo'];
		$viewmodel['info']->year_vehiculo = $vehiculo['year'];

		$viewmodel['info']->plazo = $credito['plazo'];
		$viewmodel['info']->valor_vehiculo = $vehiculo['valor'];
		$viewmodel['info']->tasa = $credito['tasa'];
		$viewmodel['info']->enganche = $credito['enganche'];
		$viewmodel['info']->fecha_inicial = $info['fecha_inicial'];
		$viewmodel['info']->monto = $credito['monto'];

		$viewmodel['info']->inversionista = $inversionista;
		$viewmodel['info']->sucursal = $sucursal;
		$viewmodel['info']->tasa_fmd = $info['tasa_fmd'];

		$this->load->view('creditos/detalles',$viewmodel);
	}

	public function pagos_pendientes($id)
	{
		$data = [];
		$this->output->unset_template();
		$this->load->model('creditos_model');
		$data['payments'] = $this->creditos_model->get_pagos_pendientes($id);
		$this->load->view('creditos/pagos_pendientes',$data);
	}
	
	public function editar($id)
	{
		$this->load->css('assets/plugins/iCheck/square/red.css');

		$this->load->js('assets/plugins/mask/jquery.mask.js');
		$this->load->js('assets/plugins/iCheck/icheck.min.js');
		$this->load->js('assets/themes/admin/js/main_creditos.js');

		$this->viewmodel['title'] = 'Editar Credito';
		$this->viewmodel['desc'] = 'Formulario';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->model('creditos_model');
		$this->load->library('form_validation');
		//validaciones de la forma del Info 
		//validaciones de la forma de cliente
		$this->form_validation->set_rules('cliente[nombre]', 'Cliente->nombre', 'required');
		$this->form_validation->set_rules('cliente[direccion]', 'Cliente->direccion','required');
		$this->form_validation->set_rules('cliente[telefono]', 'Cliente->telefono', 'required');
		$this->form_validation->set_rules('cliente[email]', 'Cliente->correo', 'required');
		$this->form_validation->set_rules('cliente[movil]', 'Cliente->movil', 'required');
		//validaciones de la forma de aval
		$this->form_validation->set_rules('aval[nombre]', 'Aval->nombre', 'required');
		$this->form_validation->set_rules('aval[direccion]', 'Aval->direccion', 'required');
		$this->form_validation->set_rules('aval[telefono]', 'Aval->telefono', 'required');
		$this->form_validation->set_rules('aval[email]', 'Aval->correo', 'required');
		$this->form_validation->set_rules('aval[movil]', 'Aval->movil','required');		
		//validaciones de la forma del vehiculo
		$this->form_validation->set_rules('vehiculo[marca]', 'Vehiculo->marca', 'required');
		$this->form_validation->set_rules('vehiculo[modelo]', 'Vehiculo->modelo', 'required');
		$this->form_validation->set_rules('vehiculo[year]', 'Vehiculo->Año', 'required');
		$this->form_validation->set_rules('vehiculo[valor]', 'Vehiculo->valor', 'required');
		//validaciones de la forma del credito
		$this->form_validation->set_rules('credito[monto]', 'Credito->monto', 'required');
		$this->form_validation->set_rules('credito[plazo]', 'Credito->plazo', 'required');
		$this->form_validation->set_rules('credito[tasa]', 'Credito->tasa', 'required');
		$this->form_validation->set_rules('credito[enganche]', 'Credito->enganche', 'required');
		//validaciones de la forma del Info 
		$this->form_validation->set_rules('data[codigo]', 'Info->Código', 'required');
		$this->form_validation->set_rules('data[inversionistas]', 'Info->inversionistas', 'required');
		$this->form_validation->set_rules('data[sucursales]', 'Info->sucursales', 'required');
		$this->form_validation->set_rules('data[tasa_fmd]', 'Info->Tasa FMD', 'required');
		$this->form_validation->set_rules('data[fecha_inicial]', 'Info->Fecha Inicial', 'required');

		if ($this->form_validation->run() == TRUE){

			$cliente = $this->input->post('cliente');
			$credito = $this->input->post('credito');
			$info = $this->input->post('data');
			$vehiculo = $this->input->post('vehiculo');
			$aval = $this->input->post('aval');

			$idcliente = $this->input->post('idcliente');
			$idaval = $this->input->post('idaval');
			
			$this->load->model('creditos_model');
			$this->viewmodel['success'] = $this->creditos_model->editar_credito($id,$idcliente,$idaval,$cliente,$credito,$vehiculo,$info,$aval);

			$this->setHistorial('Crédito ' . $info['codigo'] . ' editado.');

			redirect('creditos');
		}
		else {
    		$this->viewmodel['message'] = validation_errors();
    	}

    	//cargar opciones de inputs
    	$this->viewmodel['message'] = $this->session->flashdata('eliminar');
    	$credito = $this->creditos_model->get_credito($id);
		$this->input_options(false, $credito);

		$this->viewmodel['sucursales'] = $this->creditos_model->getAllSucursales();
		$this->viewmodel['inversionistas'] = $this->creditos_model->getAllInversionistas();
		$this->viewmodel['idinversionista'] = $credito['idinversionista'];
		$this->viewmodel['idsucursal'] = $credito['idsucursal'];
		$this->viewmodel['id'] = $id;

		$this->viewmodel['submit_url'] = 'creditos/editar/'.$id.'';

		$this->viewmodel['hidden'] = array('idcliente' => $credito['c_id'], 'idaval' => $credito['a_id']);

		$this->load->view('creditos/agregar', $this->viewmodel);
	}

	public function crear() {
		$this->load->css('assets/plugins/iCheck/square/red.css');

		$this->load->js('assets/plugins/mask/jquery.mask.js');
		$this->load->js('assets/plugins/iCheck/icheck.min.js');
		$this->load->js('assets/themes/admin/js/main_creditos.js');
		//cargamos el modelo
		$this->load->model('creditos_model');

		$this->viewmodel['title'] = 'Crear Credito';
		$this->viewmodel['desc'] = 'Formulario';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->library('form_validation');

		$tables = $this->config->item('tables','ion_auth');
		$identity_column = $this->config->item('identity','ion_auth');
		$this->data['identity_column'] = $identity_column;

		//validaciones de la forma de cliente
		$this->form_validation->set_rules('cliente[nombre]', 'Cliente->nombre', 'required');
		$this->form_validation->set_rules('cliente[direccion]', 'Cliente->direccion','required');
		$this->form_validation->set_rules('cliente[telefono]', 'Cliente->telefono', 'required');
		$this->form_validation->set_rules('cliente[email]', 'Cliente->correo', 'required');
		$this->form_validation->set_rules('cliente[movil]', 'Cliente->movil', 'required');
		//validaciones de la forma de aval
		$this->form_validation->set_rules('aval[nombre]', 'Aval->nombre', 'required');
		$this->form_validation->set_rules('aval[direccion]', 'Aval->direccion', 'required');
		$this->form_validation->set_rules('aval[telefono]', 'Aval->telefono', 'required');
		$this->form_validation->set_rules('aval[email]', 'Aval->correo', 'required');
		$this->form_validation->set_rules('aval[movil]', 'Aval->movil','required');		
		//validaciones de la forma del vehiculo
		$this->form_validation->set_rules('vehiculo[marca]', 'Vehiculo->marca', 'required');
		$this->form_validation->set_rules('vehiculo[modelo]', 'Vehiculo->modelo', 'required');
		$this->form_validation->set_rules('vehiculo[year]', 'Vehiculo->Año', 'required');
		$this->form_validation->set_rules('vehiculo[valor]', 'Vehiculo->valor', 'required');
		//validaciones de la forma del credito
		$this->form_validation->set_rules('credito[monto]', 'Credito->monto', 'required');
		$this->form_validation->set_rules('credito[plazo]', 'Credito->plazo', 'required');
		$this->form_validation->set_rules('credito[tasa]', 'Credito->tasa', 'required');
		$this->form_validation->set_rules('credito[enganche]', 'Credito->enganche', 'required');
		//validaciones de la forma del Info 
		$this->form_validation->set_rules('data[codigo]', 'Info->Código', 'required');
		$this->form_validation->set_rules('data[inversionistas]', 'Info->inversionistas', 'required');
		$this->form_validation->set_rules('data[sucursales]', 'Info->sucursales', 'required');
		$this->form_validation->set_rules('data[tasa_fmd]', 'Info->Tasa FMD', 'required');
		$this->form_validation->set_rules('data[fecha_inicial]', 'Info->Fecha Inicial', 'required');
		$this->form_validation->set_rules('data[letras_pagadas]', 'Info->Letras Pagadas', 'integer|less_than[credito[plazo]]');
		$this->form_validation->set_rules('data[letras_adelantadas]', 'Info->Letras Adelantadas', 'integer|less_than[credito[plazo] - data[letras_pagadas]]');


		if ($this->form_validation->run() == TRUE){

			$cliente = $this->input->post('cliente');
			$credito = $this->input->post('credito');
			$info = $this->input->post('data');
			$vehiculo = $this->input->post('vehiculo');
			$aval = $this->input->post('aval');
			
			$this->load->model('creditos_model');
			$this->viewmodel['success'] = $this->creditos_model->crear_credito($cliente,$credito,$vehiculo,$info,$aval);

			redirect('creditos');
		}
		else 
		{
    		$this->viewmodel['message'] = validation_errors();
	    }

		//cargar opciones de inputs
		$this->input_options();

		$this->viewmodel['sucursales'] = $this->creditos_model->getAllSucursales();
		$this->viewmodel['inversionistas'] = $this->creditos_model->getAllInversionistas();
		$this->viewmodel['idsucursal'] = null;
		$this->viewmodel['idinversionista'] = null;
		$this->viewmodel['hidden'] = null;

		$this->viewmodel['submit_url'] = 'creditos/crear';

		$this->load->view('creditos/agregar', $this->viewmodel);
	}

	public function eliminar($id)
	{
		//Paso 1. Obtener los IDs del cliente y aval que se va a eliminar.
		$this->load->model('creditos_model');
		$credito = $this->creditos_model->get_credito($id);
		$idcliente = $credito['c_id'];
		$idaval = $credito['a_id'];

		//Paso 2. Validar que no existan pagos realizados a este crédito. Si existen pagos, se regresa a la página de editar con un mensaje de error.
		if ($this->creditos_model->get_pagos_realizados_count($idcliente) > 0)
		{
			$this->session->set_flashdata('eliminar', 'No se puede eliminar el crédito ya que tiene pagos registrados.');

			redirect('creditos/editar/'.$id);
		}
		else
		{
			//Paso 3. Eliminar todo el crédito.
			if ($this->creditos_model->delete_credito($id, $idcliente, $idaval) == false)
			{
				$this->session->set_flashdata('eliminar', 'Ocurrió un error al eliminar. Intenta de nuevo más tarde.');

				redirect('creditos/editar/'.$id);
			}
			else
				redirect('creditos');
		}
	}

	public function ajustes($id)
	{
		$this->_load();

		$this->viewmodel['title'] = 'Ajuste de Letras';
		$this->viewmodel['desc'] = '';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->js('assets/themes/admin/js/creditos/ajustes.js');

		$this->load->model('sucursales_model');
		$this->load->model('ajustes_model');
		$this->viewmodel['sucursales'] = $this->sucursales_model->getSucursales();
		$this->viewmodel['cliente'] = $this->ajustes_model->getInfoCliente($id);
		$this->viewmodel['pagos_pendientes'] = $this->ajustes_model->getPendingPayments($id);

		$this->load->view('creditos/ajustes', $this->viewmodel);
	}

	//declaramos en una funcion las opciones de los inputs para limpiar el codigo
	public function input_options($new = true, $credito = null)
	{
		/*-----> CLIENTE <-----*/
		$this->viewmodel['cliente']['nombre'] = array(
		    'name'  => 'cliente[nombre]',
		    'id'    => 'cliente[nombre]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Nombre',
		    'data-validation' => 'required',
		    'value' => $new == true ? $this->form_validation->set_value('cliente[nombre]') : $credito['c_nombre']
		);
		$this->viewmodel['cliente']['direccion'] = array(
		    'name'  => 'cliente[direccion]',
		    'id'    => 'cliente[direccion]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Dirección',
		    'data-validation' => 'required',
		    'value' => $new == true ? $this->form_validation->set_value('cliente[direccion]') : $credito['c_direccion']
		);		
		$this->viewmodel['cliente']['telefono'] = array(
		    'name'  => 'cliente[telefono]',
		    'id'    => 'cliente[telefono]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Telefono',
		    'data-validation' => 'number, length',
		    'data-validation-length' => '7-10',
		    'value' => $new == true ? $this->form_validation->set_value('cliente[telefono]') : $credito['c_telefono']
		);		
		$this->viewmodel['cliente']['email'] = array(
		    'name'  => 'cliente[email]',
		    'id'    => 'cliente[email]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Correo Electrónico',
		    'data-validation' => 'email',
		    'value' => $new == true ? $this->form_validation->set_value('cliente[email]') : $credito['c_email']
		);		
		$this->viewmodel['cliente']['movil'] = array(
		    'name'  => 'cliente[movil]',
		    'id'    => 'cliente[movil]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Celular',
		    'data-validation' => 'number, length',
		    'data-validation-length' => '7-10',
		    'value' => $new == true ? $this->form_validation->set_value('cliente[movil]') : $credito['c_movil']
		);
		/*-----> AVAL <-----*/
		$this->viewmodel['aval']['nombre'] = array(
		    'name'  => 'aval[nombre]',
		    'id'    => 'aval[nombre]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Nombre',
		    'data-validation' => 'required',
		    'value' => $new == true ? $this->form_validation->set_value('aval[nombre]') : $credito['a_nombre']
		);
		$this->viewmodel['aval']['direccion'] = array(
		    'name'  => 'aval[direccion]',
		    'id'    => 'aval[direccion]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Dirección',
		    'data-validation' => 'required',
		    'value' => $new == true ? $this->form_validation->set_value('aval[direccion]') : $credito['a_direccion']
		);		
		$this->viewmodel['aval']['telefono'] = array(
		    'name'  => 'aval[telefono]',
		    'id'    => 'aval[telefono]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Telefono',
		    'data-validation' => 'number, length',
		    'data-validation-length' => '7-10',
		    'value' => $new == true ? $this->form_validation->set_value('aval[telefono]') : $credito['a_telefono']
		);		
		$this->viewmodel['aval']['email'] = array(
		    'name'  => 'aval[email]',
		    'id'    => 'aval[email]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Correo Electrónico',
		    'data-validation' => 'email',
		    'value' => $new == true ? $this->form_validation->set_value('aval[email]') : $credito['a_email']
		);		
		$this->viewmodel['aval']['movil'] = array(
		    'name'  => 'aval[movil]',
		    'id'    => 'aval[movil]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Celular',
		    'data-validation' => 'number, length',
		    'data-validation-length' => '7-10',
		    'value' => $new == true ? $this->form_validation->set_value('aval[movil]') : $credito['a_movil']
		);

		/*-----> INFO <-----*/
		$this->viewmodel['data']['codigo'] = array(
		    'name'  => 'data[codigo]',
		    'id'    => 'data[codigo]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Código',
		    'data-validation' => 'number',
		    'data-validation-allowing' => 'number',
		    'value' => $new == true ? $this->form_validation->set_value('data[codigo]') : $credito['codigo']
		);
		$this->viewmodel['data']['tasa_fmd'] = array(
		    'name'  => 'data[tasa_fmd]',
		    'id'    => 'data[tasa_fmd]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Tasa de FMD',
		    'data-validation' => 'number',
		    'data-validation-allowing' => 'range[0;100],float',
		    'value' => $new == true ? $this->form_validation->set_value('data[tasa_fmd]') : $credito['tasa_fmd']
		);
		$this->viewmodel['data']['letras_pagadas'] = array(
		    'name'  => 'data[letras_pagadas]',
		    'id'    => 'data[letras_pagadas]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Letras Pagadas',
		    'data-validation' => 'optional_number',
		    'value' => $this->form_validation->set_value('data[letras_pagadas]')
		);
		$this->viewmodel['data']['letras_adelantadas'] = array(
		    'name'  => 'data[letras_adelantadas]',
		    'id'    => 'data[letras_adelantadas]',
		    'type'  => 'text',
		    'class' => 'form-control',
		    'placeholder' => 'Letras Adelantadas',
		    'data-validation' => 'optional_number',
		    'value' => $this->form_validation->set_value('data[letras_adelantadas]')
		);
		$this->viewmodel['data']['sucursales'] = array(
		    'name'  => 'data[sucursales]',
		    'id'    => 'data[sucursales]',
		    'class' => 'form-control',
		    'data-validation' => 'required'
		);
		$this->viewmodel['data']['inversionistas'] = array(
		    'name'  => 'data[inversionistas]',
		    'id'    => 'data[inversionistas]',
		    'class' => 'form-control',
		    'data-validation' => 'required'
		);
		$this->viewmodel['data']['fecha_inicial'] = array(
		    'name'  => 'data[fecha_inicial]',
		    'id'    => 'data[fecha_inicial]',
		    'class' => 'form-control datepicker',
		    'placeholder' => 'Fecha Inicial',
		    'data-validation' => 'required',
		    'value' => $new == true ? $this->form_validation->set_value('data[fecha_inicial]') : $credito['fecha_inicial']
		);
		/*-----> VEHICULO <-----*/
		$this->viewmodel['vehiculo']['marca'] = array(
		    'name'  => 'vehiculo[marca]',
		    'id'    => 'vehiculo[marca]',
		    'class' => 'form-control',
		    'placeholder' => 'Marca',
		    'data-validation' => 'required',
		    'value' => $new == true ? $this->form_validation->set_value('vehiculo[marca]') : $credito['marca_vehiculo']
		);
		$this->viewmodel['vehiculo']['valor'] = array(
		    'name'  => 'vehiculo[valor]',
		    'id'    => 'vehiculo[valor]',
		    'class' => 'form-control',
		    'placeholder' => 'Valor del Vehículo',
		    'data-validation' => 'required, currency',
		    'readonly' => 'true',
		    'value' => $new == true ? $this->form_validation->set_value('vehiculo[valor]') : $credito['valor_vehiculo']
		);
		$this->viewmodel['vehiculo']['year'] = array(
		    'name'  => 'vehiculo[year]',
		    'id'    => 'vehiculo[year]',
		    'class' => 'form-control',
		    'placeholder' => 'Año del Vehiculo',
		    'data-validation' => 'number, length',
		    'data-validation-length' => '4',
		    'value' => $new == true ? $this->form_validation->set_value('vehiculo[year]') : $credito['year_vehiculo']
		);
		$this->viewmodel['vehiculo']['modelo'] = array(
		    'name'  => 'vehiculo[modelo]',
		    'id'    => 'vehiculo[modelo]',
		    'class' => 'form-control',
		    'placeholder' => 'Modelo',
		    'data-validation' => 'required',
		    'value' => $new == true ? $this->form_validation->set_value('vehiculo[modelo]') : $credito['modelo_vehiculo']
		);
		/*-----> CREDITO <-----*/
		$this->viewmodel['credito']['enganche'] = array(
		    'name'  => 'credito[enganche]',
		    'id'    => 'credito[enganche]',
		    'class' => 'form-control',
		    'placeholder' => 'Enganche',
		    'data-validation' => 'currency',
		    'value' => $new == true ? $this->form_validation->set_value('credito[enganche]') : $credito['enganche']
		);
		$this->viewmodel['credito']['tasa'] = array(
		    'name'  => 'credito[tasa]',
		    'id'    => 'credito[tasa]',
		    'class' => 'form-control',
		    'placeholder' => 'Tasa',
		    'data-validation' => 'number',
		    'data-validation-allowing' => 'range[0;100],float',
		    'value' => $new == true ? $this->form_validation->set_value('credito[tasa]') : $credito['tasa']
		);
		$this->viewmodel['credito']['monto'] = array(
		    'name'  => 'credito[monto]',
		    'id'    => 'credito[monto]',
		    'class' => 'form-control',
		    'placeholder' => 'Monto a Financiar',
		    'data-validation' => 'currency',
		    'value' => $new == true ? $this->form_validation->set_value('credito[monto]') : $credito['monto']
		);
		$this->viewmodel['credito']['plazo'] = array(
		    'name'  => 'credito[plazo]',
		    'id'    => 'credito[plazo]',
		    'class' => 'form-control',
		    'placeholder' => 'Plazo',
		    'data-validation' => 'number',
		    'value' => $new == true ? $this->form_validation->set_value('credito[plazo]') : $credito['plazo']
		);

		if ($new == false)
		{
			$this->viewmodel['credito']['plazo']['readonly'] = 'true';
			$this->viewmodel['credito']['monto']['readonly'] = 'true';
			$this->viewmodel['credito']['tasa']['readonly'] = 'true';

			$this->viewmodel['data']['letras_adelantadas']['readonly'] = 'true';
			$this->viewmodel['data']['letras_pagadas']['readonly'] = 'true';
		}
	}

	//AJAX functions
	public function liquidacion($id)
	{
		$this->output->unset_template();

		$this->load->model('creditos_model');

		$this->viewmodel['credito'] = $this->creditos_model->get_detalles($id);
		$this->viewmodel['conceptos'] = $this->creditos_model->get_monto_liquidacion($id);


		$this->load->view('templates/liquidacion', $this->viewmodel);
	}	

	public function form_ajustes($id)
	{
		$this->output->unset_template();

		$this->load->model('ajustes_model');
		$data = $this->ajustes_model->getPendingPayment($id);

		$this->load->view('creditos/form_ajustes', $data);
	}

	public function form_ajustes_interes($id)
	{
		$this->output->unset_template();

		$this->load->model('ajustes_model');
		$data = $this->ajustes_model->getPendingPayment($id);

		$this->load->view('creditos/form_ajustes_interes', $data);
	}

	public function set_adjustment($id)
	{
		$this->output->unset_template();

		$this->load->model('ajustes_model');
		
		if ($this->ajustes_model->setAdjustment($id, $this->input->post('monto')))
			echo json_encode(array('error'=>false));
		else
			echo json_encode(array('error'=>true));
	}

	public function set_adjustment_interest($id)
	{
		$this->output->unset_template();

		$this->load->model('ajustes_model');
		
		if ($this->ajustes_model->setAdjustmentInterest($id, $this->input->post('monto')))
			echo json_encode(array('error'=>false));
		else
			echo json_encode(array('error'=>true));
	}

	public function truncate($id)
	{
		$this->output->unset_template();

		$this->load->model('creditos_model');

		$this->creditos_model->truncate($id);
	}
}