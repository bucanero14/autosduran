<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller
{
	
	public function index()
	{
		$this->load->model('pagos_model');

	    //Recalcular pagos alterados pero no liquidados, incluyendo adelantados
	    $pagos_pendientes_realizados = $this->pagos_model->getEvaluatedPendingPayments();

	    echo '[';
	    foreach($pagos_pendientes_realizados as $pago)
	    {
	    	$this->load->model('creditos_model');

	    	$tasa_fmd = $this->creditos_model->get_tasa_fmd_by_id($pago['idcredito']) / 100;

	    	$pago['fmd'] = bcdiv(bcmul($tasa_fmd, bcadd($pago['capital'], $pago['interes'], 2), 2), '30', 2);

	    	$today = new DateTime(date('Y-m-d'));
	    	$fecha_limite = new DateTime($pago['fecha_limite']);
	    	$dias_vencido = $today->diff($fecha_limite)->format('%a');
	    	//$monto_fmd = bcmul($pago['fmd'], $dias_vencido, 2);
            //$monto_fmd = bcadd($pago['interes_fmd'], $pago['fmd'], 2);
            $monto_fmd = $pago['interes_fmd'];

            if ($dias_vencido > $pago['dias_vencido'])
            {
                $monto_fmd += bcmul($pago['fmd'], bcsub($dias_vencido, $pago['dias_vencido']), 2);
            }

	    	$pago['monto'] = bcadd($pago['capital'], bcadd($pago['interes'], $monto_fmd, 2), 2);

	    	$pago_pendiente = array(
	    		'monto' => $pago['monto'],
	    		'interes' => $pago['interes'],
	    		'fmd' => $pago['fmd'],
	    		'interes_fmd' => $monto_fmd,
	    		'dias_vencido' => $dias_vencido,
	    		'id' => $pago['id']
	    	);

	    	echo json_encode($pago_pendiente) . ',';
	    }
	    echo ']';
	}
}