<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pagos_extra extends MY_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->output->set_template('admin');

		$this->viewmodel['icon_class'] = '';
    	$this->viewmodel['breadcrumb_header'] = '';
	}

	//Common Functions
	function _load() {
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.min.css');
		$this->load->css('assets/plugins/datatables/extensions/Buttons/buttons.bootstrap.min.css');
		$this->load->css('assets/plugins/iCheck/square/red.css');
		$this->load->css('assets/themes/admin/css/custom.css');

		$this->load->js('assets/plugins/iCheck/icheck.min.js');
		$this->load->js('assets/plugins/datetimepicker/moment.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Moment/datetime-moment.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/dataTables.buttons.min.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.min.js');
		$this->load->js('assets/plugins/datatables/languages/spanish.js');
		$this->load->js('assets/plugins/print/jquery.print.js');
	}

	public function index()
	{
		$this->_load();

		$this->load->library('custom_functions');

		$this->viewmodel['title'] = 'Pagos Extraordinarios';
		$this->viewmodel['desc'] = '';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->model('pagos_extra_model');

		$this->viewmodel['pagos'] = $this->pagos_extra_model->get_pagos();

		$this->load->js('assets/themes/admin/js/pagos_extra/main.js');

		$this->load->view('pagos_extra/index', $this->viewmodel);
	}

	public function load_list()
	{
		$this->output->unset_template();
		$this->load->model('pagos_extra_model');
		$this->load->library('custom_functions');

		$this->viewmodel['pagos'] = $this->pagos_extra_model->get_pagos();

		$this->load->view('pagos_extra/list', $this->viewmodel);
	}

	public function pagar()
	{
		$this->output->unset_template();
		$this->load->view('pagos_extra/form_pago');
	}

	public function add_pago()
	{
		$this->output->unset_template();

		$this->load->model('pagos_extra_model');
		$data = $this->input->post('data');

		if ($result = $this->pagos_extra_model->insert_pago($data)) {
			$response = json_encode(array('error'=>false));
		}
		else
		{
			$response = json_encode(array('error'=>true));
		}

		echo $response;
	}

	public function recibos($id)
	{
		$this->output->unset_template();
		$this->load->model('pagos_extra_model');

		$this->viewmodel['pago'] = $this->pagos_extra_model->get_pago($id);

		$this->load->view('templates/recibo_extra', $this->viewmodel);
	}
}
