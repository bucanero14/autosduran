<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends Admin_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->output->set_template('admin');

		$this->viewmodel['icon_class'] = 'fa-users';
    	$this->viewmodel['breadcrumb_header'] = 'Inicio';
	}

	//Common Functions
	function _load() {
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.min.css');
		$this->load->css('assets/plugins/datatables/extensions/Buttons/buttons.bootstrap.min.css');

		$this->load->js('assets/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Buttons/dataTables.buttons.min.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.min.js');
		$this->load->js('assets/plugins/datatables/languages/spanish.js');
	}

	//Controller Actions
	public function index()
	{
		$this->_load();

		$this->viewmodel['title'] = 'Administrador de Usuarios';
		$this->viewmodel['desc'] = 'Crea, edita, elimina';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->model('users_model');

		$this->viewmodel['users'] = $this->users_model->getAllUsers();

		$this->load->js('assets/themes/admin/js/adminusuarios.js');

		$this->load->view('usuarios/index', $this->viewmodel);
	}

	public function crear() {
		$this->viewmodel['title'] = 'Crear Usuario';
		$this->viewmodel['desc'] = 'Formulario';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->library('form_validation');

		$tables = $this->config->item('tables','ion_auth');
		$identity_column = $this->config->item('identity','ion_auth');
		$this->data['identity_column'] = $identity_column;

		$this->form_validation->set_rules('name', 'Por favor, ingresa el nombre del empleado.', 'required');
		$this->form_validation->set_rules('identity', 'Por favor, ingresa un nombre de usuario válido.', 'required|is_unique['.$tables['users'].'.username]');
		$this->form_validation->set_rules('role', 'Por favor, especifica el rol que tendrá el usuario.', 'required');

		if ($this->form_validation->run() == TRUE){
	        $identity = $this->input->post('identity');
	        $password = 'password';

			$additional_data = array(
				'full_name' => $this->input->post('name'),
				'idsucursal' => $this->input->post('branch')
			);

			$groups = array(
				$this->input->post('role')
			);

			$id = $this->ion_auth->register($identity, $password, '', $additional_data, $groups);

			if ($id)
			{
				$this->viewmodel['success'] = 'Usuario creado con éxito.';
				redirect('usuarios');
			}
			else
				$this->viewmodel['message'] = 'Ocurrió un error. Intenta de nuevo más tarde.';	
		}
		else {
	    	$this->viewmodel['message'] = validation_errors();
	    }

	    $this->viewmodel['name'] = array(
	        'name'  => 'name',
	        'id'    => 'name',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Nombre',
	        'data-validation' => 'required',
	        'value' => $this->form_validation->set_value('name'),
	    );
	    $this->viewmodel['identity'] = array(
	        'name'  => 'identity',
	        'id'    => 'identity',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Nombre de Usuario',
	        'data-validation' => 'required',
	        'value' => $this->form_validation->set_value('identity'),
	    );

	    $this->load->model('users_model');
	    $this->viewmodel['roles'] = $this->users_model->getAllRoles();
	    $this->viewmodel['branches'] = $this->users_model->getAllBranches();

	    $this->viewmodel['current_role'] = 1;
	    $this->viewmodel['current_branch'] = 1;

	    $this->viewmodel['submit_url'] = 'usuarios/crear';

		$this->load->view('usuarios/form', $this->viewmodel);
	}

	public function editar($id) {
		$this->viewmodel['title'] = 'Editar Usuario';
		$this->viewmodel['desc'] = 'Formulario';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->library('form_validation');

		$tables = $this->config->item('tables','ion_auth');
		$identity_column = $this->config->item('identity','ion_auth');
		$this->data['identity_column'] = $identity_column;

		$this->form_validation->set_rules('role', 'Por favor, especifica el rol que tendrá el usuario.', 'required');

		$this->load->model('users_model');
		if ($this->form_validation->run() == TRUE) {

			$additional_data = array(
				'idsucursal' => $this->input->post('branch')
			);

			$this->ion_auth->remove_from_group(array(), $id);
			$this->ion_auth->add_to_group($this->input->post('role'), $id);

			$this->ion_auth->update($id, $additional_data);

			$this->viewmodel['success'] = 'Usuario editado con éxito.';
		}
		else {
			$this->viewmodel['message'] = validation_errors();
		}

		$editedUser = $this->users_model->getUserById($id);
		$this->viewmodel['name'] = array(
	        'name'  => 'name',
	        'id'    => 'name',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Nombre',
	        'disabled' => 'disabled',
	        'value' => $editedUser->full_name
	    );
	    $this->viewmodel['identity'] = array(
	        'name'  => 'identity',
	        'id'    => 'identity',
	        'type'  => 'text',
	        'class' => 'form-control',
	        'placeholder' => 'Nombre de Usuario',
	        'disabled' => 'disabled',
	        'value' => $editedUser->username
	    );

	    $this->viewmodel['roles'] = $this->users_model->getAllRoles();
	    $this->viewmodel['branches'] = $this->users_model->getAllBranches();

	    $this->viewmodel['current_role'] = $editedUser->role;
	    $this->viewmodel['current_branch'] = $editedUser->idsucursal;

	    $this->viewmodel['submit_url'] = 'usuarios/editar/' . $id;

	    $this->load->view('usuarios/form', $this->viewmodel);
	}

	//AJAX Functions
	public function reset_password() {
		$this->output->unset_template();
		$id = $this->input->post('id');

		$this->load->model('users_model');

		$editedUser = $this->users_model->getUserById($id);

		$json = array();
		if ($this->ion_auth->reset_password($editedUser->username, 'password')) {
			$json = array( 
				'success' => true,
				'message' => 'Contraseña de ' . $editedUser->username . ' restaurada exitosamente. La nueva contraseña es: password'
				);
		}
		else {
			$json = array( 
				'success' => false,
				'message' => 'Ocurrió un error al restaurar la contraseña. Intenta de nuevo más tarde.'
				);
		}

		echo json_encode($json);
	}

	public function disable_user() {
		$this->output->unset_template();
		$id = $this->input->post('id');

		echo json_encode($this->ion_auth->update($id, array('active' => 0)));
	}
}