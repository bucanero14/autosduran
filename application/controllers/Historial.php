<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Historial extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->output->set_template('admin');

		$this->viewmodel['icon_class'] = 'fa-hourglass-half';
    	$this->viewmodel['breadcrumb_header'] = 'Inicio';
	}

	//Common Functions
	function _load() {
		$this->load->css('assets/plugins/datatables/dataTables.bootstrap.min.css');

		$this->load->js('assets/plugins/datetimepicker/moment.js');
		$this->load->js('assets/plugins/datatables/jquery.dataTables.min.js');
		$this->load->js('assets/plugins/datatables/extensions/Moment/datetime-moment.js');
		$this->load->js('assets/plugins/datatables/dataTables.bootstrap.min.js');
		$this->load->js('assets/plugins/datatables/languages/spanish.js');
	}

	//Controller Actions
	public function index()
	{
		$this->_load();

		$this->viewmodel['title'] = 'Historial de Actividades';
		$this->viewmodel['desc'] = '';
		$this->viewmodel['breadcrumb_detail'] = array();
		$this->load->section('text_header', 'themes/text_header', $this->viewmodel);

		$this->load->model('historial_model');

		$this->viewmodel['historial'] = $this->historial_model->getAll();

		$this->load->js('assets/themes/admin/js/historial.js');

		$this->load->view('historial/index', $this->viewmodel);
	}

}

/* End of file Historial.php */
/* Location: ./application/controllers/Historial.php */