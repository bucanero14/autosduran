<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inversionistas_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	public function getAllInversionistas()
	{
		$this->db->select('id, nombre, telefono, movil, email');
		$this->db->from('inversionistas');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getAllInversionistas_dropDown()
	{
		$arr = [];
		$this->db->select('id, nombre');
		$this->db->from('inversionistas');

		$query = $this->db->get();

		foreach ($query->result_array() as $key => $value) {
			$arr[$value['id']] = $value['nombre'];
		}

		return $arr;
	}


	public function getInversionistaById($id)
	{
		$this->db->select('nombre, telefono, movil, email');
		$this->db->from('inversionistas');
		$this->db->where('id', $id);

		$query = $this->db->get();

		return $query->result()[0];
	}

	public function insertInversionista($name, $phone, $mobile, $email)
	{
		$data = array(
			'nombre' => $name,
			'telefono' => $phone,
			'movil' => $mobile,
			'email' => $email
		);

		$this->db->insert('inversionistas', $data);
	}

	public function updateInversionista($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('inversionistas', $data);
	}
}