<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Creditos_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	public function getAll() 
	{
		$this->db->select('cr.id, cr.codigo,cr.monto,cl.telefono,cl.nombre,cr.saldo,cr.status')
		->from('creditos as cr, clientes as cl')
	 	->where('cr.idcliente = cl.id');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getAllSucursales() 
	{
		$arr=[];

		$this->db->select('id,nombre');
		$this->db->from('sucursales');

		$query = $this->db->get();

		foreach ($query->result_array() as $key => $value) {
			$arr[$value['id']] = $value['nombre'];
		}

		return $arr;
	}

	public function get_detalles($id)
	{	
		$this->db->select('cr.*,cl.telefono as ctelefono,cl.nombre as cnombre,cl.movil as cmovil,cl.direccion as cdireccion,av.nombre as anombre,av.telefono as atelefono,av.movil as amovil, av.direccion as adireccion, su.nombre as sucursal, in.nombre as inversionista')
		->from('creditos as cr, clientes as cl,avales as av, inversionistas as in, sucursales as su')
	 	->where('cr.idcliente = cl.id')
	 	->where('cr.idaval = av.id')
	 	->where('cr.idinversionista = in.id')
	 	->where('cr.idsucursal = su.id')
	 	->where('cr.id', $id);

	 	$result = $this->db->get()->row();

		return $result;
	}

	public function get_pagos_pendientes($id)
	{
		$this->db->select('documento, monto, fmd, fecha_limite, capital, interes, interes_fmd');
		$this->db->from('pagos_pendientes');
		$this->db->where('idcredito', $id);

		$result = $this->db->get();

		return $result->result_array();
	}

	public function get_sucursal($id)
	{
		$credito = $this->db->where('id',$id)->get('creditos')->row();

		return $credito;
	}

	public function get_credito_with_next_payment($id)
	{
		//$credito = $this->db->where('id',$id)->get('creditos')->row();

		$credito = $this->db->select('cr.idsucursal, pp.fecha_limite as fecha_pago')
		->from('creditos cr, pagos_pendientes pp')
		->where('cr.id = pp.idcredito')
		->where('cr.id', $id)
		->where('pp.monto > 0')
		->where('pp.interes_fmd = 0')
		->order_by('pp.fecha_limite asc')
		->limit(1)
		->get();

		return $credito->row();
	}

	public function get_credito($id)
	{
		$this->db->select('cr.codigo, cr.tasa_fmd, cr.fecha_inicial, cr.idsucursal, cr.idinversionista, cr.marca_vehiculo, cr.year_vehiculo, cr.modelo_vehiculo, cr.monto, cr.tasa, cr.valor_vehiculo, cr.plazo, cr.enganche, c.id as c_id, c.nombre as c_nombre, c.direccion as c_direccion, c.telefono as c_telefono, c.email as c_email, c.movil as c_movil, a.id as a_id, a.nombre as a_nombre, a.direccion as a_direccion, a.telefono as a_telefono, a.email as a_email, a.movil as a_movil')
		->from('creditos cr, clientes c, avales a')
		->where('cr.id', $id)
		->where('c.id = cr.idcliente')
		->where('a.id = cr.idaval');

		$result = $this->db->get()->row_array();

		return $result;
	}

	public function getAllInversionistas() 
	{
		$arr=[];
		
		$this->db->select('id,nombre');
		$this->db->from('inversionistas');

		$query = $this->db->get();

		foreach ($query->result_array() as $key => $value) {
			$arr[$value['id']] = $value['nombre'];
		}

		return $arr;
	}

	public function get_pagos_realizados_count($idcliente){
		$this->db->select('count(1) as num_rows')
		->from('pagos_realizados')
		->where('idcliente', $idcliente);

		$result = $this->db->get();

		return $result->row()->num_rows;
	}

	public function delete_credito($idcredito, $idcliente, $idaval)
	{
		$this->db->trans_start();
		//Paso 1. Eliminar los Pagos Pendientes.
		$this->db->delete('pagos_pendientes', array('idcredito' => $idcredito));
		//Paso 2. Eliminar el crédito.
		$this->db->delete('creditos', array('id' => $idcredito));
		//Paso 3. Eliminar el cliente.
		$this->db->delete('clientes', array('id' => $idcliente));
		//Paso 4. Eliminar el aval.
		$this->db->delete('avales', array('id' => $idaval));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}

		$this->db->trans_commit();
		return true;
	}

	public function editar_credito($id,$idcliente,$idaval,$cliente,$credito,$vehiculo,$info,$aval)
	{
		$viewmodel = 'Ocurrió un error. Intenta de nuevo más tarde.';
		
		$update_arr = array(
			'idinversionista'=>$info['inversionistas'],
			'tasa_fmd'=>$info['tasa_fmd'],
			'fecha_inicial'=>$info['fecha_inicial'],
			'idsucursal'=>$info['sucursales'],
			'marca_vehiculo'=>$vehiculo['marca'],
			'modelo_vehiculo'=>$vehiculo['modelo'],
			'valor_vehiculo'=>$vehiculo['valor'],
			'year_vehiculo'=>$vehiculo['year'],
			'enganche'=>$credito['enganche'],
			'codigo'=>$info['codigo']
		);

		$this->db->where('id', $id);
		if ($this->db->update('creditos',$update_arr)) {


			$this->db->where('id', $idcliente)
			->update('clientes', $cliente);

			$this->db->where('id', $idaval)
			->update('avales', $aval);
			if ($this->cambiar_fecha_pagos_pendientes($id, $info['fecha_inicial']))
				$viewmodel = 'Credito editado con éxito.';
		}

		return $viewmodel;
	}

	public function crear_credito($cliente,$credito,$vehiculo,$info,$aval)
	{
		$viewmodel = 'Ocurrió un error. Intenta de nuevo más tarde.';
		//insertamos el cliente y guardamos su id
		if ($this->db->insert('clientes',$cliente)) {
			$id_cliente = $this->db->insert_id();
			//insertamos el aval y guardamos el id
			if ($this->db->insert('avales',$aval)) {
				$id_aval = $this->db->insert_id();

				//preparamos el array para creditos
				$insert_credito = array(
					'idinversionista'=>$info['inversionistas'],
					'tasa_fmd'=>$info['tasa_fmd'],
					'fecha_inicial'=>$info['fecha_inicial'],
					'idcliente'=>$id_cliente,
					'idaval'=>$id_aval,
					'idsucursal'=>$info['sucursales'],
					'marca_vehiculo'=>$vehiculo['marca'],
					'modelo_vehiculo'=>$vehiculo['modelo'],
					'valor_vehiculo'=>$vehiculo['valor'],
					'year_vehiculo'=>$vehiculo['year'],
					'plazo'=>$credito['plazo'],
					'monto'=>$credito['monto'],
					'tasa'=>$credito['tasa'],
					'enganche'=>$credito['enganche'],
					'saldo'=>$credito['monto'],
					'codigo'=>$info['codigo']
				);

				if ($this->db->insert('creditos',$insert_credito)) {
					$id_credito = $this->db->insert_id();
					$this->generar_pagos_pendientes($id_credito, $info['letras_pagadas'], $info['letras_adelantadas']);
					$viewmodel = 'Credito creado con éxito.';
				}
			}
		}

		return $viewmodel;
	}

	public function generar_pagos_pendientes($id, $letras_pagadas, $letras_adelantadas)
	{
		$this->load->library('custom_functions');

		if (strlen($letras_pagadas) == 0)
			$letras_pagadas = 0;

		if (strlen($letras_adelantadas) == 0)
			$letras_adelantadas = 0;

		$credito = $this->db->get_where('creditos', array('id' => $id))->row_array();

		$capital = bcdiv($credito['monto'], $credito['plazo'], 2);//$credito['monto']/$credito['plazo'];
		//% de interes mensual
		$tasa_mensual = $credito['tasa']/100;
		//fecha del primer pago
		$fecha = $credito['fecha_inicial'];
		//convertimos de string a datetime
		$start_day = new DateTime($fecha);
		//interes que se paga mensualmete
		$interes_mensual = bcmul($credito['monto'], $tasa_mensual, 2);//$credito['monto']*$tasa_mensual;
		//total a pagar mensualmente
		$monto = bcadd($interes_mensual, $capital, 2);//$interes_mensual+$capital;
		$tasa_fmd = bcdiv($credito['tasa_fmd'], '100', 2);//($credito['tasa_fmd']/100);
		$fmd = bcdiv(bcmul($monto, $tasa_fmd, 2), '30', 2);//(($monto * $tasa_fmd)/30);

		//for ($i = 0; $i < credito['plazo']; $i++) Cambiar esta línea por la de abajo al terminar la migración de datos.
		for ($i=$letras_pagadas; $i < $credito['plazo'] - $letras_adelantadas; $i++) {

			//si la fecha no es la del primer pago se le agrega un mes
			$fecha = $this->custom_functions->agregar_mes($start_day,$i);

			$insert_credito = array(
				'documento' => $i+1,
				'interes' => $interes_mensual,
				'capital' => $capital,
				'monto' => $monto,
				'fecha_limite' => $fecha,
				'fmd' => $fmd,
				'idcredito' => $id,
				'interes_fmd' => 0
			);

			$this->db->insert('pagos_pendientes',$insert_credito);
		}
	}

	function cambiar_fecha_pagos_pendientes($idcredito, $fecha)
	{
		$this->load->library('custom_functions');

		$pagos_pendientes = $this->db
		->where('pp.idcredito', $idcredito)
		->order_by('pp.fecha_limite asc')
		->get('pagos_pendientes pp')->result_array();

		$start_day = new DateTime($fecha);

		$this->db->trans_start();
		for ($i = 0; $i < sizeof($pagos_pendientes); $i++) {

			//si la fecha no es la del primer pago se le agrega un mes
			$fecha = $this->custom_functions->agregar_mes($start_day,$pagos_pendientes[$i]['documento'] - 1);

			$data = array(
				'fecha_limite' => $fecha
			);

			$this->db->where('id', $pagos_pendientes[$i]['id'])
			->update('pagos_pendientes', $data);
		}

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
		}
		return true;
	}

	public function get_tasa_fmd_by_id($id)
	{
		$this->db->select('tasa_fmd');
		$this->db->from('creditos');
		$this->db->where('id', $id);

		$query = $this->db->get();

		return $query->result_array()[0]['tasa_fmd'];
	}

	public function get_monto_liquidacion($id)
	{
		$interes = $this->db->query("select sum(pp.interes + pp.interes_fmd) as interes
									from creditos cr
									inner join pagos_pendientes pp on cr.id = pp.idcredito
									inner join clientes c on c.id = cr.idcliente
									where fecha_limite <= date_sub(date_add(CURDATE(), interval 1 month), interval 1 day)
									and pp.idcredito = {$id}")->row_array();

		$capital = $this->db->query("select sum(capital) as capital from pagos_pendientes where idcredito = {$id}")->row_array();

		$return = [];
		$return['interes'] = $interes['interes'];
		$return['capital'] = $capital['capital'];
		$return['total'] = bcadd($capital['capital'], $interes['interes'], 2);

		return $return;
	}

	public function truncate($id)
	{
		$data['status'] = 2;
		 
		$this->db->where('id',$id);
		$this->db->update('creditos', $data);
	}
}