<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pagos_extra_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	public function get_pagos()
	{

		if ($this->ion_auth->is_admin())
		{
			$this->db->select('*')
			->from('pagos_extraordinarios')
			->order_by('fecha', 'DESC');
		}
		else
		{
			//tomamos la info del usuario logeado
			$user = $this->ion_auth->user()->row();

			$this->db->select('*')
			->from('pagos_extraordinarios')
			->where('idsucursal',$user->idsucursal)
			->order_by('fecha', 'DESC');
		}

		$query = $this->db->get();

		return $query->result();
	}

	public function insert_pago($data)
	{	
		date_default_timezone_set('America/Merida');
		
		$user = $this->ion_auth->user()->row();
		$importe = $data['importe'];
		$descripcion = $data['descripcion'];
		
		$data = array(
			'idsucursal' =>$user->idsucursal,
			'monto' =>$importe,
			'descripcion'=>$descripcion,
			'fecha' => date('Y-m-d')
		);

		if ($this->db->insert('pagos_extraordinarios',$data)) {
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get_pago($id)
	{
		$this->db->select('*')
		->from('pagos_extraordinarios')
		->where('id', $id);

		return $this->db->get()->result_array()[0];
	}
}