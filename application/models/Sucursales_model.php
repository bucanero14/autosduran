<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sucursales_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	public function getSucursales()
	{
		$this->db->select('id, nombre');
		$this->db->from('sucursales');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getSucursalById($id)
	{
		$this->db->select('nombre');
		$this->db->from('sucursales');
		$this->db->where('id', $id);

		$query = $this->db->get();

		return $query->result()[0];
	}

	public function insertSucursal($name)
	{
		$data = array(
			'nombre' => $name
		);

		$this->db->insert('sucursales', $data);
	}

	public function updateSucursal($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('sucursales', $data);
	}
}