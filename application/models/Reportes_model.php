<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();	
	}

	public function cobranza_sucursal($idsucursal, $fecha)
	{
		$fecha = $fecha . '-01';
		$pagos_totales = $this->db->query("select count(1) as pagos_totales
										   from creditos cr
										   inner join pagos_pendientes pp on cr.id = pp.idcredito
										   where pp.fecha_limite between '$fecha' and LAST_DAY('$fecha')
										   and cr.idsucursal = $idsucursal
										   and cr.status = 0");

		$pagos_hechos = $this->db->query("select count(distinct pr.idcliente) as pagos_hechos from pagos_realizados pr
										  inner join creditos cr on cr.idcliente = pr.idcliente
										  where pr.fecha between '$fecha' and last_day('$fecha')
										  and cr.idsucursal = $idsucursal
										  and cr.status = 0");

		$result = new stdClass();
		$result->pagos_totales = $pagos_totales->row()->pagos_totales;
		$result->pagos_hechos = $pagos_hechos->row()->pagos_hechos;

		return $result;
	}

	public function get_cartera_inversionista($id)
	{
		$query = $this->db->query("SELECT cr.codigo, cl.nombre,cr.saldo,cr.monto,inv.nombre as nombre_inv, inv.telefono as tel_inv, inv.movil as movil_inv, inv.email as email_inv,(SELECT SUM(cr.saldo) FROM creditos cr WHERE idinversionista = '$id') as saldo_total FROM creditos cr,clientes cl,inversionistas inv WHERE idinversionista = '$id' AND cr.idcliente = cl.id AND inv.id = cr.idinversionista AND cr.status = 0");

		//$query = $this->db->get();

		return $query->result_array();
	}		

	public function get_cartera_inversionista_fecha($id,$fecha_start,$fecha_end)
	{
		$fecha_start = $fecha_start." 00:00:00";
		$fecha_end = $fecha_end." 23:59:59";

		$query = $this->db->query("SELECT pgr.id as recibo,cl.nombre,pgr.fecha,format(pr.importe_capital, 2) as importe_capital,format(pr.importe_interes, 2) as importe_interes, pp.documento FROM pendiente_realizado pr,pagos_realizados pgr,clientes cl,creditos cr,pagos_pendientes pp WHERE cr.idinversionista = '$id' AND pr.idpagopendiente = pp.id AND pp.idcredito = cr.id AND pgr.id = pr.idpagorealizado AND cl.id = pgr.idcliente AND pgr.fecha BETWEEN '$fecha_start' AND '$fecha_end'");

		return $query->result_array();
	}		

	public function get_capital_cartera_inversionista_fecha($id,$fecha_start,$fecha_end)
	{
		$fecha_start = $fecha_start." 00:00:00";
		$fecha_end = $fecha_end." 23:59:59";

		$query = $this->db->query("SELECT sum(pr.importe_capital) as total_capital FROM pendiente_realizado pr,pagos_realizados pgr,clientes cl,creditos cr,pagos_pendientes pp WHERE cr.idinversionista = '$id' AND pr.idpagopendiente = pp.id AND pp.idcredito = cr.id AND pgr.id = pr.idpagorealizado AND cl.id = pgr.idcliente AND pgr.fecha BETWEEN '$fecha_start' AND '$fecha_end'");

		return $query->row()->total_capital;
	}		

	public function get_interes_cartera_inversionista_fecha($id,$fecha_start,$fecha_end)
	{
		$fecha_start = $fecha_start." 00:00:00";
		$fecha_end = $fecha_end." 23:59:59";

		$query = $this->db->query("SELECT sum(pr.importe_interes) as total_interes FROM pendiente_realizado pr,pagos_realizados pgr,clientes cl,creditos cr,pagos_pendientes pp WHERE cr.idinversionista = '$id' AND pr.idpagopendiente = pp.id AND pp.idcredito = cr.id AND pgr.id = pr.idpagorealizado AND cl.id = pgr.idcliente AND pgr.fecha BETWEEN '$fecha_start' AND '$fecha_end'");

		return $query->row()->total_interes;
	}	

	public function usuarios_sucursal($idsucursal) 
	{
		$this->db->select('u.username usuario, u.full_name nombre, g.name as rol');
		$this->db->from('users u');
		$this->db->join('users_groups ug', 'ug.user_id = u.id');
		$this->db->join('groups g', 'g.id = ug.group_id');
		$this->db->where('u.active', 1);
		$this->db->where('u.idsucursal', $idsucursal);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function clientes_morosos($idsucursal)
	{
		$query = $this->db->query("select cr.id, c.nombre, c.movil, c.telefono, c.email, s.nombre sucursal, max(DATE_FORMAT(fecha_limite, '%d/%m/%Y')) as fecha_pago, format(sum(pp.monto), 2) as total, count(pp.id) as pagos_pendientes
									from creditos cr
									inner join pagos_pendientes pp on cr.id = pp.idcredito
									inner join clientes c on c.id = cr.idcliente
									inner join sucursales s on s.id = cr.idsucursal
									where fecha_limite <= CURDATE()
									and idsucursal = $idsucursal
									and pp.monto > 0
									and cr.status = 0
									group by c.id, c.nombre, c.movil, c.telefono, c.email, s.nombre
									having total > 0");

		return $query->result();
	}

	public function clientes_morosos_detalle($idcredito)
	{
		$this->db->select("format(monto, 2) as monto, format(capital, 2) as capital, format(interes, 2) as interes, format(fmd, 2) as fmd, format(interes_fmd, 2) as interes_fmd, DATE_FORMAT(fecha_limite, '%d/%m/%Y') as fecha_limite");
		$this->db->from('pagos_pendientes');
		$this->db->where('idcredito', $idcredito);
		$this->db->where('monto > 0');
		$this->db->where('fecha_limite <= CURDATE()');

		$query = $this->db->get();

		return $query->result();
	}

	public function pagos_del_dia($idsucursal, $fecha)
	{
		$this->db->select("pr.id as recibo, c.nombre, format(pr.monto, 2) as monto, 
			case when pr.tipo_pago = 1 then 'Efectivo'
			when pr.tipo_pago = 2 then 'Depósito Bancario'
			when pr.tipo_pago = 3 then 'Nota de Crédito'
			end as tipo_pago,
			case
			when pr.pendiente = 0 then 'Letra Vencida'
			when pr.pendiente = 1 then 'Letra Adelantada'
			when pr.pendiente = 2 then 'Letra Anticipada'
			end as pendiente");
		$this->db->from('pagos_realizados pr');
		$this->db->join('clientes c', 'pr.idcliente = c.id');
		$this->db->where('pr.idsucursal', $idsucursal);
		$this->db->where('date(pr.fecha)', $fecha);

		$query = $this->db->get();

		return $query->result();
	}

	public function get_detalle_pago($recibo)
	{
		$query = $this->db->query("select pp.documento, format(ppr.importe_interes, 2) as interes, format(ppr.importe_capital, 2) as capital from pagos_realizados pr
								   inner join pendiente_realizado ppr on pr.id = ppr.idpagorealizado
								   inner join pagos_pendientes pp on pp.id = ppr.idpagopendiente
								   where pr.id = $recibo");

		return $query->result();
	}

	public function get_total_cartera()
	{
		$this->db->select("SUM(saldo) as total")
		->from('creditos')
		->where('status = 0');

		$query = $this->db->get();

		return $query->row();
	}
}

/* End of file Reportes_model.php */
/* Location: ./application/models/Reportes_model.php */