<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//date_default_timezone_set('America/Merida');
		$this->load->database();
	}

	public function get_pagos_semana()
	{
		
		//si el usuario es admin usamos no filtramos por sucursal
		if ($this->ion_auth->is_admin())
		{
			$query = $this->db->query("select c.id, cr.codigo, cr.id as pp_id, c.nombre, c.movil, c.telefono, c.email, c.direccion,
									max(pp.fecha_limite) as fecha_limite, sum(pp.monto) as monto
									from creditos cr
									inner join pagos_pendientes pp on cr.id = pp.idcredito
									inner join clientes c on c.id = cr.idcliente
									where fecha_limite <= date_sub(date_add(CURDATE(), interval 1 month), interval 1 day)
									AND cr.status = 0
									group by c.id, c.nombre, c.movil, c.telefono, c.email");
		}
		else
		{
			//tomamos la info del usuario logeado
			$user = $this->ion_auth->user()->row();

		 	$query = $this->db->query("select c.id, cr.codigo, cr.id as pp_id, c.nombre, c.movil, c.telefono, c.email, c.direccion,
									max(pp.fecha_limite) as fecha_limite, sum(pp.monto) as monto
									from creditos cr
									inner join pagos_pendientes pp on cr.id = pp.idcredito
									inner join clientes c on c.id = cr.idcliente
									where fecha_limite <= date_sub(date_add(CURDATE(), interval 1 month), interval 1 day)
									AND cr.status = 0
									and cr.idsucursal = {$user->idsucursal}
									group by c.id, c.nombre, c.movil, c.telefono, c.email");
		}

		return $query->result_array();
	}

	public function get($idcredito)
	{

		$this->db->select('sum(pp.monto) as monto, cr.id, cr.idcliente')
		->from('pagos_pendientes pp, creditos cr')
		->where('pp.idcredito = cr.id')
		->where('pp.idcredito', $idcredito)
		->where('fecha_limite <= date_sub(date_add(CURDATE(), interval 1 month), interval 1 day)');

		$query = $this->db->get();

		return $query->row();
	}

	public function get_letras_atrasadas_count($idcredito)
	{
		$query = $this->db->query("select count(1) as pagos from pagos_pendientes pp, creditos cr
								   where cr.id = pp.idcredito
								   and cr.id = $idcredito
								   and pp.monto > 0
								   and pp.fecha_limite <= curdate()");

		return $query->row()->pagos;
	}

	public function insert_pago($data)
	{	
		
		$user = $this->ion_auth->user()->row();
		$tipo = $data['tipo_pago'];
		$importe = $data['importe'];
		$cliente = $data['id'];
		$pendiente = $data['pendiente'];
		
		$data = array(
			'idsucursal' =>$user->idsucursal,
			'idcliente' =>$cliente,
			'monto' =>$importe,
			'fecha' =>date('Y-m-d H:i:s'),
			'tipo_pago' => $tipo,
			'pendiente' => $pendiente
		);

		if ($this->db->insert('pagos_realizados',$data)) {
			return true;
		}
		else
		{
			return false;
		}
	}

}

/* End of file Dashboard_model.php */
/* Location: ./application/models/Dashboard_model.php */