<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Conceptos_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_conceptos_count_by_pago($idpagorealizado)
	{
		$this->db->where('idpagorealizado', $idpagorealizado);
		$this->db->from('concepto_pago');
		return $this->db->count_all_results();
	}

	public function create_concepto($data)
	{
		$this->db->insert('concepto_pago', $data);
	}

	public function get_conceptos($idpagorealizado)
	{
		return $this->db->get_where('concepto_pago', array('idpagorealizado' => $idpagorealizado))->result_array();
	}
}

/* End of file Conceptos_model.php */
/* Location: ./application/models/Conceptos_model.php */