<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pagos_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getUnrelatedPayments($pendiente)
	{
		$this->db->select('pr.id, pr.idcliente, pr.monto');
		$this->db->from('pagos_realizados pr');
		$this->db->join('pendiente_realizado pen', 'pr.id = pen.idpagorealizado', 'left');
		$this->db->where('pen.idpagorealizado', null);
		$this->db->where('pr.pendiente', $pendiente);
		$this->db->order_by('pr.fecha', 'asc');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getUnrelatedPaymentsByClient($idcliente, $pendiente)
	{
		$this->db->select('pr.id, pr.idcliente, pr.monto');
		$this->db->from('pagos_realizados pr');
		$this->db->join('pendiente_realizado pen', 'pr.id = pen.idpagorealizado', 'left');
		$this->db->where('pen.idpagorealizado', null);
		$this->db->where('pr.idcliente', $idcliente);
		$this->db->where('pr.pendiente', $pendiente);
		$this->db->order_by('pr.fecha', 'asc');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getPendingPaymentsByClient($idcliente, $pendiente)
	{
		$this->db->select('pp.*');
		$this->db->from('pagos_pendientes pp');
		$this->db->join('creditos c', 'c.id = pp.idcredito');
		$this->db->where('c.idcliente', $idcliente);
		$this->db->where('pp.monto > 0');

		switch ($pendiente)
		{
			case 0:
				$this->db->where('fecha_limite <= DATE_SUB(DATE_ADD(CURDATE(), INTERVAL 1 MONTH), INTERVAL 1 DAY)');
				break;
			case 1:
				$this->db->where('fecha_limite <= CURDATE()');
				break;
			case 2:
			break;
		}

		$this->db->order_by('pp.fecha_limite asc');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getAdvancePaymentsByClient($idcliente)
	{
		$this->db->select('pp.*');
		$this->db->from('pagos_pendientes pp');
		$this->db->join('creditos c', 'c.id = pp.idcredito');
		$this->db->where('c.idcliente', $idcliente);
		$this->db->where('pp.monto > 0');
		$this->db->where('fecha_limite >= DATE_SUB(DATE_ADD(CURDATE(), INTERVAL 1 MONTH), INTERVAL 1 DAY)');
		$this->db->order_by('pp.fecha_limite desc');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function updatePendingPayment($pp)
	{
		//$pp['monto'] = $pp['capital'] + $pp['interes'];

		$this->db->where('id', $pp['id']);
		$this->db->update('pagos_pendientes', $pp);
	}

	public function createPendienteRealizado($pp, $pr, $suma_interes, $suma_capital)
	{
		$data = array(
			'idpagorealizado' => $pr,
			'idpagopendiente' => $pp,
			'importe_interes' => $suma_interes,
			'importe_capital' => $suma_capital
		);

		$this->db->insert('pendiente_realizado', $data);
	}

	public function getEvaluatedPendingPayments()
	{
		$this->db->select('pp.id, pp.idcredito, pp.monto, pp.interes, pp.interes_fmd, pp.capital, pp.fmd, pp.fecha_limite, pp.dias_vencido')->distinct();
		$this->db->from('pagos_pendientes pp');
		$this->db->join('pendiente_realizado ppr', 'ppr.idpagopendiente = pp.id');
		$this->db->join('pagos_realizados pr', 'ppr.idpagorealizado = pr.id');
		//$this->db->where('pr.fecha between date_sub(pr.fecha, interval 1 day) and curdate()');
		$this->db->where('pp.fecha_limite < curdate()');
		$this->db->where('pp.monto > 0');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getUnrelatedExpiredPendingPayments()
	{
		$this->db->select('pp.id, pp.fmd, pp.fecha_limite, pp.interes, pp.capital, pp.interes_fmd');
		$this->db->from('pagos_pendientes pp');
		$this->db->join('pendiente_realizado pr', 'pr.idpagopendiente = pp.id', 'left');
		$this->db->join('creditos cr', 'cr.id = pp.idcredito');
		$this->db->where('pp.fecha_limite < curdate()');
		$this->db->where('pp.monto > 0');
		$this->db->where('cr.status = 0');
		$this->db->where('pr.idpagopendiente is null');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getSaldoCredito($id)
	{
		$this->db->select('sum(capital) saldo');
		$this->db->from('pagos_pendientes');
		$this->db->where('idcredito', $id);

		$query = $this->db->get();

		return $query->result_array()[0];
	}

	public function updateSaldo($cr)
	{
		$this->db->where('id', $cr['id']);
		$this->db->update('creditos', $cr);
	}

	public function delete_pago($id)
	{
		$this->db->trans_start();
		//Eliminar conceptos de pago (recibo)
		$this->db->delete('concepto_pago', array('idpagorealizado' => $id));
		//Eliminar pago
		$this->db->delete('pagos_realizados', array('id' => $id));
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}

		$this->db->trans_commit();
		return true;
	}
}

/* End of file Pagos_realizados_model.php */
/* Location: ./application/models/Pagos_realizados_model.php */