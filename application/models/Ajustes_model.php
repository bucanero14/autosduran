<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajustes_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
		$this->load->database();
	}

	public function getInfoCliente($id) 
	{
		$this->db->select('c.*');
		$this->db->from('clientes c, creditos cr');
		$this->db->where('cr.idcliente = c.id');
		$this->db->where('cr.id', $id);

		$query = $this->db->get();

		return $query->row();
	}

	public function getExpiredPendingPayments($idcredito)
	{
		$this->db->select('pp.monto, pp.documento, pp.capital, pp.interes, pp.interes_fmd, pp.fecha_limite, pp.id');
		$this->db->from('pagos_pendientes pp');
		$this->db->where("pp.fecha_limite < curdate()");
		$this->db->where('pp.monto >', 0);
		$this->db->where('pp.idcredito', $idcredito);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getPendingPayments($idcredito)
	{
		$this->db->select('pp.monto, pp.documento, pp.capital, pp.interes, pp.interes_fmd, pp.fecha_limite, pp.id');
		$this->db->from('pagos_pendientes pp');
		$this->db->where('pp.monto >', 0);
		$this->db->where('pp.idcredito', $idcredito);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getPendingPayment($id)
	{
		$this->db->select('*');
		$this->db->from('pagos_pendientes');
		$this->db->where('id', $id);

		$query = $this->db->get();

		return $query->row();
	}

	public function setAdjustment($id, $fmd)
	{
		try 
		{
			$pago = $this->getPendingPayment($id);

			$this->db->set('interes_fmd', $fmd);
			$this->db->set('monto', $pago->capital + $pago->interes + $fmd);
			$this->db->where('id', $id);
			$this->db->update('pagos_pendientes');
			return true;
		}
		catch (Exception $ex)
		{
			return false;
		}
	}

	public function setAdjustmentInterest($id, $interes)
	{
		try 
		{
			$pago = $this->getPendingPayment($id);

			$this->db->set('interes', $interes);
			$this->db->set('monto', $pago->capital + $pago->interes_fmd + $interes);
			$this->db->where('id', $id);
			$this->db->update('pagos_pendientes');
			return true;
		}
		catch (Exception $ex)
		{
			return false;
		}
	}
}

/* End of file  */
/* Location: ./application/models/ */