<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	public function getAllUsers() 
	{
		$this->db->select('u.id, u.username, u.full_name, s.nombre, u.active, g.name as rolename');
		$this->db->from('users u');
		$this->db->join('users_groups ug', 'ug.user_id = u.id');
		$this->db->join('groups g', 'g.id = ug.group_id');
		$this->db->join('sucursales s', 's.id = u.idsucursal');
		$this->db->where('u.active', 1);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getAllRoles()
	{
		$this->db->select('id, name');
		$this->db->from('groups');

		$query = $this->db->get();

		$result = array();
		foreach($query->result_array() as $row) {
			$result[$row['id']] = $row['name'];
		}

		return $result;
	}

	public function getAllBranches()
	{
		$this->db->select('id, nombre');
		$this->db->from('sucursales');

		$query = $this->db->get();

		$result = array();
		foreach($query->result_array() as $row) {
			$result[$row['id']] = $row['nombre'];
		}

		return $result;
	}

	public function getUserById($id) {
		$this->db->select('u.id, u.username, u.full_name, ug.group_id as role, u.idsucursal');
		$this->db->from('users u');
		$this->db->join('users_groups ug', 'ug.user_id = u.id');
		$this->db->where('u.id', $id);

		$query = $this->db->get();

		return $query->result()[0];
	}

	public function disableUser($id) {
		$data = array('active' => 0);

		$this->db->where('id', $id);
		$this->db->update('users', $data);

		return true;
	}
}