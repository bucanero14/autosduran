<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Historial_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
	}

	public function getAll()
	{
		$this->db->select('fecha, usuario, descripcion');
		$this->db->from('historial_actividades');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function setHistorial($username, $desc)
	{
		$data = array(
			'fecha' => date('Y-m-d H:i:s'),
			'usuario' => $username,
			'descripcion' => $desc
		);

		$this->db->insert('historial_actividades', $data);
	}
}

/* End of file Historial.php */
/* Location: ./application/models/Historial.php */