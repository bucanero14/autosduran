<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getPendingPayments($idcliente)
	{

		$this->db->select('pp.monto, pp.documento, pp.capital, pp.interes, pp.interes_fmd, pp.fecha_limite,pp.idcredito');
		$this->db->from('pagos_pendientes pp');
		$this->db->join('creditos c', 'c.id = pp.idcredito');
		$this->db->where('pp.monto >', 0);
		$this->db->where('c.idcliente', $idcliente);

		if (!$this->ion_auth->is_admin())
			$this->db->where("pp.fecha_limite <= date_sub(date_add(curdate(), interval 1 month), interval 1 day)");

		$query = $this->db->get();

		return $query->result_array();
	}	

	public function getInfoVehiculo($idcliente)
	{
		$this->db->select('cr.id, cr.marca_vehiculo,cr.modelo_vehiculo,cr.year_vehiculo')
		->from('creditos as cr, clientes as cl')
	 	->where('cr.idcliente = cl.id')
	 	->where('cl.id', $idcliente);

		$query = $this->db->get();

		return $query->row();
	}

	public function getPayments($idcliente)
	{
		$this->db->select("pr.id, s.nombre, pr.monto, pr.fecha, case 
			when pr.tipo_pago = 1 then 'Efectivo' 
			when pr.tipo_pago = 2 then 'Depósito Bancario'
			when pr.tipo_pago = 3 then 'Nota de Crédito'
			end as tipo_pago");
		$this->db->from('pagos_realizados pr');
		$this->db->join('sucursales s', 's.id = pr.idsucursal');
		$this->db->where('pr.idcliente', $idcliente);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function getPaymentById($id)
	{
		$query = $this->db->where('id',$id)->get('pagos_realizados')->row_array();

		return $query;
	}

	public function getObservations($idcliente)
	{
		$this->db->select('fecha, observacion');
		$this->db->from('observaciones');
		$this->db->where('idcliente', $idcliente);
		$this->db->order_by('fecha desc');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function addObservation($idcliente, $observacion)
	{
		$data = array(
			'idcliente' => $idcliente,
			'fecha' => date('Y-m-d H:i:s'),
			'observacion' => $observacion
		);

		$this->db->insert('observaciones', $data);
	}

	public function getClientInfo($id)
	{
		$this->db->select('cl.id, cl.nombre, cl.email, inv.nombre inversionista');
		$this->db->from('clientes cl, creditos cr, inversionistas inv');
		$this->db->where('cl.id', $id);
		$this->db->where('cr.idcliente = cl.id');
		$this->db->where('cr.idinversionista = inv.id');

		$query = $this->db->get();

		return $query->result_array()[0];
	}

	public function getMontoLiquidacion($idcredito, $pendiente)
	{
		$this->db->select('sum(capital) as capital');
		$this->db->from('pagos_pendientes');
		$this->db->where('idcredito', $idcredito);

		$capital = $this->db->get()->row()->capital;

		$this->db->select('sum(interes + interes_fmd) as interes');
		$this->db->from('pagos_pendientes');
		$this->db->where('idcredito', $idcredito);

		switch($pendiente)
		{
			case 0:
				$this->db->where('fecha_limite <= date_sub(date_add(curdate(), interval 1 month), interval 1 day)');
				break;
			case 1:
				$this->db->where('fecha_limite <= curdate()');
				break;
			case 2:
				break;
		}

		$interes = $this->db->get()->row()->interes;

		$this->db->select('idcliente');
		$this->db->from('creditos');
		$this->db->where('id', $idcredito);

		$return = new stdClass();
		$return->id = $idcredito;
		$return->idcliente = $this->db->get()->row()->idcliente;
		$return->monto = bcadd($capital, $interes, 2);
		return $return;
	}

	public function getTotalPendingPayments($idcredito)
	{
		$this->db->select('pp.monto, pp.documento, pp.capital, pp.interes, pp.interes_fmd, pp.fecha_limite,pp.idcredito');
		$this->db->from('pagos_pendientes pp');
		$this->db->where('pp.monto >', 0);
		$this->db->where('pp.idcredito', $idcredito);

		$query = $this->db->get();

		return $query->result_array();
	}
}

/* End of file Clientes_model.php */
/* Location: ./application/models/Clientes_model.php */