<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Custom_functions {

    public function fecha_completa($fecha){ //convierte una fecha en formato Mysql a Letras
	
		$meses= array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		
		//preg_match( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $fecha, $mifecha);
		
		$mifecha = explode("-",$fecha);
		
		$lafecha = $mifecha[2]." de ".$meses[$mifecha[1]-1]." de ".$mifecha[0];
		return $lafecha;
	}

	function agregar_mes(\DateTime $date, $monthToAdd)
	{
	    $year = $date->format('Y');
	    $month = $date->format('n');
	    $day = $date->format('d');

	    $year += floor($monthToAdd / 12);
	    $monthToAdd = $monthToAdd % 12;
	    $month += $monthToAdd;
	    if ($month > 12) {
	        $year ++;
	        $month = $month % 12;
	        if ($month === 0) {
	            $month = 12;
	        }
	    }

	    if (! checkdate($month, $day, $year)) {
	        $newDate = \DateTime::createFromFormat('Y-n-j', $year . '-' . $month . '-1');
	        $newDate->modify('last day of');
	    } else {
	        $newDate = \DateTime::createFromFormat('Y-n-d', $year . '-' . $month . '-' . $day);
	    }
	    $newDate->setTime($date->format('H'), $date->format('i'), $date->format('s'));

	    return $newDate->format('Y-m-d');
	}
}