# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.42)
# Database: autosduran
# Generation Time: 2017-05-28 23:01:11 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table avales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `avales`;

CREATE TABLE `avales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL COMMENT '	\n',
  `direccion` varchar(500) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `movil` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `avales` WRITE;
/*!40000 ALTER TABLE `avales` DISABLE KEYS */;

INSERT INTO `avales` (`id`, `nombre`, `direccion`, `telefono`, `movil`, `email`)
VALUES
	(5,'José Durán','Conocida','9999433047','9999685637','bucanero14@gmail.com'),
	(8,'Raúl Enrique Magaña González','Calle 21 interior #308','019999685637','999685637','bucanero14@gmail.com'),
	(9,'yhujij','hhuun','019999685637','9999685637','bucanero14@gmail.com'),
	(10,'Juan Carlos Brito Rivas','Calle 51-C #909 por 108-A y 112 Fracc. Las Américas II','9449417','9992923563','cocobrito3@gmail.com'),
	(11,'Eduardo','Herrera','9994064270','9992923563','eddherrera@gmail.com'),
	(12,'Eduardo','Herrera','9994064270','9992923563','eddherrera@gmail.com'),
	(13,'Rosy Herrera','Calle 21 interior #308, Privada Los Alamos','9999685637','9999685637','bucanero14@gmail.com'),
	(14,'Rosy Herrera','Calle 21 interior #308, Privada Los Alamos','9999685637','9999685637','bucanero14@gmail.com');

/*!40000 ALTER TABLE `avales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clientes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientes`;

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL COMMENT '	\n',
  `direccion` varchar(500) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `movil` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;

INSERT INTO `clientes` (`id`, `nombre`, `direccion`, `telefono`, `movil`, `email`)
VALUES
	(5,'José Durán','Conocida','9999433047','9999685637','bucanero14@gmail.com'),
	(8,'Raúl Enrique Magaña González','Calle 21 interior #308, Privada Los Alamos','019999685637','9999685637','bucanero14@gmail.com'),
	(9,'yyyyjkhj','jkkju','99999999n','9999','bucanero14@gmail.com'),
	(10,'Juan Carlos Brito Rivas','Calle 51-C #909 por 108-A y 112 Fracc. Las Américas II','9449417','9992923563','cocobrito3@gmail.com'),
	(11,'Eduardo','Herrera','9994064270','9992923563','eddherrera@gmail.com'),
	(12,'Eduardo','Herrera','9994064270','9992923563','eddherrera@gmail.com'),
	(13,'Rosy Herrera','Calle 21 interior #308, Privada Los Alamos','9999685637','9999685637','bucanero14@gmail.com'),
	(14,'Rosy Herrera','Calle 21 interior #308, Privada Los Alamos','9999685637','9999685637','bucanero14@gmail.com');

/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table concepto_pago
# ------------------------------------------------------------

DROP TABLE IF EXISTS `concepto_pago`;

CREATE TABLE `concepto_pago` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idpagorealizado` int(11) NOT NULL,
  `monto` decimal(10,2) DEFAULT '0.00',
  `concepto` text,
  PRIMARY KEY (`id`),
  KEY `fk_concepto_pago_pagos_realizados` (`idpagorealizado`),
  CONSTRAINT `fk_concepto_pago_pagos_realizados` FOREIGN KEY (`idpagorealizado`) REFERENCES `pagos_realizados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `concepto_pago` WRITE;
/*!40000 ALTER TABLE `concepto_pago` DISABLE KEYS */;

INSERT INTO `concepto_pago` (`id`, `idpagorealizado`, `monto`, `concepto`)
VALUES
	(77,18,5309.58,'Pago de letra 1 con vencimiento el día 10/01/2017.'),
	(78,18,4863.49,'Pago de letra 2 con vencimiento el día 10/02/2017.'),
	(79,18,4460.57,'Pago de letra 3 con vencimiento el día 10/03/2017.'),
	(80,18,4316.67,'Pago de letra 4 con vencimiento el día 10/04/2017.'),
	(81,18,2916.67,'Adelanto de letra 24 con vencimiento el día 10/12/2018.'),
	(82,18,2916.67,'Adelanto de letra 23 con vencimiento el día 10/11/2018.'),
	(83,18,216.35,'Abono a letra 22 con vencimiento el día 10/10/2018.'),
	(84,19,4316.67,'Pago de letra 5 con vencimiento el día 10/05/2017.'),
	(85,19,4316.67,'Pago de letra 6 con vencimiento el día 10/06/2017.'),
	(86,19,2700.32,'Adelanto de letra 22 con vencimiento el día 10/10/2018.'),
	(87,19,2916.67,'Adelanto de letra 21 con vencimiento el día 10/09/2018.'),
	(88,19,2916.67,'Adelanto de letra 20 con vencimiento el día 10/08/2018.'),
	(89,19,2916.67,'Adelanto de letra 19 con vencimiento el día 10/07/2018.'),
	(90,19,2916.67,'Adelanto de letra 18 con vencimiento el día 10/06/2018.'),
	(91,19,2916.67,'Adelanto de letra 17 con vencimiento el día 10/05/2018.'),
	(92,19,2916.67,'Adelanto de letra 16 con vencimiento el día 10/04/2018.'),
	(93,19,2916.67,'Adelanto de letra 15 con vencimiento el día 10/03/2018.'),
	(94,19,2916.67,'Adelanto de letra 14 con vencimiento el día 10/02/2018.'),
	(95,19,2916.67,'Adelanto de letra 13 con vencimiento el día 10/01/2018.'),
	(96,19,2916.67,'Adelanto de letra 12 con vencimiento el día 10/12/2017.'),
	(97,19,2916.67,'Adelanto de letra 11 con vencimiento el día 10/11/2017.'),
	(98,19,2916.67,'Adelanto de letra 10 con vencimiento el día 10/10/2017.'),
	(99,19,2916.67,'Adelanto de letra 9 con vencimiento el día 10/09/2017.'),
	(100,19,2916.67,'Adelanto de letra 8 con vencimiento el día 10/08/2017.'),
	(101,19,2916.67,'Adelanto de letra 7 con vencimiento el día 10/07/2017.');

/*!40000 ALTER TABLE `concepto_pago` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table creditos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `creditos`;

CREATE TABLE `creditos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` int(11) NOT NULL DEFAULT '0',
  `idinversionista` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `idaval` int(11) NOT NULL,
  `idsucursal` int(11) NOT NULL,
  `marca_vehiculo` varchar(45) DEFAULT NULL,
  `modelo_vehiculo` varchar(45) DEFAULT NULL,
  `year_vehiculo` int(11) DEFAULT NULL,
  `valor_vehiculo` decimal(10,2) DEFAULT NULL,
  `plazo` int(11) DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT NULL,
  `tasa_fmd` decimal(5,2) DEFAULT NULL,
  `tasa` decimal(5,2) DEFAULT NULL,
  `saldo` decimal(10,2) DEFAULT NULL,
  `enganche` decimal(10,2) DEFAULT NULL,
  `fecha_inicial` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_creditos_inversionistas_idx` (`idinversionista`),
  KEY `fk_creditos_avales1_idx` (`idaval`),
  KEY `fk_creditos_sucursales1_idx` (`idsucursal`),
  KEY `fk_creditos_clientes_idx` (`idcliente`),
  CONSTRAINT `fk_creditos_avales1` FOREIGN KEY (`idaval`) REFERENCES `avales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creditos_clientes` FOREIGN KEY (`idcliente`) REFERENCES `clientes` (`id`),
  CONSTRAINT `fk_creditos_inversionistas` FOREIGN KEY (`idinversionista`) REFERENCES `inversionistas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_creditos_sucursales1` FOREIGN KEY (`idsucursal`) REFERENCES `sucursales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `creditos` WRITE;
/*!40000 ALTER TABLE `creditos` DISABLE KEYS */;

INSERT INTO `creditos` (`id`, `codigo`, `idinversionista`, `idcliente`, `idaval`, `idsucursal`, `marca_vehiculo`, `modelo_vehiculo`, `year_vehiculo`, `valor_vehiculo`, `plazo`, `monto`, `tasa_fmd`, `tasa`, `saldo`, `enganche`, `fecha_inicial`, `status`)
VALUES
	(4,119,3,5,5,1,'Nissan','Versa',2013,100000.00,24,70000.00,10.00,2.00,0.00,30000.00,'2017-01-10',1),
	(6,120,3,8,8,1,'Honda','Civic',2010,150000.00,36,80000.00,10.00,1.50,80000.00,70000.00,'2016-12-15',0),
	(7,121,1,9,9,1,'ford','t',2013,169000.00,48,109000.00,10.00,2.00,109000.00,60000.00,'2017-02-20',0),
	(8,122,2,10,10,1,'Mazda','3',2015,150000.00,48,100000.00,10.00,2.00,100000.00,50000.00,'2017-02-25',0),
	(9,123,1,11,11,1,'Volkswagen','Vento',2014,200000.00,24,150000.00,10.00,1.50,150000.00,50000.00,'2017-04-21',0),
	(10,124,1,12,12,1,'Volkswagen','Vento',2014,200000.00,24,150000.00,10.00,1.50,150000.00,50000.00,'2017-04-21',0),
	(11,125,1,14,14,2,'Ford','Focus',2000,100000.00,12,25000.00,10.00,1.50,25000.00,75000.00,'2017-04-27',0);

/*!40000 ALTER TABLE `creditos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`)
VALUES
	(1,'Administrador','Administrator'),
	(2,'Cajero','General User');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table historial_actividades
# ------------------------------------------------------------

DROP TABLE IF EXISTS `historial_actividades`;

CREATE TABLE `historial_actividades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime DEFAULT NULL,
  `usuario` varchar(45) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `historial_actividades` WRITE;
/*!40000 ALTER TABLE `historial_actividades` DISABLE KEYS */;

INSERT INTO `historial_actividades` (`id`, `fecha`, `usuario`, `descripcion`)
VALUES
	(3,'2017-01-09 20:08:46','jduran','Acceso a la sección de Créditos.'),
	(4,'2017-01-09 08:10:32','jduran','Acceso a la sección de Créditos.'),
	(5,'2017-01-09 20:11:05','jduran','Acceso a la sección de Créditos.'),
	(6,'2017-01-09 20:18:24','mfernandez','Acceso a la sección de Créditos.'),
	(7,'2017-05-28 17:57:06','Sistema','Pagos procesados exitosamente'),
	(8,'2017-05-28 17:57:06','Sistema','Creditos recalculados exitosamente');

/*!40000 ALTER TABLE `historial_actividades` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table inversionistas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `inversionistas`;

CREATE TABLE `inversionistas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `movil` varchar(45) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `inversionistas` WRITE;
/*!40000 ALTER TABLE `inversionistas` DISABLE KEYS */;

INSERT INTO `inversionistas` (`id`, `nombre`, `telefono`, `movil`, `email`)
VALUES
	(1,'Carolina Pino','2864052','9991552119','carolina-110@hotmail.com'),
	(2,'Nicolás Sogbi','2864052','9991552119','carolinapinososa@gmail.com'),
	(3,'Raúl Magaña','9433047','9999685637','bucanero14@gmail.com');

/*!40000 ALTER TABLE `inversionistas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table observaciones
# ------------------------------------------------------------

DROP TABLE IF EXISTS `observaciones`;

CREATE TABLE `observaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `observacion` text,
  PRIMARY KEY (`id`),
  KEY `fk_cliente` (`idcliente`),
  CONSTRAINT `fk_observaciones_clientes` FOREIGN KEY (`idcliente`) REFERENCES `clientes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `observaciones` WRITE;
/*!40000 ALTER TABLE `observaciones` DISABLE KEYS */;

INSERT INTO `observaciones` (`id`, `idcliente`, `fecha`, `observacion`)
VALUES
	(2,5,'2017-01-16 01:18:35','Se le intentó contactar, pero no respondió.'),
	(3,5,'2017-01-16 01:18:55','Se le contactó una vez más, pero no contestó.'),
	(4,5,'2017-01-20 15:10:53','Se le contactó y dijo que se le llame el 21 de enero'),
	(14,5,'2017-02-01 20:38:13','Negativa a pagar');

/*!40000 ALTER TABLE `observaciones` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pagos_extraordinarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pagos_extraordinarios`;

CREATE TABLE `pagos_extraordinarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idsucursal` int(11) NOT NULL,
  `monto` int(11) DEFAULT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pagos_extraordinarios_sucursal` (`idsucursal`),
  CONSTRAINT `fk_pagos_extraordinarios_sucursal` FOREIGN KEY (`idsucursal`) REFERENCES `sucursales` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pagos_extraordinarios` WRITE;
/*!40000 ALTER TABLE `pagos_extraordinarios` DISABLE KEYS */;

INSERT INTO `pagos_extraordinarios` (`id`, `idsucursal`, `monto`, `descripcion`, `fecha`)
VALUES
	(1,2,50000,'Enganche de Toyota Corolla','2017-03-01');

/*!40000 ALTER TABLE `pagos_extraordinarios` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pagos_pendientes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pagos_pendientes`;

CREATE TABLE `pagos_pendientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcredito` int(11) NOT NULL,
  `documento` int(11) DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT '0.00',
  `fmd` decimal(10,2) DEFAULT '0.00',
  `fecha_limite` date DEFAULT NULL,
  `capital` decimal(10,2) DEFAULT '0.00',
  `interes` decimal(10,2) DEFAULT '0.00',
  `interes_fmd` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `fk_pagos_pendientes_creditos1_idx` (`idcredito`),
  CONSTRAINT `fk_pagos_pendientes_creditos1` FOREIGN KEY (`idcredito`) REFERENCES `creditos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pagos_pendientes` WRITE;
/*!40000 ALTER TABLE `pagos_pendientes` DISABLE KEYS */;

INSERT INTO `pagos_pendientes` (`id`, `idcredito`, `documento`, `monto`, `fmd`, `fecha_limite`, `capital`, `interes`, `interes_fmd`)
VALUES
	(49,4,1,0.00,0.00,'2017-01-10',0.00,0.00,0.00),
	(50,4,2,0.00,0.00,'2017-02-10',0.00,0.00,0.00),
	(51,4,3,0.00,0.00,'2017-03-10',0.00,0.00,0.00),
	(52,4,4,0.00,0.00,'2017-04-10',0.00,0.00,0.00),
	(53,4,5,0.00,0.00,'2017-05-10',0.00,0.00,0.00),
	(54,4,6,0.00,0.00,'2017-06-10',0.00,0.00,0.00),
	(55,4,7,0.00,0.00,'2017-07-10',0.00,0.00,0.00),
	(56,4,8,0.00,0.00,'2017-08-10',0.00,0.00,0.00),
	(57,4,9,0.00,0.00,'2017-09-10',0.00,0.00,0.00),
	(58,4,10,0.00,0.00,'2017-10-10',0.00,0.00,0.00),
	(59,4,11,0.00,0.00,'2017-11-10',0.00,0.00,0.00),
	(60,4,12,0.00,0.00,'2017-12-10',0.00,0.00,0.00),
	(61,4,13,0.00,0.00,'2018-01-10',0.00,0.00,0.00),
	(62,4,14,0.00,0.00,'2018-02-10',0.00,0.00,0.00),
	(63,4,15,0.00,0.00,'2018-03-10',0.00,0.00,0.00),
	(64,4,16,0.00,0.00,'2018-04-10',0.00,0.00,0.00),
	(65,4,17,0.00,0.00,'2018-05-10',0.00,0.00,0.00),
	(66,4,18,0.00,0.00,'2018-06-10',0.00,0.00,0.00),
	(67,4,19,0.00,0.00,'2018-07-10',0.00,0.00,0.00),
	(68,4,20,0.00,0.00,'2018-08-10',0.00,0.00,0.00),
	(69,4,21,0.00,0.00,'2018-09-10',0.00,0.00,0.00),
	(70,4,22,0.00,0.00,'2018-10-10',0.00,0.00,0.00),
	(71,4,23,0.00,0.00,'2018-11-10',0.00,0.00,0.00),
	(72,4,24,0.00,0.00,'2018-12-10',0.00,0.00,0.00),
	(109,6,1,0.00,0.00,'2016-12-15',0.00,0.00,0.00),
	(110,6,2,0.00,0.00,'2017-01-15',0.00,0.00,0.00),
	(111,6,3,0.00,0.00,'2017-02-15',0.00,0.00,0.00),
	(112,6,4,0.00,0.00,'2017-03-15',0.00,0.00,0.00),
	(113,6,5,0.00,0.00,'2017-04-15',0.00,0.00,0.00),
	(114,6,6,3570.55,11.41,'2017-05-15',2222.22,1200.00,148.33),
	(115,6,7,3422.22,11.41,'2017-06-15',2222.22,1200.00,0.00),
	(116,6,8,3422.22,11.41,'2017-07-15',2222.22,1200.00,0.00),
	(117,6,9,3422.22,11.41,'2017-08-15',2222.22,1200.00,0.00),
	(118,6,10,3422.22,11.41,'2017-09-15',2222.22,1200.00,0.00),
	(119,6,11,3422.22,11.41,'2017-10-15',2222.22,1200.00,0.00),
	(120,6,12,3422.22,11.41,'2017-11-15',2222.22,1200.00,0.00),
	(121,6,13,3422.22,11.41,'2017-12-15',2222.22,1200.00,0.00),
	(122,6,14,3422.22,11.41,'2018-01-15',2222.22,1200.00,0.00),
	(123,6,15,3422.22,11.41,'2018-02-15',2222.22,1200.00,0.00),
	(124,6,16,3422.22,11.41,'2018-03-15',2222.22,1200.00,0.00),
	(125,6,17,3422.22,11.41,'2018-04-15',2222.22,1200.00,0.00),
	(126,6,18,3422.22,11.41,'2018-05-15',2222.22,1200.00,0.00),
	(127,6,19,3422.22,11.41,'2018-06-15',2222.22,1200.00,0.00),
	(128,6,20,3422.22,11.41,'2018-07-15',2222.22,1200.00,0.00),
	(129,6,21,3422.22,11.41,'2018-08-15',2222.22,1200.00,0.00),
	(130,6,22,3422.22,11.41,'2018-09-15',2222.22,1200.00,0.00),
	(131,6,23,3422.22,11.41,'2018-10-15',2222.22,1200.00,0.00),
	(132,6,24,3422.22,11.41,'2018-11-15',2222.22,1200.00,0.00),
	(133,6,25,3422.22,11.41,'2018-12-15',2222.22,1200.00,0.00),
	(134,6,26,3422.22,11.41,'2019-01-15',2222.22,1200.00,0.00),
	(135,6,27,3422.22,11.41,'2019-02-15',2222.22,1200.00,0.00),
	(136,6,28,3422.22,11.41,'2019-03-15',2222.22,1200.00,0.00),
	(137,6,29,3422.22,11.41,'2019-04-15',2222.22,1200.00,0.00),
	(138,6,30,3422.22,11.41,'2019-05-15',2222.22,1200.00,0.00),
	(139,6,31,3422.22,11.41,'2019-06-15',2222.22,1200.00,0.00),
	(140,6,32,3422.22,11.41,'2019-07-15',2222.22,1200.00,0.00),
	(141,6,33,3422.22,11.41,'2019-08-15',2222.22,1200.00,0.00),
	(142,6,34,3422.22,11.41,'2019-09-15',2222.22,1200.00,0.00),
	(143,6,35,3422.22,11.41,'2019-10-15',2222.22,1200.00,0.00),
	(144,6,36,3422.22,11.41,'2019-11-15',2222.22,1200.00,0.00),
	(147,7,1,0.00,0.00,'2017-02-20',0.00,0.00,0.00),
	(148,7,2,0.00,0.00,'2017-03-20',0.00,0.00,0.00),
	(149,7,3,0.00,0.00,'2017-04-20',0.00,0.00,0.00),
	(150,7,4,4569.55,14.84,'2017-05-20',2270.83,2180.00,118.72),
	(151,7,5,4450.83,14.84,'2017-06-20',2270.83,2180.00,0.00),
	(152,7,6,4450.83,14.84,'2017-07-20',2270.83,2180.00,0.00),
	(153,7,7,4450.83,14.84,'2017-08-20',2270.83,2180.00,0.00),
	(154,7,8,4450.83,14.84,'2017-09-20',2270.83,2180.00,0.00),
	(155,7,9,4450.83,14.84,'2017-10-20',2270.83,2180.00,0.00),
	(156,7,10,4450.83,14.84,'2017-11-20',2270.83,2180.00,0.00),
	(157,7,11,4450.83,14.84,'2017-12-20',2270.83,2180.00,0.00),
	(158,7,12,4450.83,14.84,'2018-01-20',2270.83,2180.00,0.00),
	(159,7,13,4450.83,14.84,'2018-02-20',2270.83,2180.00,0.00),
	(160,7,14,4450.83,14.84,'2018-03-20',2270.83,2180.00,0.00),
	(161,7,15,4450.83,14.84,'2018-04-20',2270.83,2180.00,0.00),
	(162,7,16,4450.83,14.84,'2018-05-20',2270.83,2180.00,0.00),
	(163,7,17,4450.83,14.84,'2018-06-20',2270.83,2180.00,0.00),
	(164,7,18,4450.83,14.84,'2018-07-20',2270.83,2180.00,0.00),
	(165,7,19,4450.83,14.84,'2018-08-20',2270.83,2180.00,0.00),
	(166,7,20,4450.83,14.84,'2018-09-20',2270.83,2180.00,0.00),
	(167,7,21,4450.83,14.84,'2018-10-20',2270.83,2180.00,0.00),
	(168,7,22,4450.83,14.84,'2018-11-20',2270.83,2180.00,0.00),
	(169,7,23,4450.83,14.84,'2018-12-20',2270.83,2180.00,0.00),
	(170,7,24,4450.83,14.84,'2019-01-20',2270.83,2180.00,0.00),
	(171,7,25,4450.83,14.84,'2019-02-20',2270.83,2180.00,0.00),
	(172,7,26,4450.83,14.84,'2019-03-20',2270.83,2180.00,0.00),
	(173,7,27,4450.83,14.84,'2019-04-20',2270.83,2180.00,0.00),
	(174,7,28,4450.83,14.84,'2019-05-20',2270.83,2180.00,0.00),
	(175,7,29,4450.83,14.84,'2019-06-20',2270.83,2180.00,0.00),
	(176,7,30,4450.83,14.84,'2019-07-20',2270.83,2180.00,0.00),
	(177,7,31,4450.83,14.84,'2019-08-20',2270.83,2180.00,0.00),
	(178,7,32,4450.83,14.84,'2019-09-20',2270.83,2180.00,0.00),
	(179,7,33,4450.83,14.84,'2019-10-20',2270.83,2180.00,0.00),
	(180,7,34,4450.83,14.84,'2019-11-20',2270.83,2180.00,0.00),
	(181,7,35,4450.83,14.84,'2019-12-20',2270.83,2180.00,0.00),
	(182,7,36,4450.83,14.84,'2020-01-20',2270.83,2180.00,0.00),
	(183,7,37,4450.83,14.84,'2020-02-20',2270.83,2180.00,0.00),
	(184,7,38,4450.83,14.84,'2020-03-20',2270.83,2180.00,0.00),
	(185,7,39,4450.83,14.84,'2020-04-20',2270.83,2180.00,0.00),
	(186,7,40,4450.83,14.84,'2020-05-20',2270.83,2180.00,0.00),
	(187,7,41,4450.83,14.84,'2020-06-20',2270.83,2180.00,0.00),
	(188,7,42,4450.83,14.84,'2020-07-20',2270.83,2180.00,0.00),
	(189,7,43,4450.83,14.84,'2020-08-20',2270.83,2180.00,0.00),
	(190,7,44,4450.83,14.84,'2020-09-20',2270.83,2180.00,0.00),
	(191,7,45,4450.83,14.84,'2020-10-20',2270.83,2180.00,0.00),
	(192,7,46,4450.83,14.84,'2020-11-20',2270.83,2180.00,0.00),
	(193,7,47,3721.66,14.84,'2020-12-20',1541.66,2180.00,0.00),
	(194,7,48,3721.66,14.84,'2021-01-20',1541.66,2180.00,0.00),
	(195,8,1,1648.48,13.61,'2017-02-25',396.36,0.00,1252.12),
	(196,8,2,4954.37,13.61,'2017-03-25',2083.33,2000.00,871.04),
	(197,8,3,4532.46,13.61,'2017-04-25',2083.33,2000.00,449.13),
	(198,8,4,4124.16,13.61,'2017-05-25',2083.33,2000.00,40.83),
	(199,8,5,4083.33,13.61,'2017-06-25',2083.33,2000.00,0.00),
	(200,8,6,4083.33,13.61,'2017-07-25',2083.33,2000.00,0.00),
	(201,8,7,4083.33,13.61,'2017-08-25',2083.33,2000.00,0.00),
	(202,8,8,4083.33,13.61,'2017-09-25',2083.33,2000.00,0.00),
	(203,8,9,4083.33,13.61,'2017-10-25',2083.33,2000.00,0.00),
	(204,8,10,4083.33,13.61,'2017-11-25',2083.33,2000.00,0.00),
	(205,8,11,4083.33,13.61,'2017-12-25',2083.33,2000.00,0.00),
	(206,8,12,4083.33,13.61,'2018-01-25',2083.33,2000.00,0.00),
	(207,8,13,4083.33,13.61,'2018-02-25',2083.33,2000.00,0.00),
	(208,8,14,4083.33,13.61,'2018-03-25',2083.33,2000.00,0.00),
	(209,8,15,4083.33,13.61,'2018-04-25',2083.33,2000.00,0.00),
	(210,8,16,4083.33,13.61,'2018-05-25',2083.33,2000.00,0.00),
	(211,8,17,4083.33,13.61,'2018-06-25',2083.33,2000.00,0.00),
	(212,8,18,4083.33,13.61,'2018-07-25',2083.33,2000.00,0.00),
	(213,8,19,4083.33,13.61,'2018-08-25',2083.33,2000.00,0.00),
	(214,8,20,4083.33,13.61,'2018-09-25',2083.33,2000.00,0.00),
	(215,8,21,4083.33,13.61,'2018-10-25',2083.33,2000.00,0.00),
	(216,8,22,4083.33,13.61,'2018-11-25',2083.33,2000.00,0.00),
	(217,8,23,4083.33,13.61,'2018-12-25',2083.33,2000.00,0.00),
	(218,8,24,4083.33,13.61,'2019-01-25',2083.33,2000.00,0.00),
	(219,8,25,4083.33,13.61,'2019-02-25',2083.33,2000.00,0.00),
	(220,8,26,4083.33,13.61,'2019-03-25',2083.33,2000.00,0.00),
	(221,8,27,4083.33,13.61,'2019-04-25',2083.33,2000.00,0.00),
	(222,8,28,4083.33,13.61,'2019-05-25',2083.33,2000.00,0.00),
	(223,8,29,4083.33,13.61,'2019-06-25',2083.33,2000.00,0.00),
	(224,8,30,4083.33,13.61,'2019-07-25',2083.33,2000.00,0.00),
	(225,8,31,4083.33,13.61,'2019-08-25',2083.33,2000.00,0.00),
	(226,8,32,4083.33,13.61,'2019-09-25',2083.33,2000.00,0.00),
	(227,8,33,4083.33,13.61,'2019-10-25',2083.33,2000.00,0.00),
	(228,8,34,4083.33,13.61,'2019-11-25',2083.33,2000.00,0.00),
	(229,8,35,4083.33,13.61,'2019-12-25',2083.33,2000.00,0.00),
	(230,8,36,4083.33,13.61,'2020-01-25',2083.33,2000.00,0.00),
	(231,8,37,4083.33,13.61,'2020-02-25',2083.33,2000.00,0.00),
	(232,8,38,4083.33,13.61,'2020-03-25',2083.33,2000.00,0.00),
	(233,8,39,4083.33,13.61,'2020-04-25',2083.33,2000.00,0.00),
	(234,8,40,4083.33,13.61,'2020-05-25',2083.33,2000.00,0.00),
	(235,8,41,4083.33,13.61,'2020-06-25',2083.33,2000.00,0.00),
	(236,8,42,4083.33,13.61,'2020-07-25',2083.33,2000.00,0.00),
	(237,8,43,4083.33,13.61,'2020-08-25',2083.33,2000.00,0.00),
	(238,8,44,4083.33,13.61,'2020-09-25',2083.33,2000.00,0.00),
	(239,8,45,4083.33,13.61,'2020-10-25',2083.33,2000.00,0.00),
	(240,8,46,4083.33,13.61,'2020-11-25',2083.33,2000.00,0.00),
	(241,8,47,4083.33,13.61,'2020-12-25',2083.33,2000.00,0.00),
	(242,8,48,4083.33,13.61,'2021-01-25',2083.33,2000.00,0.00),
	(243,9,1,8705.71,25.83,'2017-04-21',6250.00,1500.00,955.71),
	(244,9,2,7930.81,25.83,'2017-05-21',6250.00,1500.00,180.81),
	(245,9,3,7750.00,25.83,'2017-06-21',6250.00,1500.00,0.00),
	(246,9,4,7750.00,25.83,'2017-07-21',6250.00,1500.00,0.00),
	(247,9,5,7750.00,25.83,'2017-08-21',6250.00,1500.00,0.00),
	(248,9,6,7750.00,25.83,'2017-09-21',6250.00,1500.00,0.00),
	(249,9,7,7750.00,25.83,'2017-10-21',6250.00,1500.00,0.00),
	(250,9,8,7750.00,25.83,'2017-11-21',6250.00,1500.00,0.00),
	(251,9,9,7750.00,25.83,'2017-12-21',6250.00,1500.00,0.00),
	(252,9,10,7750.00,25.83,'2018-01-21',6250.00,1500.00,0.00),
	(253,9,11,7750.00,25.83,'2018-02-21',6250.00,1500.00,0.00),
	(254,9,12,7750.00,25.83,'2018-03-21',6250.00,1500.00,0.00),
	(255,9,13,7750.00,25.83,'2018-04-21',6250.00,1500.00,0.00),
	(256,9,14,7750.00,25.83,'2018-05-21',6250.00,1500.00,0.00),
	(257,9,15,7750.00,25.83,'2018-06-21',6250.00,1500.00,0.00),
	(258,9,16,7750.00,25.83,'2018-07-21',6250.00,1500.00,0.00),
	(259,9,17,7750.00,25.83,'2018-08-21',6250.00,1500.00,0.00),
	(260,9,18,7750.00,25.83,'2018-09-21',6250.00,1500.00,0.00),
	(261,9,19,7750.00,25.83,'2018-10-21',6250.00,1500.00,0.00),
	(262,9,20,7750.00,25.83,'2018-11-21',6250.00,1500.00,0.00),
	(263,9,21,7750.00,25.83,'2018-12-21',6250.00,1500.00,0.00),
	(264,9,22,7750.00,25.83,'2019-01-21',6250.00,1500.00,0.00),
	(265,9,23,7750.00,25.83,'2019-02-21',6250.00,1500.00,0.00),
	(266,9,24,7750.00,25.83,'2019-03-21',6250.00,1500.00,0.00),
	(267,10,1,9548.21,28.33,'2017-04-21',6250.00,2250.00,1048.21),
	(268,10,2,8698.31,28.33,'2017-05-21',6250.00,2250.00,198.31),
	(269,10,3,8500.00,28.33,'2017-06-21',6250.00,2250.00,0.00),
	(270,10,4,8500.00,28.33,'2017-07-21',6250.00,2250.00,0.00),
	(271,10,5,8500.00,28.33,'2017-08-21',6250.00,2250.00,0.00),
	(272,10,6,8500.00,28.33,'2017-09-21',6250.00,2250.00,0.00),
	(273,10,7,8500.00,28.33,'2017-10-21',6250.00,2250.00,0.00),
	(274,10,8,8500.00,28.33,'2017-11-21',6250.00,2250.00,0.00),
	(275,10,9,8500.00,28.33,'2017-12-21',6250.00,2250.00,0.00),
	(276,10,10,8500.00,28.33,'2018-01-21',6250.00,2250.00,0.00),
	(277,10,11,8500.00,28.33,'2018-02-21',6250.00,2250.00,0.00),
	(278,10,12,8500.00,28.33,'2018-03-21',6250.00,2250.00,0.00),
	(279,10,13,8500.00,28.33,'2018-04-21',6250.00,2250.00,0.00),
	(280,10,14,8500.00,28.33,'2018-05-21',6250.00,2250.00,0.00),
	(281,10,15,8500.00,28.33,'2018-06-21',6250.00,2250.00,0.00),
	(282,10,16,8500.00,28.33,'2018-07-21',6250.00,2250.00,0.00),
	(283,10,17,8500.00,28.33,'2018-08-21',6250.00,2250.00,0.00),
	(284,10,18,8500.00,28.33,'2018-09-21',6250.00,2250.00,0.00),
	(285,10,19,8500.00,28.33,'2018-10-21',6250.00,2250.00,0.00),
	(286,10,20,8500.00,28.33,'2018-11-21',6250.00,2250.00,0.00),
	(287,10,21,8500.00,28.33,'2018-12-21',6250.00,2250.00,0.00),
	(288,10,22,8500.00,28.33,'2019-01-21',6250.00,2250.00,0.00),
	(289,10,23,8500.00,28.33,'2019-02-21',6250.00,2250.00,0.00),
	(290,10,24,8500.00,28.33,'2019-03-21',6250.00,2250.00,0.00),
	(291,11,1,2712.22,8.19,'2017-04-27',2083.33,375.00,253.89),
	(292,11,2,2466.52,8.19,'2017-05-27',2083.33,375.00,8.19),
	(293,11,3,2458.33,8.19,'2017-06-27',2083.33,375.00,0.00),
	(294,11,4,2458.33,8.19,'2017-07-27',2083.33,375.00,0.00),
	(295,11,5,2458.33,8.19,'2017-08-27',2083.33,375.00,0.00),
	(296,11,6,2458.33,8.19,'2017-09-27',2083.33,375.00,0.00),
	(297,11,7,2458.33,8.19,'2017-10-27',2083.33,375.00,0.00),
	(298,11,8,2458.33,8.19,'2017-11-27',2083.33,375.00,0.00),
	(299,11,9,2458.33,8.19,'2017-12-27',2083.33,375.00,0.00),
	(300,11,10,2458.33,8.19,'2018-01-27',2083.33,375.00,0.00),
	(301,11,11,2458.33,8.19,'2018-02-27',2083.33,375.00,0.00),
	(302,11,12,2458.33,8.19,'2018-03-27',2083.33,375.00,0.00);

/*!40000 ALTER TABLE `pagos_pendientes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pagos_realizados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pagos_realizados`;

CREATE TABLE `pagos_realizados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idsucursal` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `monto` decimal(10,2) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `tipo_pago` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pagos_realizados_sucursales1_idx` (`idsucursal`),
  KEY `idcliente` (`idcliente`),
  CONSTRAINT `fk_pagos_realizados_clientes` FOREIGN KEY (`idcliente`) REFERENCES `clientes` (`id`),
  CONSTRAINT `fk_pagos_realizados_sucursales1` FOREIGN KEY (`idsucursal`) REFERENCES `sucursales` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pagos_realizados` WRITE;
/*!40000 ALTER TABLE `pagos_realizados` DISABLE KEYS */;

INSERT INTO `pagos_realizados` (`id`, `idsucursal`, `idcliente`, `monto`, `fecha`, `tipo_pago`)
VALUES
	(18,2,5,25000.00,'2017-03-20 15:37:27',1),
	(19,2,5,55083.71,'2017-05-28 17:56:43',1);

/*!40000 ALTER TABLE `pagos_realizados` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pendiente_realizado
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pendiente_realizado`;

CREATE TABLE `pendiente_realizado` (
  `idpagopendiente` int(11) NOT NULL,
  `idpagorealizado` int(11) NOT NULL,
  `importe_capital` decimal(10,2) DEFAULT '0.00',
  `importe_interes` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`idpagopendiente`,`idpagorealizado`),
  KEY `fk_pagos_pendientes_has_pagos_realizados_pagos_realizados1_idx` (`idpagorealizado`),
  KEY `fk_pagos_pendientes_has_pagos_realizados_pagos_pendientes1_idx` (`idpagopendiente`),
  CONSTRAINT `fk_pagos_pendientes_has_pagos_realizados_pagos_pendientes1` FOREIGN KEY (`idpagopendiente`) REFERENCES `pagos_pendientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pagos_pendientes_has_pagos_realizados_pagos_realizados1` FOREIGN KEY (`idpagorealizado`) REFERENCES `pagos_realizados` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `pendiente_realizado` WRITE;
/*!40000 ALTER TABLE `pendiente_realizado` DISABLE KEYS */;

INSERT INTO `pendiente_realizado` (`idpagopendiente`, `idpagorealizado`, `importe_capital`, `importe_interes`)
VALUES
	(49,18,2916.67,2392.91),
	(50,18,2916.67,1946.82),
	(51,18,2916.67,1543.90),
	(52,18,2916.67,1400.00),
	(53,19,2916.67,1400.00),
	(54,19,2916.67,1400.00),
	(55,19,2916.67,0.00),
	(56,19,2916.67,0.00),
	(57,19,2916.67,0.00),
	(58,19,2916.67,0.00),
	(59,19,2916.67,0.00),
	(60,19,2916.67,0.00),
	(61,19,2916.67,0.00),
	(62,19,2916.67,0.00),
	(63,19,2916.67,0.00),
	(64,19,2916.67,0.00),
	(65,19,2916.67,0.00),
	(66,19,2916.67,0.00),
	(67,19,2916.67,0.00),
	(68,19,2916.67,0.00),
	(69,19,2916.67,0.00),
	(70,18,216.35,0.00),
	(70,19,2700.32,0.00),
	(71,18,2916.67,0.00),
	(72,18,2916.67,0.00);

/*!40000 ALTER TABLE `pendiente_realizado` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sucursales
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sucursales`;

CREATE TABLE `sucursales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sucursales` WRITE;
/*!40000 ALTER TABLE `sucursales` DISABLE KEYS */;

INSERT INTO `sucursales` (`id`, `nombre`)
VALUES
	(1,'Avenida Yucatán'),
	(2,'Matriz'),
	(3,'Cartera especial'),
	(4,'Montecristo - Bitacora');

/*!40000 ALTER TABLE `sucursales` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idsucursal` int(11) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `remember_code` varchar(40) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT '',
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `full_name` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `idsucursal`, `ip_address`, `username`, `password`, `remember_code`, `salt`, `email`, `created_on`, `last_login`, `active`, `full_name`)
VALUES
	(1,2,'127.0.0.1','jduran','$2y$08$risDHM7UE4hq8dONV9h98e6yVFVyz63lREKVP.SNQAVj6dnnrakpW','UbXXv6lKyGTpwveT6A1f/.',NULL,'admin@admin.com',1268889823,1496006069,1,'José Durán'),
	(8,3,'127.0.0.1','mnajera','$2y$08$L/OxoWjMKSVdM4qFM583Ku2E./TxnFCi9llpVxj5pXWRwkagAJWUK',NULL,NULL,'',2016,1480634769,0,'Mariana Nájera'),
	(9,2,'127.0.0.1','rmagana','$2y$08$G6GQCSZoplovdw.aE3pX..XChqk8iApCrbT9di/IJ/.77cz33IhcK',NULL,NULL,'',2016,1484946504,1,'Raúl Magaña'),
	(10,1,'127.0.0.1','mfernandez','$2y$08$F7Ot283O6kyva8h6amFqVOijyhNwf/vbcUkFbsSs.ALEOmm5DbNQC',NULL,NULL,'',2016,1484946581,1,'Maritza Fernandez');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(15,1,1),
	(13,8,2),
	(14,9,2),
	(18,10,2);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
